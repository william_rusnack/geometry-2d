Hey Chris,  
Thanks for helping out!

I have a few solutions there that sort of generate one but are all too slow.  
This reversing subsections I think will actually be performant.  

Important to remember is that the reversals are dependant on the previous reversals.

[The part that I'm currently working on](https://bitbucket.org/william_rusnack/geometry-2d/src/9813ffa3cf595b5f1055aae79825089c59868e31/src/Polyline.hs#lines-452)  
[Test for NonSelfIntersectingPolyline type](https://bitbucket.org/william_rusnack/geometry-2d/src/master/tests/PolylineSpec.hs#lines-626)  

Let me know if you need any help with the reversals.

I'm going to start setting up the arbitrary function to use the data structure that you proposed.

Bill
