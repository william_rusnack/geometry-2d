import matplotlib.pyplot as plt


def xy(ps): return [p[0] for p in ps], [p[1] for p in ps]

def plt_point(p):
  plt.plot(*xy([p]), 'o')

def plt_line(pt0, pt1):
  plt.plot(*xy([pt0, pt1]))

def plot_arc(arc):
  plt_line(arc['c'], arc['s'])
  plt_line(arc['c'], arc['e'])
  plt_line(arc['s'], tuple((a+b) for (a, b) in zip(arc['s'], arc['v'])))

def plotPolyine(ps, opts='-b'): plt.plot(*xy(ps), opts)

plot_arc({
  'c': (51.247376309857785, 29.97421833830766),
  's': (0.0,                55.0),
  'e': (2.728003257124197,  0.0),
  'v': (-0.43880688741567997, -0.8985813906133171),
})

plotPolyine([
  (0.0,                55.0),
  (-4.440352006059477, 17.667299900475385),
  (-5.384873994289812, 36.71005918494618),
  (2.728003257124197,  0.0),
])

plt.show()
