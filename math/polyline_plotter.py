import matplotlib.pyplot as plt


directory = 'init_square-triangle-triangle'

def xy(ps): return [p[0] for p in ps], [p[1] for p in ps]

def plotLine(ps, opts='-b'): plt.plot(*xy(ps), opts)

def progressivePlot(ps, opts='-b'):
  for c in range(1, len(ps)):
    plotLine(ps, 'y')

    lines = tuple(zip(ps[:c], ps[1:]))
    for lps in lines[:-1]:
      plotLine(lps, opts)

    plotLine(lines[-1], '--r')

    plt.show()



plotLine(((-12.181763716305491,-2710.3259369878965),(256.56356236007434,19.75598363917952)))
plotLine(((-3.818033064733611,5.360199085809186),(23.30621946089043,6.859824644570518)))
plt.show()
