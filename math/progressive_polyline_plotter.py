import matplotlib.pyplot as plt

pl = (
    (1, 2),
    (1, 1),
    (0, 0),
    (4, 0),
    (0, 4),
    (0, 0),
    (1, 1),
    (2, 1),
    (1, 2),
)

pll = pl[1:]


for c in range(1, len(pl)):
    for (_, p0, p1) in zip(range(c), pl, pll):
        plt.plot([p0[0], p1[0]], [p0[1], p1[1]], 'b')

    plt.plot([p0[0], p1[0]], [p0[1], p1[1]], 'r')

    plt.show()
