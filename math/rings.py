import matplotlib.pyplot as plt
from itertools import chain, cycle


def xy(ps): return [p[0] for p in ps], [p[1] for p in ps]

def plotRing(ps, opts='-b'):
  ps = tuple(chain([ps[-1]], ps))
  plt.plot(*xy(ps), opts)

def plotEndpoints(ps, opts='b'):
  plt.plot(*xy([ps[0]]), 'x' + opts)
  plt.plot(*xy([ps[-1]]), 'o' + opts)

def plotPoint(p, color='b', opts='o'):
  plt.plot([p[0]], [p[1]], opts + color)

def plotSegm(ps, color='b', opts='-'):
  plt.plot(*xy(ps), opts + color)

def plotSegms(ss, color='b', opts='-'):
  for s in ss:
    plotSegm(s, color=color, opts=opts)

def plotLine(ps, color='b', opts='-'):
  plotSegm(ps, color=color, opts=opts)
  [p0, p1] = ps
  plotPoint(p0, color=color, opts='x' + opts)
  plotPoint(p1, color=color, opts='o' + opts)

def plotPolyline(ps, opts='-', color='b'): plt.plot(*xy(ps), opts + color)

colors = 'b', 'g', 'r', 'c', 'm', 'y', 'k'









rs = [
 [(0,0),(3,0),(3,3),(0,3),(1,1),(1,2),(2,2),(2,1),(1,1),(0,3)],
 [(3,0),(3,3),(0,3),(1,1),(1,2),(2,2),(2,1),(1,1),(0,3),(0,0)],
 [(3,3),(0,3),(1,1),(1,2),(2,2),(2,1),(1,1),(0,3),(0,0),(3,0)],
 [(0,3),(1,1),(1,2),(2,2),(2,1),(1,1),(0,3),(0,0),(3,0),(3,3)],
 [(1,1),(1,2),(2,2),(2,1),(1,1),(0,3),(0,0),(3,0),(3,3),(0,3)],
 [(1,2),(2,2),(2,1),(1,1),(0,3),(0,0),(3,0),(3,3),(0,3),(1,1)],
 [(2,2),(2,1),(1,1),(0,3),(0,0),(3,0),(3,3),(0,3),(1,1),(1,2)],
 [(2,1),(1,1),(0,3),(0,0),(3,0),(3,3),(0,3),(1,1),(1,2),(2,2)],
 [(1,1),(0,3),(0,0),(3,0),(3,3),(0,3),(1,1),(1,2),(2,2),(2,1)],
 [(0,3),(0,0),(3,0),(3,3),(0,3),(1,1),(1,2),(2,2),(2,1),(1,1)]]

for r in rs:
  plotRing(r, '--')
  plotPoint(r[0])
  plt.show()



# c = [(0,0), (2,0), (2,1), (1,1), (1,2), (2,2), (2,3), (0,3)]

# ts = [
# ((0,0),(2,0),(2,1)),
# ((1,2),(2,2),(2,3)),
# ((0,0),(2,1),(1,1)),
# ((1,2),(2,3),(0,3)),
# ((0,3),(0,0),(1,1))]

# # plotRing(c)

# for t in ts:
#   plotRing(t, '--')

# plt.show()
