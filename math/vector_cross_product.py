import matplotlib.pyplot as plt
from math import sin, cos, atan2, pi
from more_itertools import numeric_range

def cross_product(u, v):
  [u_x, u_y] = u
  [v_x, v_y] = v
  return (u_x * v_y) - (v_x * u_y)

baseAngle = 0
# baseAngle = atan2(basePoint[1], basePoint[0])
basePoint = cos(baseAngle), sin(baseAngle)

theta = tuple(numeric_range(baseAngle, baseAngle + 2 * pi, pi / 50))
# theta_perp = tuple(t + pi / 2 for t in theta)
theta_perp = tuple(numeric_range(baseAngle, baseAngle + 2 * pi, pi / 50))

cp = lambda t: cross_product(basePoint, (cos(t), sin(t)))

cross_product = tuple(map(cp, theta))
cross_product_perp = tuple(map(cp, theta_perp))


# a = tuple(numeric_range(baseAngle, baseAngle + 2 * pi, pi / 50))
# theta_perp = iter(t + pi / 2 for t in a)p
# cross_product_perp = tuple(cross_product(basePoint, (cos(t), sin(t))) for t in theta)


plt.plot(theta, cross_product)
plt.show()
