{-# LANGUAGE GeneralizedNewtypeDeriving
           , MultiParamTypeClasses
           , StandaloneDeriving
           , DataKinds
           , NoImplicitPrelude
           , InstanceSigs
           , DeriveFunctor
           , FlexibleInstances
           , UndecidableInstances
           , DataKinds #-}

module Angle where

import Dimension

import qualified Normalized as Norm
import Normalized
import IsClose (IsClose)

import Utilities (Zero(zero))

import GHC.Real (Real)
import Control.Applicative (Applicative)
import Data.Eq
import Data.Function
import Data.Functor
import Data.Ord
import GHC.Float (Floating)
import GHC.Num hiding (negate)
import GHC.Show

import qualified GHC.Float


data Angle
type Aa a = Absolute Angle a
type Ra a = Relative Angle 1 a

data AngleClass = Acute | Obtuse | Reflex deriving (Eq)
data Clock = CCW | CW

nHalfCircle :: (Applicative r, Real a, NormalizedBounded (r a), Ord (r a)) => r a -> HalfCircle (r a)
nHalfCircle = HalfCircle . getNormalizedLU . normalizeLU
newtype HalfCircle a = HalfCircle {getHalfCircleAngle :: a}
  deriving (Show, Eq, Ord, IsClose, ABS, Zero, IsNaN)
instance (Scalar a, Functor a, Floating b, Zero (a b)) => NormalizedBounded (HalfCircle (a b)) where
  lowerBound = HalfCircle $ zero
  upperBound = HalfCircle $ pi

nPiPi :: (Applicative r, Real a, NormalizedBounded (r a), Ord (r a)) => r a -> PiPi (r a)
nPiPi = PiPi . getNormalizedLU . normalizeLU
newtype PiPi a = PiPi {getPiPi :: a}
  deriving (Show, Eq, Ord, IsClose, ABS, Zero, IsNaN)
instance (Scalar a, Functor a, Floating b, Zero b) => NormalizedBounded (PiPi (a b)) where
  lowerBound = PiPi $ piN
  upperBound = PiPi $ pi
-- instance Scalar PiPi where
--   toScalar = toScalar . getPiPi
--   fromScalar = PiPi . fromScalar

-- normalize :: (Scalar a, NormalizedBounded a, NormalizedBounded a)
--           => a -> NormalizedLU a
-- normalize = normalizeDiffBounds lowerBound upperBound
-- 
-- normalizeDiffBounds :: (Scalar a, NormalizedBounded a)
--                     => a -> a -> a -> NormalizedLU a
-- normalizeDiffBounds l u x =
--   NormalizedLU $ fromScalar $ getNormalizedLU $
--   Norm.normalizeLUDiffBounds (toScalar l) (toScalar u) (toScalar x)

pi :: (Scalar a, Floating b) => a b
pi = fromScalar GHC.Float.pi

piN :: (Scalar a, Functor a, Floating b, Num b) => a b
piN = negate pi

twoPi :: (Scalar a, Functor a, Floating b, Num b) => a b
twoPi = 2 ~*= pi

-- instance (Zero a, Floating a) => NormalizedBounded (Ra a) where
--   lowerBound = zero
--   upperBound = twoPi

