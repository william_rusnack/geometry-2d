{-# LANGUAGE GeneralizedNewtypeDeriving
           , MultiParamTypeClasses
           , InstanceSigs #-}

module Arc where

import Dimension
  ( Relative(Relative)
  , RelativeAddition((=-=), negate)
  , RelativeMultiplication((=/=))
  , Distance
  , (~*=), (=/~)
  , abs
  , fromScalar
  )
import qualified Trig as T
import Angle
  ( Ra
  , pi
  , twoPi
  , getHalfCircleAngle
  )
import Vector
  ( NZVector
  , Unit(Unit)
  , vecAngle
  , Magnitude(mag)
  , parallel
  , perp
  , xp
  , setLength
  , rotate
  , _toNZV, toUv
  , reverse
  )
import PLS
  ( (.+->)
  , (.->.)
  , (..-)
  , Point
  , Segm
  , TailHeadPoints(tp, hp)
  )
-- import Polyline (ApproxAsPolyline(approxAsPolyline))

import Utilities (zero)
import Ratio (getNZRatio)
import Normalized (NormalizedLU(NormalizedLU), getNormalizedLU)

import Test.QuickCheck
  ( Arbitrary(arbitrary)
  , suchThat
  , Gen
  )

import Prelude hiding (negate, abs, pi, reverse)

import Data.Maybe (fromJust)


-- start point, endpoint, start tangent
newtype ArcEsT = ArcEsT (Point, Point, Unit)
                 deriving (Show, Eq)
nArcEsT :: Point -> Point -> Unit -> Maybe ArcEsT
nArcEsT sp ep u
  | sp == ep || (sp .->. ep) `parallel` u = Nothing
  | otherwise = Just $ ArcEsT (sp, ep, u)

type Radius = Distance

type Center = Point


class ArcOps a where
  radius :: a -> Radius
  angle :: a -> NormalizedLU Ra
  chord :: a -> Segm -- segment connecting arc endpoints
  chordVec :: a -> NZVector
  center :: a -> Center
  tangent :: a -> Unit


instance ArcOps ArcEsT where
  radius :: ArcEsT -> Radius
  radius a = fromScalar $ numer =/= denom
    where numer = mag $ chord a
          denom = Relative $ 2 * T.cos b
          b = (pi =-= getNormalizedLU (angle a)) =/~ 2

  angle :: ArcEsT -> NormalizedLU Ra
  angle a@(ArcEsT (_,_,u)) =
    NormalizedLU $ 2 ~*= (getHalfCircleAngle $ getNormalizedLU $ vecAngle (chordVec a) u)

  chord :: ArcEsT -> Segm
  chord (ArcEsT (sp, ep, _)) =
    case sp ..- ep of
    Just s -> s
    Nothing -> error "use nArcEsT to construct your arcs instead of ArcEsT"

  chordVec :: ArcEsT -> NZVector
  chordVec = _toNZV . chord

  center :: ArcEsT -> Center
  center a@(ArcEsT (sp,_,t@(Unit t'))) = sp .+-> pv
    where pv = setLength r (perp t)
          r | t' `xp` chordVec a > zero = radius a
            | otherwise              = negate $ radius a

  tangent :: ArcEsT -> Unit
  tangent (ArcEsT (_, _, u)) = u


instance TailHeadPoints ArcEsT where
  tp (ArcEsT (p, _, _)) = p
  hp (ArcEsT (_, p, _)) = p


-- instance ApproxAsPolyline ArcEsT where
--   approxAsPolyline :: Distance -- tolerance
--              -> ArcEsT
--              -> Polyline
--   approxAsPolyline t a@(ArcEsT (sp, ep, tv))
--     | t <= 0 = error "tolerance needs to be > 0"
--     | otherwise = Polyline $ sp :
--         foldr (\ i a' -> convAng i : a')
--               [ep]
--               [1..n-1]
--     where
--           ia = aa =/~ n
--           n = fromIntegral (ceiling (aa =/= (2 ~*= acos (1 - t =/= r))) :: Int)
--           aa = if sv `xp` tv > 0 then angle a
--                                  else negate $ angle a
--           r = radius a

--           sa = ang sv
--           sv = cp .->. sp
--           cp = center a

--           convAng :: Double -> Point
--           convAng = ((.+->) cp) . (setLength r) . _toUv . (=+. sa) . (~*= ia)


-- Arbitrary Helpers --

instance Arbitrary ArcEsT where
  arbitrary = do
    p0 <- arbitrary
    p1 <- suchThat arbitrary (/= p0)
    u <- suchThat arbitrary (not . parallel (p0 .->. p1))
    return $ ArcEsT (p0, p1, u)

newtype ArcRadiusAngleCenter = ArcRadiusAngleCenter
  {getArcRadiusAngleCenter :: (ArcEsT, Radius, NormalizedLU Ra, Center)}
  deriving Show
instance Arbitrary ArcRadiusAngleCenter where
  arbitrary = do
    c <- arbitrary
    v <- arbitrary :: Gen NZVector

    let p0 = c .+-> v

    a <- suchThat ((~*= twoPi) . ((-) 1) . (* 2) . getNZRatio <$> arbitrary)
                  (/= pi =/~ 2) :: Gen Ra

    let p1 = c .+-> rotate a v

    let Just t = toUv $ (if a > zero then id else reverse) $ perp v

    return $ ArcRadiusAngleCenter ( fromJust $ nArcEsT p0 p1 t
                                  , mag v
                                  , NormalizedLU $ abs a
                                  , c )
