{-# LANGUAGE DeriveGeneric #-}

module DSegm where

import PLS
  ( (->->)
  , (-><-)
  , (<-->)
  , (<->)
  , (..-)
  , Point
  , Segm
  , TailHeadPoints(tp, hp, thps')
  , Intersect(intersects, interiorIntersect, ininIntersect)
  , Reverse(reverse)
  )
import SegmArbitrary (getInteriorIntersectingSegms)

import Utilities (t3to2, bimap')

import Unified.Collections.List (List(List))
import Unified.Utilities.List (allCombo'')

import Data.Maybe (fromJust)

import Test.QuickCheck (Arbitrary(arbitrary), suchThat)

import Generic.Random (genericArbitraryU)
import GHC.Generics (Generic)

import Prelude hiding (reverse)


data DSegm = DSegm {setNP :: NumeralPrefix, getSegm :: Segm}
  deriving (Show, Eq, Ord)

nDS :: NumeralPrefix -> Point -> Point -> Maybe DSegm
nDS np p0 p1 = DSegm np <$> p0 ..- p1

nDS' :: NumeralPrefix -> (Point, Point) -> Maybe DSegm
nDS' np ps = uncurry (nDS np) $ ps

nDS'' :: NumeralPrefix -> Point -> Point -> DSegm
nDS'' np p0 p1 = fromJust $ nDS np p0 p1

nDS''' :: NumeralPrefix -> (Point, Point) -> DSegm
nDS''' np ps = uncurry (nDS'' np) ps

data NumeralPrefix = Uni -- Uni-directional
                   | Bi -- Bi-directional
                   | Pre -- pre-Bi
                   | Pst -- post-Bi
                   | PPB -- pre & post Bi
                   deriving (Show, Eq, Ord, Generic)

npAllowedOrders :: List (NumeralPrefix, NumeralPrefix)
npAllowedOrders = List
                  [ (Uni, Uni)
                  , (Uni, Pre)
                  , (Pre, Bi)
                  , (Bi, Pst)
                  , (Pst, Uni)
                  , (Pst, Pre)
                  , (Bi, PPB)
                  , (PPB, Bi) ]

reverseD :: DSegm -> Maybe DSegm
reverseD (DSegm Bi s) = Just $ DSegm Bi $ reverse s
reverseD _ = Nothing -- Only Bi DLines should be reversed

instance Reverse DSegm where
  reverse (DSegm d s) = DSegm d $ reverse s


instance Intersect DSegm where
  intersects (DSegm np0 sInit0) (DSegm np1 sInit1) = sInit0 `intersects` sInit1 &&
                                           not (i (np0, sInit0) (np1, sInit1))
    where i :: (NumeralPrefix, Segm) -> (NumeralPrefix, Segm) -> Bool

          i (Pre, s0) (Pst, s1)     = s0 ->-> s1 && nII
          i n0@(Pst, _) n1@(Pre, _) = i n1 n0

          i (Pre, s0) (PPB, s1)     = s0 ->-> s1 && nII
          i n0@(PPB, _) n1@(Pre, _) = i n1 n0

          i (Pst, s0) (PPB, s1)     = s1 ->-> s0 && nII
          i n0@(PPB, _) n1@(Pst, _) = i n1 n0

          i (Bi, s0) (Pre, s1)      = s0 -><- s1 && nII
          i n0@(Pre, _) n1@(Bi, _)  = i n1 n0

          i (Bi, s0) (Pst, s1)      = s0 <--> s1 && nII
          i n0@(Pst, _) n1@(Bi, _)  = i n1 n0

          i (Bi, s0) (Bi, s1) = s0 <-> s1 ||
                                ( ( any (uncurry (==)) $
                                    uncurry allCombo'' $
                                    bimap' thps' (s0, s1) ) &&
                                  nII )

          i d@(Bi, _) (PPB, s) = (i d (Pre, s) || i d (Pst, s)) && nII
          i n0@(PPB, _) n1@(Bi, _) = i n1 n0

          i _ _ = False

          nII = not $ sInit0 `interiorIntersect` sInit1

  interiorIntersect (DSegm Bi s0) (DSegm Bi s1) = not (s0 <-> s1) &&
                                                  s0 `interiorIntersect` s1
  interiorIntersect (DSegm _ s0) (DSegm _ s1) = s0 `interiorIntersect` s1

  -- untested and not sure if this is the right implimentation
  ininIntersect (DSegm Bi s0) (DSegm Bi s1) = not (s0 <-> s1) &&
                                              s0 `ininIntersect` s1
  ininIntersect (DSegm _ s0) (DSegm _ s1) = s0 `ininIntersect` s1

instance TailHeadPoints DSegm where
  tp (DSegm _ s) = tp s
  hp (DSegm _ s) = hp s


-- Arbitrary Helpers --

instance Arbitrary NumeralPrefix where
  arbitrary = genericArbitraryU

newtype InteriorIntersectsDSegm = InteriorIntersectsDSegm
  {getInteriorIntersectsDSegm :: (DSegm, DSegm)}
  deriving Show
instance Arbitrary InteriorIntersectsDSegm where
  arbitrary = do
    np0 <- arbitrary
    np1 <- arbitrary

    let ag = t3to2 . getInteriorIntersectingSegms <$> arbitrary
    (s0, s1) <- if np0 == Bi && np1 == Bi
                then suchThat ag (uncurry (/=) . fmap reverse)
                else ag

    return $ InteriorIntersectsDSegm (DSegm np0 s0, DSegm np1 s1)
