{-# LANGUAGE GeneralizedNewtypeDeriving
           , TypeApplications
           , DeriveFunctor
           , FlexibleInstances
           , FlexibleContexts
           , NoImplicitPrelude
           , ScopedTypeVariables
           , DataKinds
           , TypeOperators
           , KindSignatures
           #-}

module Dimension where

import IsClose (IsClose)
import Utilities
import Utilities.Arbitrary (AddNoise)

import Control.Applicative (Applicative, liftA2)
import Data.Bool
import Data.Eq
import Data.Function
import Data.Functor
import Data.Ord
import Data.Tuple
import GHC.Float (Floating, Double)
import GHC.Num (Num)
import GHC.Real (Fractional, fromIntegral, floor, RealFrac)
import GHC.Show
import GHC.TypeNats
import Test.QuickCheck (Arbitrary, NonNegative)

import qualified Data.Int
import qualified GHC.Float
import qualified GHC.Num
import qualified GHC.Real


newtype Absolute d a = Absolute {getAbsolute :: a}
  deriving (Eq, Ord, IsClose, Arbitrary, Zero, Show, AddNoise, Functor)

newtype Relative d (e :: Nat) a = Relative {getRelative :: a}
  deriving (Eq, Ord, Arbitrary, IsClose, Zero, Show, ABS, IsNaN, AddNoise, Functor)

data X
data Y
data Z
data D

type Ax = Absolute X Double
type Ay = Absolute Y Double
type Az = Absolute Z Double

type Rx = Relative X 1 Double
type Ry = Relative Y 1 Double
type Rz = Relative Z 1 Double
type Distance = Relative D 1 (NonNegative Double)

infixl 6 .+=
(.+=) :: Num a => Absolute d a -> Relative d 1 a -> Absolute d a
(.+=) = curry $ Absolute . biLift' (GHC.Num.+) getAbsolute getRelative

infixl 6 =+.
(=+.) :: Num a => Relative d 1 a -> Absolute d a -> Absolute d a
(=+.) = flip (.+=)

(.-.) :: Num a => Absolute d a -> Absolute d a -> Relative d 1 a
(.-.) = curry $ Relative . biLiftC' (GHC.Num.-) getAbsolute

infixl 6 .-.
(.-=) :: Num a => Absolute d a -> Relative d 1 a -> Absolute d a
(.-=) = curry $ Absolute .  biLift' (GHC.Num.-) getAbsolute getRelative

_toAbsolute :: Relative d 1 a -> Absolute d a
_toAbsolute = Absolute . getRelative

_toRelative :: Absolute d a -> Relative d 1 a
_toRelative = Relative . getAbsolute

mod :: (Applicative f, Fractional a, RealFrac a, Num a) => f a -> f a -> f a
mod = liftA2 (\x y -> x GHC.Num.- y GHC.Num.* fromIntegral @Data.Int.Int (floor (x GHC.Real./ y)))

-- class FConv a b where fConv :: a -> b
-- instance FConv (Relative a c) (Relative b c) where fConv = Relative . getRelative
-- instance FConv (Absolute a c) (Absolute b c) where fConv = Absolute . getAbsolute

infixl 6 =+=
(=+=) :: forall d (e :: Nat) a. Num a => Relative d e a -> Relative d e a -> Relative d e a
(=+=) (Relative r0) (Relative r1) = Relative (r0 GHC.Num.+ r1)

infixl 6 =-=
(=-=) :: forall d (e :: Nat) a. Num a => Relative d e a -> Relative d e a -> Relative d e a
(=-=) x y = x =+= (negate y)

negate :: forall d (e :: Nat) a. Num a => Relative d e a -> Relative d e a
negate = fmap GHC.Num.negate

infixl 7 =*=
(=*=) :: forall d (e0 :: Nat) (e1 :: Nat) a. Num a => Relative d e0 a -> Relative d e1 a -> Relative d (e0 + e1) a
(=*=) (Relative r0) (Relative r1) = Relative (r0 GHC.Num.* r1)

sq :: forall d (e :: Nat) a. Num a => Relative d e a -> Relative d (e + e) a
sq r = r =*= r

infixl 7 =/=
(=/=) :: forall d (e0 :: Nat) (e1 :: Nat) a. Fractional a => Relative d e0 a -> Relative d e1 a -> Relative d (e0 - e1) a
(=/=) (Relative r0) (Relative r1) = Relative $ r0 GHC.Real./ r1

sqrt :: forall d e a. Floating a => Relative d e a -> Relative d (Div e 2) a
sqrt = Relative . GHC.Float.sqrt . getRelative

infixl 7 =/~
(=/~) :: (Functor f, Fractional a) => f a -> a -> f a
(=/~) v s = (GHC.Real./ s) <$> v

infixl 7 =*~
(=*~) :: (Functor f, Num a) => f a -> a -> f a
(=*~) v s = (GHC.Num.* s) <$> v

infixl 7 ~*=
(~*=) :: (Functor f, Num a) => a -> f a -> f a
(~*=) = flip (=*~)

class ABS a where abs :: a -> a
instance ABS Double where abs = GHC.Num.abs

class IsNaN a where isNaN :: a -> Bool
instance IsNaN Double where isNaN = GHC.Float.isNaN

class Scalar a where
  toScalar :: a b -> b
  fromScalar :: b -> a b

instance Scalar (Absolute d) where
  toScalar = getAbsolute
  fromScalar = Absolute

instance Scalar (Relative d e) where
  toScalar = getRelative
  fromScalar = Relative

