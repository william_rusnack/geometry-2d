{-# LANGUAGE GeneralizedNewtypeDeriving
           , InstanceSigs
           , MultiParamTypeClasses #-}

module GeometryClasses where

class BiIntersect a b where
  -- biIntersect :: a -> b -> Bool
  -- biIntersect' (a, b) -> Bool
  -- biIntersect' = uncurry biIntersect

  biInteriorIntersect :: a -> b -> Bool
  biInteriorIntersect' :: (a, b) -> Bool
  biInteriorIntersect' = uncurry biInteriorIntersect


class Contain a b where
  -- interior and edges
  contains :: a -> b -> Bool
  contains = curry contains'

  contains' :: (a, b) -> Bool
  contains' = uncurry contains

  -- interior but not edges
  interiorContains :: a -> b -> Bool
  interiorContains = curry interiorContains'

  interiorContains' :: (a, b) -> Bool
  interiorContains' = uncurry interiorContains

class SelfIntersecting a where
  selfIntersecting :: a -> Bool
