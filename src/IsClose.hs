module IsClose where

-- checks if values are absolutely and/or relatively close to each other
absErr :: Double
absErr = 1e-9
relErr :: Double
relErr = 1e-9 -- if equal to absErr, absolute dominates range (-1, 1)


infix 4 `isClose`
infix 4 `isCloseNZ`
infix 4 `isCloseAR`
infix 4 `isCloseR`
infix 4 `isCloseRO`
class IsClose a where
  -- checks if values are close from floating point error
  isClose :: a -> a -> Bool
  isClose = isCloseAR absErr relErr

  isClose' :: (a, a) -> Bool
  isClose' = uncurry isClose

  -- check if values are close when near zero
  isCloseNZ :: a -> a -> Bool
  isCloseNZ = isCloseA absErr

  isCloseNZ' :: (a, a) -> Bool
  isCloseNZ' = uncurry isCloseNZ

  isCloseAR :: Double -- absolute difference
            -> Double -- relative difference
            -> a      -- value
            -> a      -- value
            -> Bool
  isCloseAR a r n n' = isCloseA a n n' || isCloseR r n n'

  isCloseAR' :: Double -> Double -> (a, a) -> Bool
  isCloseAR' a r = uncurry $ isCloseAR a r

  isCloseA :: Double -- absolute difference
           -> a      -- value
           -> a      -- value
           -> Bool

  isCloseA'  :: Double -> (a, a) -> Bool
  isCloseA' a = uncurry $ isCloseA a

  isCloseR :: Double -- relative difference
           -> a      -- value
           -> a      -- value
           -> Bool
  isCloseR r n n' = isCloseRO r n n' || isCloseRO r n' n

  isCloseR'  :: Double -> (a, a) -> Bool
  isCloseR' r = uncurry $ isCloseR r

  isCloseRO :: Double -- relative difference
           -> a      -- value
           -> a      -- value
           -> Bool

  isCloseRO'  :: Double -> (a, a) -> Bool
  isCloseRO' r = uncurry $ isCloseRO r

instance IsClose Double where
  isCloseAR a r n n' = diff <= a || (diff / denom) <= r
    where diff = abs (n - n')
          denom = max (abs n) (abs n')

  isCloseA a n n' = diff <= a
    where diff = abs (n - n')

  -- no precidence of n or n' for range
  -- not good for large r values
  isCloseR r n n' = (abs (n - n') / denom) <= r
    where denom = max (abs n) (abs n')

  -- relative range set from n not n' (unlike isCloseR)
  -- use if you want to make sure n' is close to n and not n close to n'
  isCloseRO r n n' = abs ((n - n') / n) <= r

instance IsClose a => IsClose (Maybe a) where
  isCloseA a (Just x) (Just y) = isCloseA a x y
  isCloseA _ Nothing Nothing = True
  isCloseA _ _ _ = False

  isCloseRO r (Just x) (Just y) = isCloseRO r x y
  isCloseRO _ Nothing Nothing = True
  isCloseRO _ _ _ = False
