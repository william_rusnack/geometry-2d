module LineArbitrary where

import PLS
  ( Point
  , TailHeadPoints(tp, hp)
  , From2Points
  , (.+->)
  , (..-)
  , Reverse(reverse)
  )

import Dimension
  ( (=*~)
  , Distance
  , Squared
  )
import Angle (pi, piN)
import Vector
  ( Magnitude(magSqrd)
  , magSqrd
  , NZVector
  , parallel
  , perp
  , rotate
  , ToVec(toVec)
  , Vector
  , ToUv
  )
import Ratio (NZRatio(NZRatio))

import Utilities (zero)
import Utilities.Arbitrary (NonEqual(NonEqual))

import Data.Bitraversable (bisequenceA)
import Data.Tuple.HT (thd3, swap)

import Test.QuickCheck
  ( Arbitrary(arbitrary)
  , Gen
  , NonZero(NonZero)
  , Positive(Positive)
  , suchThat
  )
import Unified.QuickCheck
  ( elements
  , oneof
  )

import Prelude hiding (map, negate, reverse, pi, sqrt)


newtype NonParallel a = NonParallel
  {getNonParallel :: (a, a)}
  deriving Show
instance ( TailHeadPoints a
         , ToVec a
         , Arbitrary a
         , From2Points a ) => Arbitrary (NonParallel a) where
  arbitrary = do
    l0 <- arbitrary
    v0 <- (arbitrary :: Gen Vector)
    let p0 = tp l0 .+-> v0
    v1 <- suchThat arbitrary (\v -> v /= v0 && p0 /= hp l0 .+-> v)
    let Just l1 = p0 ..- (hp l0 .+-> v1)
    return $ NonParallel (l0, l1)

newtype ParallelLines a = ParallelLines
  {getParallelLines :: (a, a, Squared Distance)}
  deriving Show
instance ( TailHeadPoints a
         , ToVec a
         , Arbitrary a
         , From2Points a ) => Arbitrary (ParallelLines a) where
  arbitrary = do
    LinePoint (l0, p0, d) <- arbitrary

    NonZero f <- arbitrary
    let p1 = p0 .+-> toVec l0 =*~ f
    let Just l1 = p0 ..- p1

    return $ ParallelLines (l0, l1, d)


newtype LinePoint a = LinePoint
  {getLinePoint :: (a, Point, Squared Distance)}
  deriving Show
instance (TailHeadPoints a, ToVec a, Arbitrary a) =>
         Arbitrary (LinePoint a) where
  arbitrary = do
    l <- arbitrary

    f0 <- arbitrary
    let v = toVec l
    let vl = v =*~ f0
    f1 <- arbitrary
    let vp = toVec $ perp v =*~ f1
    let p = tp l .+-> vl .+-> vp

    return $ LinePoint (l, p, magSqrd vp)

-- point does not lay on line
-- includes squared length from line
newtype LinePointDisjoint a = LinePointDisjoint
  {getLinePointDisjoint :: (a, Point, Squared Distance)}
  deriving Show
instance (TailHeadPoints a, ToVec a, Arbitrary a) =>
         Arbitrary (LinePointDisjoint a) where
  arbitrary = LinePointDisjoint . getLinePoint <$>
              (suchThat arbitrary $ (/= zero) . thd3 . getLinePoint)


newtype CollinearLines a = CollinearLines
  {getCollinearLines :: (a, a)}
  deriving Show
instance ( Arbitrary a
         , TailHeadPoints a
         , ToVec a
         , From2Points a ) => Arbitrary (CollinearLines a) where
  arbitrary = do
    l0 <- arbitrary

    let p = tp l0
    let v = toVec l0
    NonEqual (NonZero f0, NonZero f1) <- arbitrary
    let Just l1 = (p .+-> v =*~ f0) ..- (p .+-> v =*~ f1)

    return $ CollinearLines (l0, l1)

newtype NonCollinearLines a = NonCollinearLines
  {getNonCollinearLines :: (a, a)}
  deriving Show
instance ( ToUv a
         , ToVec a
         , TailHeadPoints a
         , Arbitrary a
         , From2Points a
         , Eq a ) => Arbitrary (NonCollinearLines a) where
  arbitrary = oneof $ fmap (fmap NonCollinearLines) [
      (\(x, y, _) -> (x, y)) . getParallelNotCollinearLines <$> arbitrary
    , getNonCollinearRandomLines <$> arbitrary
    ]

newtype ParallelNotCollinearLines a = ParallelNotCollinearLines
  {getParallelNotCollinearLines :: (a, a, Squared Distance)}
  deriving Show
instance ( ToUv a
         , ToVec a
         , TailHeadPoints a
         , Arbitrary a
         , From2Points a ) => Arbitrary (ParallelNotCollinearLines a) where
  arbitrary = ParallelNotCollinearLines . getParallelLines <$>
              (suchThat arbitrary $ (/= zero) . thd3 . getParallelLines)

newtype NonCollinearRandomLines a = NonCollinearRandomLines
  {getNonCollinearRandomLines :: (a, a)}
  deriving Show
instance ( From2Points a
         , ToVec a
         , Eq a
         , Arbitrary a
         , ToUv a
         , TailHeadPoints a ) => Arbitrary (NonCollinearRandomLines a) where
  arbitrary = do
    l0 <- arbitrary

    let v0 = toVec l0
    v1 <- arbitrary :: Gen Vector
    v1' <- if v0 `parallel` v1 || v1 == zero
           then suchThat arbitrary $ not . parallel v0
           else arbitrary :: Gen NZVector
    let p0 = tp l0 .+-> v1
    let Just l1 = p0 ..- (p0 .+-> v1')

    return $ NonCollinearRandomLines (l0, l1)

pointOnEachSide :: (TailHeadPoints a, ToVec a, Reverse a)
                => a -> Gen (Point, Point)
pointOnEachSide l = do
  let gp l' = (\(NZRatio r, Positive m) ->
                tp l' .+-> (rotate (pi =*~ r) (toVec l') =*~ m) ) <$>
              arbitrary

  let gs = (gp l, gp $ reverse l)
  bisequenceA =<< elements [gs, swap gs]

twoPointsOnOneSide :: (TailHeadPoints a, ToVec a, Reverse a)
                   => a -> Gen (Point, Point)
twoPointsOnOneSide l = do
  a <- elements [pi, piN]

  let v = toVec l
  let gp = (.+->) (tp l) .
           (\(NZRatio r, Positive m) -> rotate (a =*~ r) v =*~ m) <$>
           arbitrary
  p0 <- gp

  sequenceA (p0, suchThat gp (/= p0))
