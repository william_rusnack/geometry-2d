{-# LANGUAGE GeneralizedNewtypeDeriving
           , MultiWayIf
           , MultiParamTypeClasses
           , UndecidableInstances
           , FlexibleInstances #-}

module Normalized where

import GeometryClasses (Contain(contains, interiorContains))

import Range (Range(Range))
import Utilities (bimap', iff)
import Utilities.Arbitrary (chooseXU)

import Data.Bitraversable (bisequenceA)
import Data.Fixed (mod')
import Data.Tuple.HT (swap, double)
import System.Random (Random)
import Test.QuickCheck (Arbitrary(arbitrary))

-- a circular domain where the lowerBound is the same as the upperBound
-- lowerBound will always be used in place of upperBound
newtype Normalized a = Normalized {getNormalized :: a} deriving (Show, Eq, Ord)

normalize :: (NormalizedBounded a, Ord a, Num a, Real a) => a -> Normalized a
normalize = normalizeDiffBounds lowerBound upperBound

normalizeDiffBounds :: (Ord a, Num a, Real a) => a -> a -> a -> Normalized a
normalizeDiffBounds l u x = Normalized $
  if | l <= x && x < u -> x
     | x == u -> l
     | otherwise -> ((x - l) `mod'` (u - l)) + l

-- a circular domain where the lowerBound is different from the upperBound
-- lowerBound and upperBound are both valid values
newtype NormalizedLU a = NormalizedLU {getNormalizedLU :: a} deriving (Show, Eq, Ord)

normalizeLU :: (NormalizedBounded a, Ord a, Num a, Real a) => a -> NormalizedLU a
normalizeLU = normalizeLUDiffBounds lowerBound upperBound

normalizeLUDiffBounds :: (Ord a, Num a, Real a) => a -> a -> a -> NormalizedLU a
normalizeLUDiffBounds l u x = NormalizedLU $
  if l <= x && x <= u
    then x
    else ((x - l) `mod'` (u - l)) + l

class NormalizedBounded a where
  lowerBound :: a
  upperBound :: a

instance {-# OVERLAPPING #-} (NormalizedBounded a, Ord a) =>
         Contain (Range (Normalized a)) (Normalized a) where
  contains (Range l u) v = uncurry (if l < u then (&&) else (||)) (l <= v, v <= u)

  interiorContains (Range l u) v = uncurry (if l < u then (&&) else (||)) (l < v, v < u)


-- Arbitrary Instances --

instance ( Arbitrary a
         , NormalizedBounded a
         , Ord a
         , Num a
         , Real a
         ) => Arbitrary (Normalized a) where
  arbitrary = normalize <$> arbitrary

newtype NormalizedRangeLowerToUpper a = NormalizedRangeLowerToUpper
  {getNormalizedRangeLowerToUpper :: Range (Normalized a)}
  deriving Show
instance ( NormalizedBounded a
         , Arbitrary a
         , Ord a
         , Random a
         , Show a
         ) => Arbitrary (NormalizedRangeLowerToUpper a) where
  arbitrary = NormalizedRangeLowerToUpper .
              uncurry Range .
              -- warbler (\t -> if uncurry (>) t then swap else id) .
              iff (uncurry (>)) swap id .
              bimap' Normalized <$>
              ( bisequenceA $
                double $
                chooseXU lowerBound upperBound )

newtype NormalizedRangeUpperToLower a = NormalizedRangeUpperToLower
  {getNormalizedRangeUpperToLower :: Range (Normalized a)}
  deriving Show
instance ( NormalizedBounded a
         , Arbitrary a
         , Ord a
         , Random a
         , Show a
         ) => Arbitrary (NormalizedRangeUpperToLower a) where
  arbitrary = NormalizedRangeUpperToLower .
              uncurry Range .
              -- warbler (\t -> if uncurry (<) t then swap else id) .
              iff (uncurry (<)) swap id .
              bimap' Normalized <$>
              ( bisequenceA $
                double $
                chooseXU lowerBound upperBound )

