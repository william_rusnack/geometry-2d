{-# LANGUAGE FlexibleInstances
           , GeneralizedNewtypeDeriving
           , InstanceSigs
           , MultiParamTypeClasses
           , ScopedTypeVariables
           , ViewPatterns
           , PartialTypeSignatures
           , FlexibleContexts
           , TypeApplications #-}

module PLS where

import IsClose (IsClose(isClose, isCloseA, isCloseRO, isCloseAR))

import GeometryClasses
  ( BiIntersect(biInteriorIntersect)
  , Contain(contains, interiorContains)
  )

import Dimension
  ( (.+=)
  , (.-.)
  , (=*=)
  , (=*~)
  , (=/=)
  , (=/~)
  , _toRelative
  , Ax
  , Ay
  , Distance
  , fromScalar
  , toScalar
  , negate
  , Rx
  , Ry
  , sq
  , sqrt
  , Squared
  , Squared(Squared)
  , Relative
  , Absolute
  )
import Angle (Ra)
import Vector
  ( collinearV
  , GetDimension(getXY)
  , Magnitude(magSqrd)
  , magSqrd
  , parallel
  , perp
  , rotate
  , setLength
  , to1Dsq
  , ToNZV(_toNZV)
  , ToUv(_toUv)
  , ToVec(toVec)
  , Vector(Vector)
  , xp
  )
import Range (nRange, overlaps, interiorOverlap)

import Utilities
  ( bimap'
  , maybeBool
  , zero
  )
import Utilities.Arbitrary (AddNoise(arNoise, arNoiseUnbounded))

import Unified
  ( head
  , sortOn
  , fromFoldable
  )
import Unified.Utilities (take4)
import Unified.Collections.List (List)
import Unified.Utilities.List
  ( filterL
  , allCombo''
  )
import Unified.AtLeast (NonEmpty, AtLeast)

import Control.Applicative ((<|>), liftA2)
import Control.Arrow ((&&&))
import Control.Monad
import Data.Bifunctor (bimap)
import Data.Bitraversable (bisequenceA)
import Data.Maybe (isJust)
import Data.Tuple.HT (mapFst3, swap)
import Test.QuickCheck (Arbitrary(arbitrary), suchThat)

import Prelude hiding (map, negate, reverse, pi, sqrt, getLine, head, filter, concat)


-------------- Point --------------

newtype Point = Point {getPointCoords :: (Ax, Ay)} deriving (Eq, Arbitrary, Ord)

pt :: Double -> Double -> Point
pt x y = Point (fromScalar x, fromScalar y)

(.->.) :: Point -> Point -> Vector
(.->.) (Point (x0, y0)) (Point (x1, y1)) = Vector (x1 .-. x0, y1 .-. y0)
(.<-.) :: Point -> Point -> Vector
(.<-.) = flip (.->.)

infixl 6 .+->
-- (.+->) :: GetDimension a Relative => Point -> a -> Point
-- (.+->) (Point (px, py)) v = Point (px .+= getX v, py .+= getY v)
(.+->) :: GetDimension Relative a => Point -> a -> Point
(.+->) (getXY @Absolute -> (px, py))
       (getXY @Relative -> (vx, vy)) =
  Point (px .+= vx, py .+= vy)

avgPoint :: Foldable t => AtLeast n t Point -> Point
avgPoint = Point .
           (\(i, x, y) -> (x =/~ i, y =/~ i)) .
           mapFst3 fromIntegral .
           foldr (\(Point (x, y)) (i, tx, ty) -> ( i + 1
                                                 , tx .+= (_toRelative x :: Rx)
                                                 , ty .+= (_toRelative y :: Ry) ) )
                 (0 :: Int, zero, zero)

rotate :: Point -> Ra -> Point -> Point
rotate c a p = c .+-> Vector.rotate a (c .->. p)

class Translate a where
  translate :: Vector -> a -> a

class DistanceBetween a b where
  (|--|) :: a -> b -> Squared Distance
  (|--|) x y = sq (x |-| y)

  (|-|) :: a -> b -> Distance
  (|-|) x y = sqrt (x |--| y)


instance IsClose Point where
  isCloseAR a r (Point (x0, y0)) (Point (x1, y1)) = isCloseAR a r x0 x1 &&
                                                    isCloseAR a r y0 y1

  isCloseA a (Point (x0, y0)) (Point (x1, y1)) = isCloseA a x0 x1 &&
                                                 isCloseA a y0 y1

  isCloseRO a (Point (x0, y0)) (Point (x1, y1)) = isCloseRO a x0 x1 &&
                                                  isCloseRO a y0 y1

instance DistanceBetween Point Point where
  (|--|) p p' = magSqrd $ p .->. p'

instance Translate Point where
  translate = flip (.+->)

instance (Functor t, Translate a) => Translate (t a) where
  translate v = fmap (translate v)

instance Show Point where
  -- show (Point (Ax (Absolute x), Ay (Absolute y))) =
  --   "pt (" ++ show x ++ ") (" ++ show y ++ ")"
  show = (\(x, y) -> "pt (" ++ show x ++ ") (" ++ show y ++ ")") .
         bimap toScalar toScalar .
         getPointCoords


instance AddNoise Point where
  arNoise a r (Point (x, y)) =
    Point <$> bisequenceA (arNoise a r x, arNoise a r y)

  arNoiseUnbounded a r (Point (x, y)) =
    Point <$> bisequenceA (arNoiseUnbounded a r x, arNoiseUnbounded a r y)

instance Edges t => DistanceBetween (t Point) Point where
  (|--|) (edges -> es) p = minimum $ fmap (|--| p) es

instance (Edges a, Edges b) => DistanceBetween (a Point) (b Point) where
  (|--|) (edges -> es0) (edges -> es1) =
    minimum $
    fmap (uncurry (|--|)) $
    allCombo'' es0 es1

instance GetDimension Absolute Point where
  getXY = getPointCoords


-------------- Line --------------

newtype Line = Line {getLinePts :: (Point, Point)} deriving (Show, Eq, Ord)


collinearL :: ( ToUv a, ToUv b,
               TailHeadPoints a, TailHeadPoints b )
           => a -> b -> Bool
collinearL lp lq =
  lp `parallel` lq &&
  ( p == q ||
    lp `parallel` (p .->. q) )
  where p = tp lp
        q = tp lq
collinearL' :: ( ToUv a, ToUv b,
               TailHeadPoints a, TailHeadPoints b )
            => (a, b) -> Bool
collinearL' = uncurry collinearL

class From2Points a where
  (..-) :: Point -> Point -> Maybe a
  (..-) = curry f2p

  f2p :: (Point, Point) -> Maybe a
  f2p = uncurry (..-)

class TailHeadPoints a where
  tp :: a -> Point
  hp :: a -> Point

  thps :: a -> (Point, Point)
  thps x = (tp x, hp x)

  thps' :: a -> List Point
  thps' = fromFoldable . (\(t, h) -> [t, h]) . thps

class IntersectPoint a where
  intersectPoint :: a -> a -> Maybe Point
  intersectPoint = curry intersectPoint'

  intersectPoint' :: (a, a) -> Maybe Point
  intersectPoint' = uncurry intersectPoint

class ToRight a where
  toRight :: Distance -- offset distance
          -> a
          -> a

class Intersect a where
  -- interior interior, boundry interior, boundry boundry
  intersects :: a -> a -> Bool
  intersects = curry intersects'
  intersects' :: (a, a) -> Bool
  intersects' = uncurry intersects

  -- interior interior and boundry interior
  interiorIntersect :: a -> a -> Bool
  interiorIntersect = curry interiorIntersect'
  interiorIntersect' :: (a, a) -> Bool
  interiorIntersect' = uncurry interiorIntersect

  -- interior interior only
  ininIntersect :: a -> a -> Bool
  ininIntersect = curry ininIntersect'
  ininIntersect' :: (a, a) -> Bool
  ininIntersect' = uncurry ininIntersect

class Reverse a where
  reverse :: a -> a


instance {-# INCOHERENT #-} Edges t => Intersect (t Point) where
  intersects (edges -> es0) (edges -> es1) =
    any intersects' $ allCombo'' es0 es1

  interiorIntersect = intersects

  ininIntersect = interiorIntersect

instance From2Points Line where
  (..-) p0 p1 | p0 == p1 = Nothing
              | otherwise = Just $ Line (p0, p1)

instance IntersectPoint Line where
  intersectPoint lp lq
    | toVec lp `parallel` toVec lq = Nothing
    | isInfinite t   = Nothing
    | otherwise = Just (p .+-> r =*~ t)
    where t = ((q .<-. p) `xp` s) =/= (r `xp` s)
          r = toVec lp
          s = toVec lq
          p = tp lp
          q = tp lq

instance Contain Line Point where
  contains l p = v `parallel` (ltp .->. p) || ltp == p
    where v = _toNZV l
          ltp = tp l

  interiorContains = contains

instance Translate Line where
  translate v (Line (p, p')) = Line $ bimap' (.+-> v) (p, p')

instance Reverse Line where
  reverse = Line . swap . getLinePts

instance TailHeadPoints Line where
  tp (Line (p, _)) = p
  hp (Line (_, p)) = p

instance DistanceBetween Line Point where
  (|--|) l p = p |--| ip
    where Just ip = intersectPoint l l'
          l' = Line (p, p .+-> perp v)
          v = toVec l

instance DistanceBetween Line Line where
  (|--|) l0 l1 | l0 `parallel` l1 = l0 |--| tp l1
               | otherwise = zero

instance ToRight Line where
  toRight o l = translate (((setLength o) . negate . perp . _toUv) l) l

instance IsClose Line where
  isCloseA a l0 l1 = isCloseA a (tp l0) (tp l1) &&
                     isCloseA a (hp l0) (hp l1)

  isCloseRO r l0 l1 = isCloseRO r (tp l0) (tp l1) &&
                      isCloseRO r (hp l0) (hp l1)

instance ToVec Line where
  toVec = uncurry (.->.) . thps

instance Arbitrary Line where
  arbitrary = do
    p0 <- arbitrary
    p1 <- suchThat arbitrary (/= p0)
    return $ Line (p0, p1)



-------------- Segm --------------

newtype Segm = Segm {getLine :: Line}
  deriving (Show, Eq, Ord, Reverse, Arbitrary, TailHeadPoints, Translate, ToRight, From2Points, IsClose, ToVec, ToNZV, ToUv)


-- constructors --

-- nS :: Point -> Point -> Maybe Segm
-- nS p0 p1 = if p0 == p1
--            then Nothing
--            else Just $ nS'' p0 p1

-- nS' :: (Point, Point) -> Maybe Segm
-- nS' = uncurry nS

-- unsafe
nS'' :: Point -> Point -> Segm
nS'' = curry nS'''

-- unsafe
nS''' :: (Point, Point) -> Segm
nS''' = Segm . Line


-- functions --

intersectSegm :: Segm -> Segm -> Maybe Segm
intersectSegm s0 s1
  | s0 `collinearL` s1 && interiorOverlap r0 r1 =
    f2p .
    (\(_, p0, p1, _) -> (p0, p1)) =<<
    ( take4 $
      sortOn position $
      join $
      fmap thps' $
      fromFoldable
      [s0, s1] )
  | otherwise = Nothing
  where
        (r0, r1) = bimap' (uncurry nRange . bimap' position . thps) (s0, s1)
        position = to1Dsq v0 . (tp s0 .->.)
        v0 = _toNZV s0
intersectSegm' :: (Segm, Segm) -> Maybe Segm
intersectSegm' = uncurry intersectSegm

sharedEndpoint :: (TailHeadPoints a, TailHeadPoints b) => a -> b -> Maybe Point
sharedEndpoint s0 s1 = head $ filterL (`elem` thps' s0) $ thps' s1
sharedEndpoint' :: (TailHeadPoints a, TailHeadPoints b) => (a, b) -> Maybe Point
sharedEndpoint' = uncurry sharedEndpoint

sharesEndpoint :: (TailHeadPoints a, TailHeadPoints b) => a -> b -> Bool
sharesEndpoint = curry sharesEndpoint'
sharesEndpoint' :: (TailHeadPoints a, TailHeadPoints b) => (a, b) -> Bool
sharesEndpoint' = isJust . sharedEndpoint'

hasEndpoint :: TailHeadPoints a => a -> Point -> Bool
hasEndpoint s = liftA2 (||) (== tp s) (== hp s)

hasEndpoint' :: TailHeadPoints a => (a, Point) -> Bool
hasEndpoint' = uncurry hasEndpoint

otherEndpoint :: TailHeadPoints a => a -> Point -> Point
otherEndpoint (thps -> ps) p = if fst ps == p then snd ps
                                              else fst ps

-- head 2 tail
(->->) :: ( TailHeadPoints a
          , TailHeadPoints b
          , Contain a Point
          , Contain b Point )
       => a -> b -> Bool
(->->) s s' = hp s == tp s' &&
              not (s `contains` hp s') &&
              not (s' `contains` tp s)
-- head 2 head
(-><-) :: ( TailHeadPoints a
          , TailHeadPoints b
          , Contain a Point
          , Contain b Point )
       => a -> b -> Bool
(-><-) s s' = hp s == hp s' &&
              not (s `contains` tp s') &&
              not (s' `contains` tp s)
-- tail 2 tail
(<-->) :: ( TailHeadPoints a
          , TailHeadPoints b
          , Contain a Point
          , Contain b Point )
       => a -> b -> Bool
(<-->) s s' = tp s == tp s' &&
              not (s `contains` hp s') &&
              not (s' `contains` hp s)
-- reversed
(<->) :: ( TailHeadPoints a
          , TailHeadPoints b
          , Contain a Point
          , Contain b Point )
       => a -> b -> Bool
(<->) s s' = tp s == hp s' && tp s' == hp s

midpoint :: Segm -> Point
midpoint s = tp s .+-> (toVec s =/~ 2)

-- no intersect if endpoints points only touching interior
-- no intersect if parallel
-- only intersect if interiors cross
crossIntersect :: Segm -> Segm -> Bool
crossIntersect s s' =
  case intersectPoint s s' of
    Just ip -> notElem ip [hp s, tp s, hp s', tp s']
    Nothing -> False
crossIntersect' :: (Segm, Segm) -> Bool
crossIntersect' = uncurry crossIntersect

class Edges t where
  edges :: t Point -> NonEmpty List Segm


instance Intersect Segm where
  -- Reference: https://stackoverflow.com/a/565282/6933270
  -- Optimization Improvement: isOverlap checks both x and y. x could only be checked unless vertial and then y could be checked
  intersects :: Segm -> Segm -> Bool
  intersects s0 s1 = isJust (intersectPoint s0 s1) ||
                     (collinearL s0 s1 && isOverlap)
    where isOverlap = rxp `overlaps` rxq &&
                      ryp `overlaps` ryq

          rxp = nRange xp0 xp1
          ryp = nRange yp0 yp1

          rxq = nRange xq0 xq1
          ryq = nRange yq0 yq1

          (Point (xp0, yp0), Point (xp1, yp1)) = thps s0
          (Point (xq0, yq0), Point (xq1, yq1)) = thps s1

  interiorIntersect :: Segm -> Segm -> Bool
  interiorIntersect s0 s1 = maybeBool (nepP <$> intersectPoint s0 s1) ||
                            (isJust $ intersectSegm s0 s1)
    where nepP :: Point -> Bool
          nepP = liftA2 (||) (nepS s0) (nepS s1)

          nepS :: Segm -> Point -> Bool
          nepS s = not . hasEndpoint s

  ininIntersect' :: (Segm, Segm) -> Bool
  ininIntersect' ss =
    ( maybeBool $
      ( not . uncurry (||) . uncurry (&&&) (bimap' hasEndpoint ss) <$>
        intersectPoint' ss ) ) ||
    (isJust $ intersectSegm' ss)

instance BiIntersect Line Segm where
  biInteriorIntersect l s@(Segm l') =
    maybeBool $ contains s <$> intersectPoint l l'

-- only yields a point when intersect is in the interior or endpoint
-- does not yield the same results near the endpoints as IntersectPoint Line
instance IntersectPoint Segm where
  intersectPoint' :: (Segm, Segm) -> Maybe Point
  intersectPoint' ss =
    if isJust sep
      then if uncurry parallel ss
        then mfilter ( not .
                       uncurry (||) .
                       flip bimap' (ss, swap ss) .
                       (\p (s0, s1) -> s0 `contains` (s1 `otherEndpoint` p)) )
                     sep
        else sep
      else mfilter ( (uncurry (liftA2 (&&)) :: (Point -> Bool, Point -> Bool) -> Point -> Bool) $
                     (bimap' contains ss :: (Point -> Bool, Point -> Bool)) :: Point -> Bool )
                   ( ( head =<<
                       flip filterL (uncurry (<>) $ bimap' thps' ss) <$>
                       (isClose <$> ip) ) <|>
                     ip )


    where sep = uncurry sharedEndpoint ss
          ip = intersectPoint' $ bimap' getLine ss :: Maybe Point

instance Contain Segm Point where
  contains :: Segm -> Point -> Bool
  contains s p = _contains (>=) s p || tp s == p

  interiorContains :: Segm -> Point -> Bool
  interiorContains = _contains (>)

_contains :: (Squared Distance -> Squared Distance -> Bool) -> Segm -> Point -> Bool
_contains comp s p = sv `collinearV` pv &&
                     magSqrd sv `comp` pm &&
                     pm `comp` zero
  where sv = _toNZV s
        pv = tp s .->. p
        pm = magSqrd pv

instance Contain Segm Segm where
  contains s s' = all (contains s) $ thps' s'
  interiorContains s s' = all (interiorContains s) $ thps' s'

instance {-# OVERLAPS #-} DistanceBetween Segm Point where
  (|--|) :: Segm -> Point -> Squared Distance
  (|--|) (Segm l) p | tv `xp` perpV =*= hv `xp` perpV <= Squared (zero) = l |--| p
                    | otherwise = min (tp' |--| p) (hp' |--| p)
    where perpV = perp $ toVec l
          tv = tp' .->. p
          tp' = tp l
          hv = hp' .->. p
          hp' = hp l

instance DistanceBetween Segm Segm where
  (|--|) :: Segm -> Segm -> Squared Distance
  (|--|) s0 s1 | s0 `intersects` s1 = zero
               | otherwise = min (pd s0 s1) (pd s1 s0)
    where pd :: Segm -> Segm -> Squared Distance
          pd s s' = uncurry min $ bimap' ((|--|) s) (thps s')

instance Magnitude Segm where
  magSqrd :: Segm -> Squared Distance
  magSqrd = magSqrd . toVec
