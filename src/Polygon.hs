{-# LANGUAGE LambdaCase
           , ScopedTypeVariables
           , ViewPatterns
           , PartialTypeSignatures #-}

module Polygon where

import GeometryClasses (Contain(interiorContains))
import Angle (Clock(CW))
import Vector (between', xp)
import PLS
  ( Point
  , (.->.)
  , pt
  , ininIntersect'
  )
import Triangle (Triangle, nTriangle)
import Polyline (ToPolyline(toPolyline))
import Ring (Ring, Ring_(getRing))
import RingDL (r2rdl, rds2r, linkRings)

import Utilities (zero, (<$<), maybeBool)

import Unified
  ( reverse
  , lengthEq
  , Cons((<|), head)
  , Snoc((|>))
  , Drop(drop)
  )
import Unified.Utilities
  ( catMaybes
  , take3
  , take4
  )
import Unified.Collections.List (List(List))
import Unified.Utilities.List (rotationsL)
import Control.Applicative ((<|>), liftA2)
import Control.Arrow ((&&&))
import Data.Bifunctor (bimap)
import Data.Bitraversable (bisequenceA)
import Data.List (unfoldr)
import Unified.Collections.Set (Set)

import Prelude hiding (reverse, take, drop, last, head, tail, map)

import Debug.Trace


-- newtype Polygon = Polygon {getRings :: [Ring]} deriving (Show, Eq)
data Polygon = Polygon {exteriorRing :: Ring, interiorRings :: List Ring} deriving (Show, Eq)

getPolygonPoints :: Polygon -> Set Point
getPolygonPoints = undefined

numPolygonPoints :: Polygon -> Int
numPolygonPoints = undefined

combinePolygonRings :: Polygon -> List Ring
combinePolygonRings = liftA2 (<|) exteriorRing interiorRings

triangulate :: Polygon -> Maybe (List Triangle)
triangulate =
  List .
  unfoldr ( head .
            catMaybes .
            fmap (bisequenceA . (goodTriangle &&& looseTPoint) :: Ring -> Maybe (Triangle, Ring)) .
            rotationsL ) <$<
  rds2r <$<
  linkRings .
  fmap r2rdl .
  liftA2 (<|) (reverse . exteriorRing) interiorRings

  where goodTriangle :: Ring -> Maybe Triangle
        goodTriangle ps@(get4Points -> Just (p0, p1, p2, p3))
          | p0 == pt 0 3 && p1 == pt 1 1 && trace (show ([p0, p2], looseTPoint ps)) True &&
            (trace ("curvingInwards: " ++ show curvingInwards) curvingInwards) &&
            (trace ("withinNext: " ++ show withinNext) withinNext) &&
            (trace ("noIntersect: " ++ show noIntersect) noIntersect) &&
            (trace ("noPointInside: " ++ show noPointInside) noPointInside) = t
          | curvingInwards &&
            withinNext &&
            noIntersect &&
            noPointInside = t
          | otherwise = Nothing

          where curvingInwards =  v01 `xp` v12 > zero

                withinNext = between' v21 v23 CW v20 :: Bool

                noIntersect =
                  ( maybeBool $
                    not . ininIntersect' <$>
                    ( bisequenceA $
                      bimap toPolyline (>>= toPolyline . getRing) -- could pose a problem since ps is probably a ring
                      (List [p0, p2], looseTPoint ps) ) ) ||
                  (ps `lengthEq` 3)

                noPointInside = maybeBool $ (not . flip any ps . interiorContains <$> t)

                t = nTriangle p0 p1 p2

                v01 = p0 .->. p1
                v12 = p1 .->. p2

                v20 = p2 .->. p0
                v21 = p2 .->. p1
                v23 = p2 .->. p3
        goodTriangle _ = Nothing

        looseTPoint :: Ring -> Maybe Ring
        looseTPoint =
          liftA2 (liftA2 (|>))
            (drop 2)
            ((\(p,_,_,_) -> p) <$< get4Points)

        get4Points :: Ring -> Maybe (Point, Point, Point, Point)
        get4Points =
          liftA2 (<|>)
            take4
            ((\(p0, p1, p2) -> (p2, p0, p1, p2)) <$< take3)
