{-# LANGUAGE GeneralizedNewtypeDeriving
           , MultiParamTypeClasses
           , InstanceSigs
           , DeriveFoldable
           , NoImplicitPrelude
           , DeriveTraversable
           , StandaloneDeriving
           , UndecidableInstances
           , ViewPatterns
           , TypeSynonymInstances
           , FlexibleInstances
           , LambdaCase
           , PartialTypeSignatures
           , ScopedTypeVariables
           , DataKinds
           , TypeApplications
           , KindSignatures
           , TypeOperators
           , TypeFamilies
           #-}

module Polyline where

import IsClose (IsClose(isCloseAR, isCloseA, isCloseRO))
import GeometryClasses
  ( SelfIntersecting(selfIntersecting)
  , Contain(contains, interiorContains)
  )
import Dimension
  ( Squared
  , Distance
  , SqRt(sqrt)
  )
import Angle (Ra)
import Vector
  ( ToVec
  , collinearV'
  )
import qualified PLS
import PLS hiding (Reverse, reverse)

import Utilities
import Utilities.Arbitrary (AddNoise(arNoise, arNoiseUnbounded))

import Unified
import Unified.Utilities
  ( (<$|)
  , tailInit
  )
import Unified.AtLeast
  ( AtLeast(AtLeast, getAtLeast)
  , tailInitAL
  , toAtLeast
  )
import Unified.Collections.List (List(List, getList))
import Unified.Utilities.List
  ( allCombo''
  , secondCombo
  , tails
  , toPairsL
  , filterL
  , zipL
  , mapL
  , tails
  )
import Unified.Collections.Sequence (Seq)
import Unified.Arbitrary (localUnique)

import Control.Applicative (liftA2, (<$>))
import Control.Arrow ((&&&))
import Control.Monad
import Data.Bifunctor (bimap, first, second)
import Data.Bitraversable (bisequenceA)
import Data.Bool (Bool(True, False), not, (&&), (||), otherwise)
import Data.Eq
import Data.Foldable hiding (length)
import Data.Function
import Data.Functor (Functor, fmap)
import Data.Maybe (Maybe(Just, Nothing), fromMaybe)
import Data.Monoid (mconcat)
import Data.Ord
import Data.Semigroup (Semigroup((<>)))
import Data.Traversable (Traversable, traverse, sequenceA)
import Data.Tuple (swap, curry, uncurry, snd)
import Test.QuickCheck
import Text.Show (Show)


type Polyline = Polyline_ Point
newtype Polyline_ a = Polyline {getPolyline :: AtLeast 2 Seq a}
  deriving ( Show, Eq, Semigroup, Foldable, Functor, Traversable
           , Length, Cons, Snoc, Index, Reverse, DeleteAt, HeadNE, LastNE, Take, Drop, FilterNE, Sort, SplitAtMay )

-- true if the endpoints are equal
isClosed :: Polyline -> Bool
isClosed pl = head pl == last pl

-- closes the polyline if not closed (closed: endpoints are equal)
closePl :: Polyline -> Polyline
closePl pl | head pl /= last pl = hp pl <| pl
           | otherwise = pl

-- not the furthest distance from x and y but the
-- furthest point of x from y or point of y from x
maxDistance :: Polyline -> Polyline -> Distance
maxDistance x y = sqrt $ maximum $ mconcat $ fmap dists [c, swap c]
  where dists :: (Polyline, Polyline) -> List (Squared Distance)
        dists (x', y') = mapL ((|--|) x') y'
        c = (x, y)
maxDistance' :: (Polyline, Polyline) -> Distance
maxDistance' = uncurry maxDistance

combine :: Polyline -> Polyline -> Maybe Polyline
combine = curry combine'
combine' :: (Polyline, Polyline) -> Maybe Polyline
combine' =
  Polyline . AtLeast . uncurry (<>) <$<
  iff (collinearV' . bimap lastEdge firstEdge)
    (bisequenceA . bimap init tail . bimap' (getAtLeast . getPolyline))
    (sequenceA . second tail . bimap' (getAtLeast . getPolyline)) .
  (\(pls, p) ->
    pls & case bimap' (== p) $ bimap hp tp pls of
      (True, True) -> id
      (False, False) -> swap
      (True, False) -> second reverse
      (False, True) -> first reverse ) <=<
    -- pls & case bimap' ((== p) . tp) pls of
    --   (False, True)  -> id
    --   (True, False)  -> swap
    --   (False, False) -> second reverse
    --   (True, True)   -> first reverse ) <=<
  ( sequenceA .
    mkSnd sharedEndpoint'
    :: (Polyline, Polyline) -> Maybe ((Polyline, Polyline), Point) )

-- index :: forall (i :: Nat) n a. (0 <= i, i+1 <= 2) => Polyline_ a -> a
-- index = indexNE @i . getPolyline
-- 
-- index' :: forall (i :: Nat) a. Polyline_ a -> a
-- index' = indexNER @i . getPolyline

newtype End a = End {getEnd :: a} deriving (Show, Eq)
deriving instance ToVec (End Segm)
deriving instance TailHeadPoints (End Segm)
deriving instance From2Points (End Segm)
deriving instance Intersect (End Segm)
deriving instance PLS.Reverse (End Segm)
instance Contain (End Segm) Point where
  contains s = contains (getEnd s)
  interiorContains = interiorContains . getEnd

newtype Interior a = Interior {getInterior :: a} deriving (Show, Eq)
deriving instance ToVec (Interior Segm)
deriving instance TailHeadPoints (Interior Segm)
deriving instance Intersect (Interior Segm)
deriving instance From2Points (Interior Segm)
instance Contain (Interior Segm) Point where
  contains s = contains (getInterior s)
  interiorContains = interiorContains . getInterior

interiorEdges :: Polyline -> List (Interior Segm)
interiorEdges = fmap Interior . fromMaybe empty . tailInit . getAtLeast . edges

firstEdge :: Polyline -> End Segm
firstEdge = End . nS''' . (tp &&& secondHead)

lastEdge :: Polyline -> End Segm
lastEdge = End . nS''' . (secondLast &&& hp)

secondHead :: Polyline_ a -> a
secondHead = indexNE @1 . getPolyline

secondLast :: Polyline_ a -> a
secondLast = indexNER @1 . getPolyline

endEdges :: Polyline -> List (End Segm)
endEdges pl = List [firstEdge pl, lastEdge pl]

eiIntersects :: End Segm -> Interior Segm -> Bool
eiIntersects = curry eiIntersects'
eiIntersects' :: (End Segm, Interior Segm) -> Bool
eiIntersects' = intersects' . bimap getEnd getInterior

eiIntersectPoint :: (End Segm, Interior Segm) -> Maybe Point
eiIntersectPoint = intersectPoint' . bimap getEnd getInterior

interiorPoints :: Polyline -> List (Interior Point)
interiorPoints =
  fmap Interior .
  fromFoldable .
  tailInitAL .
  getPolyline

rotate :: Functor t => Point -> Ra -> t Point -> t Point
rotate p a = fmap $ PLS.rotate p a


class ApproxAsPolyline a where
  approxAsPolyline :: Distance -- tolerance
                   -> a
                   -> Polyline

class ToPolyline a where
  toPolyline :: a -> Maybe Polyline

instance Foldable t => ToPolyline (t Point) where
  toPolyline = fromEFoldableMay

instance FromEFoldableMay Polyline_ Point where
  fromEFoldableMay =
    Polyline <$<
    toAtLeast <=<
    liftA2 (liftA2 (<|))
      (head . List . toList)
      ( Just .
        fromFoldable .
        fmap snd .
        filterL (uncurry (/=)) .
        toPairsL )

-- instance {-# INCOHERENT #-}
--           ( Cons t
--           , Foldable t
--           ) => ToPolyline (t Point) where
--   -- toPolyline ps
--   --   | maybeBool $ (`lengthGT` 1) <$> fps = fromFoldable =<< fps
--   --   | otherwise = Nothing
--   --   where fps :: Maybe (List Point)
--   --         fps = liftA2 (<|)
--   --                 (head ps)
--   --                 (Just $ fmap lst $ filterL (uncurry (/=)) $ toPairsL ps)
--   toPolyline =
--     liftA2 (liftA2 (<|))
--       head
--       ( Polyline <$<
--         fromFoldableMay .
--         fmap lst .
--         filter (uncurry (/=)) .
--         toPairsL )

instance Foldable t => ToPolyline (t Segm) where
  toPolyline ((fromFoldable :: Foldable f => f a -> List a) -> es)
    | all (uncurry (==) . bimap hp tp) $ toPairsL es =
      Polyline <$> (fromFoldableMay =<< (tp <$> head es) <$| fmap hp es)
    | otherwise = Nothing

instance Edges Polyline_ where
  edges = AtLeast . fmap nS''' . toPairsL

instance Contain Polyline Point where
  contains :: Polyline -> Point -> Bool
  contains pl p = any (`contains` p) (edges pl)

  interiorContains :: Polyline -> Point -> Bool
  interiorContains pl p =
    maybeBool (any (`contains` p) <$> (edges <$> tailInit pl)) ||
    any (`interiorContains` p) [h, t] ||
    (length pl == 3 && secondHead pl == p)
    where h = headNE $ edges pl
          t = nS'' (hp pl) (secondLast pl)

instance Intersect Polyline where
  intersects :: Polyline -> Polyline -> Bool
  intersects = (intersects :: Edges t => t Point -> t Point -> Bool)

  interiorIntersect' :: (Polyline, Polyline) -> Bool
  interiorIntersect' pls =
    any interiorIntersect' (allCombo'' ex0 ex1) ||
    ( any (any intersects' . uncurry allCombo'' :: (List Segm, List Segm) -> Bool)
          ([(ie0, ie1), (ex0, ie1), (ex1, ie0)] :: [(List Segm, List Segm)]) )
    where (ie0, ie1) = bimap' (fmap getInterior . interiorEdges) pls
          (ex0, ex1) = bimap' (fmap getEnd . endEdges) pls

  ininIntersect' :: (Polyline, Polyline) -> Bool
  ininIntersect' pls =
    (maybeBool (intersects' <$> bisequenceA (bimap' tailInit pls))) ||
    (any ininIntersect' $ uncurry allCombo'' $ bimap' endEdges pls) ||
    ( any ( (\case Nothing -> True
                   Just b -> b ) .
            ( not .
              hasEndpoint' .
              first fst <$<
              sequenceA .
              mkSnd eiIntersectPoint ) ) $
      filterL eiIntersects' $
      uncurry (<>) $
      bimap' (uncurry allCombo'' . bimap endEdges interiorEdges) $
      mkSnd swap
      pls ) ||
    ( any (\pls'@(pl0, pl1) ->
        if pl0 `lengthEq` 3
          then if pl1 `lengthEq` 3
            then uncurry (==) $ bimap' ((!!) 1) pls'
            else any (`contains` (secondHead pl0)) $ interiorEdges pl1
          else False )
      [pls, swap pls] )

instance SelfIntersecting Polyline where
  selfIntersecting :: Polyline -> Bool
  selfIntersecting pl =
    any (uncurry interiorIntersect) (toPairsL es) ||
    any intersects' (secondCombo es)
    where es = edges pl

instance IsClose Polyline where
  isCloseAR a r pl0 pl1 =
    length pl0 == length pl1 &&
    all (uncurry $ isCloseAR a r) (zipL pl0 pl1)

  isCloseA a pl0 pl1 =
    length pl0 == length pl1 &&
    all (uncurry $ isCloseA a) (zipL pl0 pl1)

  isCloseRO r pl0 pl1 =
    length pl0 == length pl1 &&
    all (uncurry $ isCloseRO r) (zipL pl0 pl1)

instance AddNoise Polyline where
  arNoise b0 b1 pl = traverse (arNoise b0 b1) pl
  arNoiseUnbounded b0 b1 pl = traverse (arNoiseUnbounded b0 b1) pl

instance TailHeadPoints Polyline where
  tp = headNE
  hp = lastNE

instance Arbitrary Polyline where
  arbitrary = do
    i <- choose (2, 100)
    ps <- localUnique i empty
    return $ Polyline $ AtLeast ps

  shrink :: Polyline -> [Polyline]
  shrink pl | pl == shortest = []
            | pl == sndShortest = [shortest]
            | length pl == 2 = [sndShortest, shortest]
            | otherwise = getList $
                          fromMaybe empty $ tail $
                          takeWhile ((> 1) . length) $
                          (tails :: Polyline -> List Polyline) pl :: [Polyline]
    where sndShortest = Polyline $ AtLeast $ fromFoldable [pt 0 0, pt 1 1]
          shortest =    Polyline $ AtLeast $ fromFoldable [pt 0 0, pt 1 0]

