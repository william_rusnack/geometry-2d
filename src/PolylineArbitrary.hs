{-# LANGUAGE GeneralizedNewtypeDeriving
           , MultiParamTypeClasses
           , MultiWayIf
           , TupleSections
           , InstanceSigs
           , DeriveFoldable
           , NoImplicitPrelude
           , DeriveTraversable
           , StandaloneDeriving
           , UndecidableInstances
           , ViewPatterns
           , TypeSynonymInstances
           , FlexibleInstances
           , LambdaCase
           , PartialTypeSignatures
           , ScopedTypeVariables
           , DataKinds
           #-}

module PolylineArbitrary where

import Polyline

import GeometryClasses (contains)
import Dimension (Distance, Squared)
import qualified PLS
import PLS hiding (reverse, Reverse)
import SegmArbitrary

import Utilities
import Utilities.Arbitrary (minChoose, minChooseD, maxGenValue, thisOrThat)

import Unified
import Unified.Arbitrary (uniqueValues, localUnique)
import Unified.AtLeast (AtLeast(AtLeast, getAtLeast), tailInitAL)
import Unified.Collections.List (List(List, getList))
import Unified.Collections.Sequence (Seq)
import Unified.QuickCheck (oneof, elements, shuffle, vector)
import Unified.Utilities (take2, tailInit, unfoldr, count)
import Unified.Utilities.List (allCombo'', tupleCombo, filterL, zipL)
import Unified.Utilities.List (tails, inits)

import Control.Applicative (pure, liftA2)
import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad.ST (runST)
import Data.Bifunctor (bimap, first, second)
import Data.Bitraversable (bisequenceA)
import Data.Bool (Bool(True, False), not, (&&), (||), otherwise)
import Data.Composition ((.:))
import Data.Either (Either(Left, Right))
import Data.Eq
import Data.Foldable hiding (length)
import Data.Function
import Data.Functor
import Data.Int (Int)
import Data.Maybe
import Data.Monoid (mconcat)
import Data.Ord
import Data.STRef (newSTRef, readSTRef, writeSTRef)
import Data.Semigroup (Semigroup((<>)))
import Data.Traversable (Traversable, sequenceA)
import Data.Tuple (swap, uncurry, snd)
import Data.Tuple.HT (double)
import GHC.Num
import Test.QuickCheck hiding (oneof, elements, shuffle, vector)
import Text.Show (Show, show)

import qualified Control.Monad.Loops as ML
import qualified Data.List as L
import qualified Data.Vector.Mutable as MV

import Prelude (error)


type PLPL = (Polyline, Polyline)
type PLPT = (Polyline, Point)
type PPP = (PLPL, Point)

newtype PolylineAndContainedPoint = PolylineAndContainedPoint
  {getPolylineAndContainedPoint :: PLPT}
  deriving Show
instance Arbitrary PolylineAndContainedPoint where
  arbitrary = PolylineAndContainedPoint <$> oneof
    [ getPointOnPolylineSegm <$> arbitrary
    , getPolylineAndEndpoint <$> arbitrary
    , getPolylineAndConnectorPoint <$> arbitrary
    , getPolylineAndInteriorEndSegmPoint <$> arbitrary
    , getPolylineISsCP <$> arbitrary ]

-- point lies on the polyine
newtype PointOnPolylineSegm = PointOnPolylineSegm
  {getPointOnPolylineSegm :: PLPT}
  deriving Show
instance Arbitrary PointOnPolylineSegm where
  arbitrary = do
    pl <- arbitrary
    s <- elements $ edges pl
    p <- pointOnSegment s
    return $ PointOnPolylineSegm (pl, p)

  shrink = shrinkBothEndsToContainedPoint2 PointOnPolylineSegm getPointOnPolylineSegm

newtype PolylineAndEndpoint = PolylineAndEndpoint
  {getPolylineAndEndpoint :: PLPT}
  deriving (Show, Eq)
instance Arbitrary PolylineAndEndpoint where
  arbitrary = do
    pl <- arbitrary
    p <- elements $ thps' pl
    return $ PolylineAndEndpoint (pl, p)

  shrink =
    getList .
    fmap PolylineAndEndpoint .
    (\(pl, p) ->
      fmap (, p) $
      sortOn length $
      fromMaybe empty $
      biLiftC (<>) tail
        (if tp pl == p then inits pl else empty)
        (if hp pl == p then tails pl else empty) ) .
    getPolylineAndEndpoint

newtype PolylineAndUniqueEndpoint = PolylineAndUniqueEndpoint
  {getPolylineAndUniqueEndpoint :: PLPT}
  deriving Show
instance Arbitrary PolylineAndUniqueEndpoint where
  arbitrary =
    PolylineAndUniqueEndpoint <$>
    suchThat
      (getPolylineAndEndpoint <$> arbitrary)
      (\(pl, p) -> not $ elem p $ fmap getInterior $ interiorPoints pl)

newtype PolylinesTwoSharedEndpoints = PolylinesTwoSharedEndpoints
  {getPolylinesTwoSharedEndpoints :: PLPL}
  deriving Show
instance Arbitrary PolylinesTwoSharedEndpoints where
  arbitrary = do
    pl0 <- arbitrary
    pl1 <-
      Polyline . AtLeast .
      (<|) (hp pl0) .
      (|> tp pl0) <$>
      (vector =<< arbitrary)
    return $ PolylinesTwoSharedEndpoints (pl0, pl1)

newtype PolylinesWithNoSharedEndpoints = PolylinesWithNoSharedEndpoints
  {getPolylinesWithNoSharedEndpoints :: PLPL}
  deriving Show
instance Arbitrary PolylinesWithNoSharedEndpoints where
  arbitrary =
    PolylinesWithNoSharedEndpoints <$>
    suchThat arbitrary (disjoint' . bimap' thps')

-- point on the polyline that is an end point of two of its segments
newtype PolylineAndConnectorPoint = PolylineAndConnectorPoint
  {getPolylineAndConnectorPoint :: PLPT}
  deriving Show
instance Arbitrary PolylineAndConnectorPoint where
  arbitrary = do
    l <- choose (3, 20) :: Gen Int
    pl <- Polyline . AtLeast <$> localUnique l empty :: Gen Polyline
    p <- elements $ (tailInitAL $ getPolyline pl) :: Gen Point
    return $ PolylineAndConnectorPoint (pl, p)

newtype PolylineAndInteriorPoint = PolylineAndInteriorPoint
  {getPolylineAndInteriorPoint :: PLPT}
  deriving Show
instance Arbitrary PolylineAndInteriorPoint where
  arbitrary = PolylineAndInteriorPoint <$> oneof
    [ getPolylineAndInteriorEndSegmPoint <$> arbitrary
    , getPolylineISsCP <$> arbitrary ]

-- point lies on the interior of one of the end segments of the polyline
newtype PolylineAndInteriorEndSegmPoint = PolylineAndInteriorEndSegmPoint
  {getPolylineAndInteriorEndSegmPoint :: PLPT}
  deriving Show
instance Arbitrary PolylineAndInteriorEndSegmPoint where
  arbitrary = do
    pl <- arbitrary
    let es = edges pl
    s <- elements [headNE es, lastNE es]
    p <- interiorPointOnSegment s
    return $ PolylineAndInteriorEndSegmPoint (pl, p)

-- polyline interior segmends (not end segment) contain a point
newtype PolylineISsCP = PolylineISsCP
  {getPolylineISsCP :: PLPT}
  deriving Show
instance Arbitrary PolylineISsCP where
  arbitrary = do
    l <- choose (4, 10)
    -- pl <- Polyline . AtLeast <$> localUnique l empty
    -- s <- elements $ edges $ fromJust $ tailInit pl
    -- p <- pointOnSegment s
    -- return $ PolylineISsCP (pl, p)

    PolylineISsCP <$> suchThat
      ( do
          pl <- Polyline . AtLeast <$> localUnique l empty
          s <- elements $ edges $ fromJust $ tailInit pl
          p <- pointOnSegment s
          return (pl, p) )
      (\(pl, p) -> not $ any (`contains` p) $ endEdges pl)

newtype DisjointPolylineAndPoint = DisjointPolylineAndPoint
  {getDisjointPolylineAndPoint :: PLPT}
  deriving Show
instance Arbitrary DisjointPolylineAndPoint where
  arbitrary = do
    pl <- arbitrary
    let es = edges pl
    p <- suchThat arbitrary (\p -> not $ any (`contains` p) es)
    return $ DisjointPolylineAndPoint (pl, p)

-- can have interior intersect
newtype SharedPolylineEndpoint = SharedPolylineEndpoint
  {getSharedPolylineEndpoint :: PPP}
  deriving Show
instance Arbitrary SharedPolylineEndpoint where
  arbitrary = do
    pl0 <- arbitrary

    l <- choose (1, maxGenValue)
    p <- elements $ thps' pl0
    pl1' <- Polyline . fromJust . fromFoldableMay <$> localUnique l (singleton p :: List Point)
    pl1 <- elements [pl1', reverse pl1']

    return $ SharedPolylineEndpoint ((pl0, pl1), p)

  shrink = shrinkBothEndsToContainedPoint SharedPolylineEndpoint getSharedPolylineEndpoint

newtype PolylineIntersect = PolylineIntersect
  {getPolylineIntersect :: (PLPL, Either Point Segm)}
  deriving Show
instance Arbitrary PolylineIntersect where
  arbitrary = PolylineIntersect <$> oneof
    ( (second Left  . getBoundryInteriorPlIntersect <$> arbitrary) :
      interiorIntersectsGets )

newtype BoundryInteriorPlIntersect = BoundryInteriorPlIntersect
  {getBoundryInteriorPlIntersect :: PPP}
  deriving Show
instance Arbitrary BoundryInteriorPlIntersect where
  arbitrary = do
    (s0'', s1', p) <- getEndInteriorIntersectingSegms <$> arbitrary
    let (s0', s1) = (if s0'' `hasEndpoint` p then id else swap) (s0'', s1')
    let s0 = (if p == tp s0' then id else PLS.reverse) s0'

    nr0 <- choose (0, maxGenValue)
    pl0' <- extend2Ways 0 nr0 $ thps' s0

    let mc = minChooseD 2 0
    nl1 <- mc
    nr1 <- mc
    pl1' <- extend2Ways nl1 nr1 $ thps' s1

    (pl0, pl1) <- elements $ tupleCombo reverse (pl0', pl1')

    return $ BoundryInteriorPlIntersect ((pl0, pl1), p)

  shrink = shrinkBothEndsToContainedPoint BoundryInteriorPlIntersect getBoundryInteriorPlIntersect

newtype InteriorPolylineIntersect = InteriorPolylineIntersect
  {getInteriorPolylineIntersect :: (PLPL, Either Point Segm)}
  deriving Show
instance Arbitrary InteriorPolylineIntersect where
  arbitrary = InteriorPolylineIntersect <$> oneof interiorIntersectsGets

interiorIntersectsGets :: [Gen (PLPL, Either Point Segm)]
interiorIntersectsGets =
  [ second Left  . getInteriorCrossPlIntersect <$> arbitrary
  , second Left  . getInteriorTransPtPlIntersect <$> arbitrary
  , second Left  . getInteriorTransPtSegmOfPlIntersect <$> arbitrary
  , second Right . getInteriorCollinearPlIntersect <$> arbitrary ]

-- polylines have a point intersect that is not a polyline endpoint
newtype InteriorCrossPlIntersect = InteriorCrossPlIntersect
  {getInteriorCrossPlIntersect :: PPP}
  deriving Show
instance Arbitrary InteriorCrossPlIntersect where
  arbitrary = do
    SegmsIntersectingAtPoint (s0, s1, p) <- arbitrary

    let es :: Segm -> Gen Polyline
        es s = do
          let mc = minChooseD 2
          nl <- mc $ if p == tp s then 1 else 0
          nr <- mc $ if p == hp s then 1 else 0
          suchThat (extend2Ways nl nr $ thps' s) $
            not . (`hasEndpoint` p)

    pl0 <- es s0
    pl1 <- es s1

    return $ InteriorCrossPlIntersect ((pl0, pl1), p)

  shrink = shrinkBothEndsToContainedPoint InteriorCrossPlIntersect getInteriorCrossPlIntersect

extend2Ways :: forall t.
            ( Foldable t
            , Reverse t
            , Drop t
            , Length t
            , Semigroup (t Point)
            , Cons t
            , Empty t
            ) => Int -> Int -> t Point -> Gen Polyline
extend2Ways nl nr ps = do
  let l = length ps
  left <- localUnique nl ps :: Gen (t Point)
  right <- (fromJust . drop l . reverse) <$> (localUnique nr $ reverse ps) :: Gen (t Point)
  return $ Polyline $ fromJust $ (fromFoldableMay $ (left <> right :: t Point) :: Maybe (AtLeast 2 Seq Point))

-- a two segment's endpoints intersect that is not a polyline endpoint
newtype InteriorTransPtPlIntersect = InteriorTransPtPlIntersect
  {getInteriorTransPtPlIntersect :: PPP}
  deriving Show
instance Arbitrary InteriorTransPtPlIntersect where
  arbitrary = do
    OneSharedEndpointSegms (s0, s1, p) <- arbitrary

    let es :: Segm -> Gen Polyline
        es s' = do
          let s = (if p == tp s' then id else PLS.reverse) s'
          let mc = minChooseD 2
          nl <- mc 1
          nr <- mc 0
          suchThat (extend2Ways nl nr $ thps' s) $
            not . (`hasEndpoint` p)

    pl0 <- es s0
    pl1 <- es s1

    return $ InteriorTransPtPlIntersect ((pl0, pl1), p)

  shrink = shrinkBothEndsToContainedPoint InteriorTransPtPlIntersect getInteriorTransPtPlIntersect

-- segment contains segment endpoint that is not a polyline endpoint
newtype InteriorTransPtSegmOfPlIntersect = InteriorTransPtSegmOfPlIntersect
  {getInteriorTransPtSegmOfPlIntersect :: PPP}
  deriving Show
instance Arbitrary InteriorTransPtSegmOfPlIntersect where
  arbitrary = do
    EndInteriorIntersectingSegms (s0'', s1', p) <- arbitrary
    let (s0', s1) = (if hasEndpoint s0'' p then id else swap) (s0'', s1')
    let s0 = (if p == tp s0' then id else PLS.reverse) s0'

    let mc = minChooseD 2
    let n = mc 0
    nl0 <- mc 1
    nr0 <- n
    nl1 <- n
    nr1 <- n

    pl0' <- extend2Ways nl0 nr0 $ thps' s0
    pl1' <- extend2Ways nl1 nr1 $ thps' s1

    (pl0, pl1) <- fromJust . take2 <$> shuffle (List [pl0', pl1'])

    return $ InteriorTransPtSegmOfPlIntersect ((pl0, pl1), p)

  shrink = shrinkBothEndsToContainedPoint InteriorTransPtSegmOfPlIntersect getInteriorTransPtSegmOfPlIntersect

newtype InteriorCollinearPlIntersect = InteriorCollinearPlIntersect
  {getInteriorCollinearPlIntersect :: (PLPL, Segm)}
  deriving Show
instance Arbitrary InteriorCollinearPlIntersect where
  arbitrary = do
    CollinearIntersectingSegms (s0, s1, si) <- arbitrary

    let n = minChooseD 2 0
    let es s = do
          nl <- n
          nr <- n
          extend2Ways nl nr $ thps' s

    pl0 <- es s0
    pl1 <- es s1

    return $ InteriorCollinearPlIntersect ((pl0, pl1), si)

newtype DisjointPolylines = DisjointPolylines
  {getDisjointPolylines :: PLPL}
  deriving Show
instance Arbitrary DisjointPolylines where
  arbitrary = do
    let n = choose (2, maxGenValue)
    n0 <- n
    n1 <- n

    (pl0, pl1) <-
      fromJust .
      ( (\v -> if isNothing v then error "Nothing fromFoldableMay" else v) .
        bisequenceA .
        bimap' (Polyline <$< fromFoldableMay) <=<
        (\v -> if isNothing v then error ("Nothing splitAtMay" <> " n0: " <> show n0 <> ", n1: " <> show n1) else v) .
        splitAtMay n0 ) <$>
      uniqueValues (n0 + n1) (empty :: List Point)

    return $ DisjointPolylines $ untanglePls 0 pl0 0 pl1 False

-- i0 and i1 should be 0
-- c should be false
untanglePls :: Int
            -> Polyline
            -> Int
            -> Polyline
            -> Bool
            -> PLPL
untanglePls i0 pl0 i1 pl1 c =
  case (s pl0 i0, s pl1 i1, c) of
    (Nothing, _, True) -> untanglePls 0 pl0 0 pl1 False
    (Nothing, _, False) -> (pl0, pl1)
    (_, Nothing, _) -> untanglePls (i0 + 1) pl0 0 pl1 c
    (Just s0, Just s1, _) ->
      if s0 `intersects` s1
      then let (t0, h0) = splitAt (i0 + 1) $ gps pl0
               (t1, h1) = splitAt (i1 + 1) $ gps pl1
        in untanglePls i0 (pps $ t0 <> h1) (i1+1) (pps $ t1 <> h0) True
      else untanglePls i0 pl0 (i1+1) pl1 c
  where s :: Polyline -> Int -> Maybe Segm
        s pl i = f2p =<< bisequenceA (i !! pl, (i + 1) !! pl)

        gps = getAtLeast . getPolyline
        pps = Polyline . AtLeast

newtype NonSelfIntersectingPolyline = NonSelfIntersectingPolyline
  {getNonSelfIntersectingPolyline :: Polyline}
  deriving Show
instance Arbitrary NonSelfIntersectingPolyline where
  arbitrary = do
    l <- minChoose 2
    nonSelfIntersectingPolyline l

-- l - number of points in polyline
nonSelfIntersectingPolyline :: Int -> Gen NonSelfIntersectingPolyline
nonSelfIntersectingPolyline l =
  NonSelfIntersectingPolyline .
  Polyline .
  fromJust .
  fromFoldableMay .
  untanglePoints <$>
  uniqueValues l (empty :: List Point)

untanglePoints :: (FromFoldable t, Traversable t, Foldable t, Length t) => t Point -> t Point
untanglePoints ps = runST $ do
  let l = length ps

  v <- MV.new l
  traverse_ (uncurry $ MV.write v) (zipL [0..] ps)

  let r = MV.read v
  let s i = nS''' <$> bisequenceA (r i, r $ i + 1)
  let sw = MV.swap v
  let revSub i i' | i < i' = do
                     sw i i'
                     revSub (i+1) (i'-1)
                  | otherwise = return ()

  for_ [0..l-3] $ \i -> do
      s0 <- s i
      s1 <- s $ i + 1

      if s0 `contains` hp s1
      then sw (i+1) (i+2)
      else if s1 `contains` tp s0
      then sw i (i+1)
      else return ()

  let ut inter = do
        isChange <- newSTRef True
        ML.whileM_ (readSTRef isChange) $ do
          writeSTRef isChange False
          for_ [2..l-2] $ \space -> do
            for_ (zipL [0..] [space..l-2]) $ \(ti, li) -> do
              ts <- s ti
              ls <- s li

              if ts `inter` ls && tp ts /= hp ls
              then do
                revSub (ti+1) li
                writeSTRef isChange True
              else return ()
        readSTRef isChange

  void $ ut crossIntersect
  _ <- ML.andM $ L.cycle [ ut $ \s0 s1 -> collinearL s0 s1 || intersects s0 s1
                         , ut crossIntersect ]

  sequenceA $ fromFoldable $ fmap (MV.read v) $ List [0..l-1]

-- no interal intersect and shares one or more endpoints
newtype BoundryOnlyPolylineIntersect = BoundryOnlyPolylineIntersect
  {getBoundryOnlyPolylineIntersect :: PPP}
  deriving Show
instance Arbitrary BoundryOnlyPolylineIntersect where
  arbitrary = oneof
    [ do -- parallel connected end segment
        n <- minChoose 2
        ps <- getAtLeast . getPolyline .
              getNonSelfIntersectingPolyline <$>
              nonSelfIntersectingPolyline n

        i <- choose (0, n - 2)
        p <- interiorPointOnSegment $ nS''' $ fromJust $ bisequenceA (i !! ps, (i + 1) !! ps)
        let (ps0, ps1) = splitAt (i + 1) ps

        pls <- elements $ tupleCombo reverse
          ( Polyline $ AtLeast $ ps0 |> p
          , Polyline $ AtLeast $ p <| ps1 )

        pure $ BoundryOnlyPolylineIntersect (pls, p)

    , do -- split at interior point
        n <- minChoose 3
        i <- choose (2, n - 1)
        (ps0, ps1) <-
          splitAt i .
          getAtLeast . getPolyline .
          getNonSelfIntersectingPolyline <$>
          nonSelfIntersectingPolyline n

        let pl0 = Polyline $ AtLeast $ ps0
        let p = hp pl0
        let pl1 = Polyline $ AtLeast $ p <| ps1

        pls <- elements $ tupleCombo reverse (pl0, pl1)

        pure $ BoundryOnlyPolylineIntersect (pls, p) ]

  shrink = shrinkBothEndsToContainedPoint BoundryOnlyPolylineIntersect getBoundryOnlyPolylineIntersect

-- first point and last point are different
newtype OpenPolyline = OpenPolyline
  {getOpenPolyline :: Polyline}
  deriving Show
instance Arbitrary OpenPolyline where
  arbitrary = OpenPolyline <$> suchThat arbitrary (uncurry (/=) . (head &&& last))

-- endpoints are equal
newtype ClosedPolyline = ClosedPolyline
  {getClosedPolyline :: Polyline}
  deriving Show
instance Arbitrary ClosedPolyline where
  arbitrary = do
    pl <- arbitrary
    return $ ClosedPolyline $ hp pl <| pl

newtype SelfIntersectingPolyline = SelfIntersectingPolyline
  {getSelfIntersectingPolyline :: (Polyline, Either Point Segm)}
  deriving Show
instance Arbitrary SelfIntersectingPolyline where
  arbitrary = SelfIntersectingPolyline <$> oneof
    [ (\pl -> (pl, Left $ tp pl)) . getClosedPolyline <$> arbitrary
    , (fmap Left) . getCrossPlSelfIntersect <$> arbitrary
    , (fmap Left) . getTransPtPlSelfIntersect <$> arbitrary
    , (fmap Left) . getTransPtSegmOfPlSelfIntersect <$> arbitrary
    , (fmap Right) . getCollinearPlSelfIntersect <$> arbitrary
    , (fmap Left) . getTransPtEndPtPLSelfIntersect <$> arbitrary
    , (fmap Left) . getEndPtSegmPlSelfIntersect <$> arbitrary ]

newtype CrossPlSelfIntersect = CrossPlSelfIntersect
  {getCrossPlSelfIntersect :: PLPT}
  deriving Show
instance Arbitrary CrossPlSelfIntersect where
  arbitrary = do
    let n = minChooseD 3 0
    l <- n
    m <- n
    r <- n

    SegmsIntersectingAtPoint (s0, s1, p) <- arbitrary
    pl <- extendLMR l m r s0 s1
    return $ CrossPlSelfIntersect (pl, p)

extendLMR :: Int -> Int -> Int -> Segm -> Segm -> Gen Polyline
extendLMR l m r s0 s1 = do
  lps <- extend2Ways l 0 (thps' s0)
  rps <- extend2Ways m r (thps' s1)
  return $ lps <> rps

newtype TransPtPlSelfIntersect = TransPtPlSelfIntersect
  {getTransPtPlSelfIntersect :: PLPT}
  deriving Show
instance Arbitrary TransPtPlSelfIntersect where
  arbitrary = do
    let mc = minChooseD 3
    l <- mc 0
    m <- mc 1
    r <- mc 0

    OneSharedEndpointSegms (s0', s1', p) <- arbitrary

    let (s0, s1) = fromJust $ head $
                   filterL (\(s, s') -> hp s == tp s') $
                   tupleCombo PLS.reverse (s0', s1')
    pl <- extendLMR l m r s0 s1
    return $ TransPtPlSelfIntersect (pl, p)

newtype TransPtSegmOfPlSelfIntersect = TransPtSegmOfPlSelfIntersect
  {getTransPtSegmOfPlSelfIntersect :: PLPT}
  deriving Show
instance Arbitrary TransPtSegmOfPlSelfIntersect where
  arbitrary = do
    let mc = minChooseD 3
    l <- mc 0
    m <- mc 1
    r <- mc 0

    EndInteriorIntersectingSegms (s0', s1', p) <- arbitrary

    let (s0, s1) = fromJust $ head $
                   filterL (\(s, s') -> s `contains` tp s') $
                   tupleCombo PLS.reverse (s0', s1')
    pl <- extendLMR l m r s0 s1
    return $ TransPtSegmOfPlSelfIntersect (pl, p)

newtype CollinearPlSelfIntersect = CollinearPlSelfIntersect
  {getCollinearPlSelfIntersect :: (Polyline, Segm)}
  deriving Show
instance Arbitrary CollinearPlSelfIntersect where
  arbitrary = do
    let mc = minChooseD 3
    l <- mc 0
    m <- mc 0
    r <- mc 0

    CollinearIntersectingSegms (s0, s1, s) <- arbitrary

    let sep = sharedEndpoint s0 s1
    pl <- case (m, sep) of
          (0, Just p) -> extend2Ways l r $
                         intersperse p $
                         filterL (/= p) $
                         mconcat $
                         fmap thps' [s0, s1]
          _ -> extendLMR l m r s0 s1

    return $ CollinearPlSelfIntersect (pl, s)

newtype TransPtEndPtPLSelfIntersect = TransPtEndPtPLSelfIntersect
  {getTransPtEndPtPLSelfIntersect :: PLPT}
  deriving Show
instance Arbitrary TransPtEndPtPLSelfIntersect where
  arbitrary = do
    OneSharedEndpointSegms (s0', s1', p) <- arbitrary

    let (s0, s1) = fromJust $ head $
                   filterL (\(s, s') -> hp s == hp s') $
                   tupleCombo PLS.reverse (s0', s1')

    let mc = minChooseD 2 0
    l <- mc
    m <- mc
    let r = 0
    pl' <- extendLMR l m r s0 s1
    pl <- elements [pl', reverse pl']

    return $ TransPtEndPtPLSelfIntersect (pl, p)

newtype EndPtSegmPlSelfIntersect = EndPtSegmPlSelfIntersect
  {getEndPtSegmPlSelfIntersect :: PLPT}
  deriving Show
instance Arbitrary EndPtSegmPlSelfIntersect where
  arbitrary = do
    SegmAndInteriorPoint (s, p) <- arbitrary

    let mc = minChooseD 2 0
    l <- mc
    r <- mc
    pl' <- ((<|) p) <$> extend2Ways l r (thps' s)
    pl <- elements [pl', reverse pl']

    return $ EndPtSegmPlSelfIntersect (pl, p)

newtype PolylinePointDistance = PolylinePointDistance
  {getPolylinePointDistance :: (PLPT, Squared Distance)}
  deriving Show
instance Arbitrary PolylinePointDistance where
  arbitrary = PolylinePointDistance <$> oneof
    [ (\(pl, p) -> ((pl, p), zero)) . getPolylineAndContainedPoint <$> arbitrary
    , do
        DisjointPolylineAndPoint (pl, p) <- arbitrary
        let d = minimum $ fmap (|--| p) (edges pl)
        return ((pl, p), d) ]

newtype PolylinePolylineDistance = PolylinePolylineDistance
  {getPolylinePolylineDistance :: (PLPL, Squared Distance)}
  deriving Show
instance Arbitrary PolylinePolylineDistance where
  arbitrary = PolylinePolylineDistance <$> oneof
    [ second (const zero) . getPolylineIntersect <$> arbitrary
    , do
        DisjointPolylines (pl0, pl1) <- arbitrary
        let d = minimum $ fmap (uncurry (|--|)) $
                               (allCombo'' (edges pl0) (edges pl1))
        return ((pl0, pl1), d) ]

-- the two polylines share all the polylines except one, which is the inclueded point
-- one polyline contains point and the other does not
newtype MissingPointPolyline = MissingPointPolyline
  {getMissingPointPolyline :: PPP}
  deriving Show
instance Arbitrary MissingPointPolyline where
  arbitrary = do
    (p, pl0) <- suchThat
      (swap <$> (sequenceA . mkSnd elements =<< arbitrary))
      ( liftA2 (&&)
          ((`lengthGT` 2) . snd)
          ((== 1) . uncurry count) )
      :: Gen (Point, Polyline)

    pls <- uncurry thisOrThat $
           mkSnd swap $
           mkSnd ( Polyline . AtLeast .
                   filter (/= p) .
                   getAtLeast . getPolyline )
           pl0
    return $ MissingPointPolyline (pls, p)

newtype Exterior a = Exterior {getExterior :: a} deriving Show
shrinkBothEndsToContainedPoint :: (PPP -> a) -> (a -> PPP) -> a -> [a]
shrinkBothEndsToContainedPoint constructor accessor =
  getList .
  (\(pls, p) ->
    fmap (constructor . (, p)) $
    sortOn (biLiftC' (+) length) $
    ( liftA2 (<>)
        (uncurry allCombo'')
        ( ( uncurry (biLift' (<>)) $
            bimap
              (fmap . (first getExterior .: (,)))
              (fmap . (second getExterior .: flip (,)))
              pls ) .
          swap ) ) $
    bimap' (shrinkPolylineLeavePoint p . getExterior) pls ) .
  first (bimap' Exterior) .
  accessor

shrinkBothEndsToContainedPoint2 :: (PLPT -> a) -> (a -> PLPT) -> a -> [a]
shrinkBothEndsToContainedPoint2 constructor accessor =
  getList .
  (\(pl, p) ->
    fmap (constructor . (, p)) $
    shrinkPolylineLeavePoint p pl ) .
  accessor

shrinkPolylineLeavePoint :: Point -> Polyline -> List Polyline
shrinkPolylineLeavePoint p spl =
  reverse $
  unfoldr (double <$< sh) spl
  where sh pl = let
          f = firstEdge pl `contains` p
          l = lastEdge pl `contains` p
          in if
            | f && l -> Nothing
            | l -> tail pl
            | f -> init pl
            | otherwise -> tailInit pl

-- the last point of the first polyline is the first point of the second polyline
-- the connecting segments are collinear and point in the same direction
newtype SharedEpVectorColinearPls = SharedEpVectorColinearPls
  {getSharedEpVectorColinearPls :: PPP}
  deriving Show
instance Arbitrary SharedEpVectorColinearPls where
  arbitrary = do
    (s, p) <- getSegmAndInteriorPoint <$> arbitrary

    let mc = minChooseD 2 0
    n0 <- mc
    n1 <- mc

    pl0 <- extend2Ways n0 0 (fromFoldable [tp s, p] :: Seq Point)
    pl1 <- extend2Ways 0 n1 (fromFoldable [p, hp s] :: Seq Point)

    return $ SharedEpVectorColinearPls ((pl0, pl1), p)

  shrink = shrinkBothEndsToContainedPoint SharedEpVectorColinearPls getSharedEpVectorColinearPls

-- the last point of the first polyline is the first point of the second polyline
-- the connecting segments are collinear and point in oposite directions
newtype SharedEpRevColinearPls = SharedEpRevColinearPls
  {getSharedEpRevColinearPls :: PPP}
  deriving Show
instance Arbitrary SharedEpRevColinearPls where
  arbitrary = do
    (s, p) <- getSegmAndInteriorPoint <$> arbitrary

    let mc = minChooseD 2 0
    n0 <- mc
    n1 <- mc

    pl0 <- extend2Ways n0 0 (fromFoldable [p, tp s] :: Seq Point)
    pl1 <- extend2Ways 0 n1 $ thps' s

    return $ SharedEpRevColinearPls ((pl0, pl1), p)

  shrink = shrinkBothEndsToContainedPoint SharedEpRevColinearPls getSharedEpRevColinearPls

-- the last point of the first polyline is the first point of the second polyline
-- the connecting segments not collinear
newtype SharedEpNonColinearPls = SharedEpNonColinearPls
  {getSharedEpNonColinearPls :: PPP}
  deriving Show
instance Arbitrary SharedEpNonColinearPls where
  arbitrary = SharedEpNonColinearPls <$> suchThat
    ( (\(pls, p) -> (, p) $ bimap
        (iff ((== p) . hp) id reverse)
        (iff ((== p) . tp) id reverse)
        pls ) .
      getSharedPolylineEndpoint <$> arbitrary )
    (not . collinearL' . bimap firstEdge lastEdge . fst)

  shrink = shrinkBothEndsToContainedPoint SharedEpNonColinearPls getSharedEpNonColinearPls
