{-# LANGUAGE InstanceSigs
           , MultiParamTypeClasses
           , FlexibleInstances
           , UndecidableInstances
           , ScopedTypeVariables
           , AllowAmbiguousTypes #-}

module Range where

import GeometryClasses (Contain(contains, interiorContains))

import Data.Tuple (swap)
import Test.QuickCheck (Arbitrary(arbitrary))


data Range a = Range {lower :: a, upper :: a} deriving (Show, Eq)

overlaps :: (Eq a, Ord a) => Range a -> Range a -> Bool
overlaps = _overlap contains

interiorOverlap :: (Eq a, Ord a) => Range a -> Range a -> Bool
interiorOverlap r0 r1 = _overlap interiorContains r0 r1 || r0 == r1

nRange :: Ord a => a -> a -> Range a
nRange = curry nRange'
nRange' :: Ord a => (a, a) -> Range a
nRange' rs | fst rs < snd rs = uncurry Range rs
           | otherwise       = uncurry Range $ swap rs

-- class NewRange a where
--   nRange :: a -> a -> Range a
--   nRange = curry nRange'
-- 
--   nRange' :: (a, a) -> Range a
--   nRange' = uncurry nRange
-- 
-- instance {-# OVERLAPPABLE #-} Ord a => NewRange a where
--   nRange' rs | fst rs < snd rs = uncurry Range rs
--              | otherwise       = uncurry Range $ swap rs

instance {-# OVERLAPPABLE #-} Ord a => Contain (Range a) a where
  contains :: Range a -> a -> Bool
  contains = _contains (>=)

  interiorContains :: Range a -> a -> Bool
  interiorContains = _contains (>)

_contains :: Ord a => (a -> a -> Bool) -> Range a -> a -> Bool
_contains cmp (Range l u) v = v `cmp` l && u `cmp` v

_overlap :: (Range a -> a -> Bool) -> Range a -> Range a -> Bool
_overlap f r0@(Range l0 u0) r1@(Range l1 u1) =
  any (uncurry f) [(r0, l1), (r0, u1), (r1, l0), (r1, u0)]

instance (Ord a, Arbitrary a) => Arbitrary (Range a) where
  arbitrary = do
    x <- arbitrary
    y <- arbitrary
    return $ nRange x y
