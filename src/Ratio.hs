{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Ratio where

import Data.List (sortOn)

import Test.QuickCheck
  ( Arbitrary(arbitrary, shrink)
  , suchThat
  , choose
  , NonZero(NonZero)
  )


-- [0, 1]
newtype Ratio = Ratio {getRatio :: Double} deriving (Show, Eq, Ord, Num)
instance Arbitrary Ratio where
  arbitrary = Ratio <$> choose (0, 1)

-- range [0, 1)
newtype ZRatio = ZRatio {getZRatio :: Double} deriving (Show, Eq, Ord, Num)
instance Arbitrary ZRatio where
  arbitrary = (ZRatio . getRatio) <$> suchThat arbitrary ((/= 1) . getRatio)

-- Non zero and one ratio (0, 1)
newtype NZRatio = NZRatio {getNZRatio :: Double} deriving (Show, Eq, Ord, Num)
instance Arbitrary NZRatio where
  arbitrary = (NZRatio . getZRatio) <$> suchThat arbitrary ((/= 0) . getZRatio)

  shrink (NZRatio v) = if elem v ss then []
                                    else map NZRatio $ sortOn (abs . (+ negate v)) ss
    where ss = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

-- (-inf, -1) && (1, inf)
newtype NonRatio = NonRatio Double deriving (Show, Eq, Ord, Num)
instance Arbitrary NonRatio where
  arbitrary = do
    -- NonZero num <- arbitrary

    -- den <- suchThat arbitrary (\den -> (num / den) < (-1) || (num / den) > 1)
    -- return $ NonRatio (num / den)

    (NonZero x, NonZero y) <- arbitrary

    let (den:num:_) = sortOn abs [x, y]

    return $ NonRatio $ num / den
