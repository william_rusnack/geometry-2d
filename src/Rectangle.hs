{-# LANGUAGE InstanceSigs #-}

module Rectangle where

import Dimension (abs)
import Area (FindArea(area), (=**=))
import Range (nRange, overlaps, interiorOverlap)
import Vector (Vector(Vector))
import PLS
  ( Point(Point)
  , (.->.)
  , Intersect(intersects, interiorIntersect, ininIntersect)
  )

import Prelude hiding (abs)


newtype RectVec = RectVec Vector deriving (Show, Eq)
instance FindArea RectVec where
  area r = abs (rx =**= ry)
    where RectVec (Vector (rx, ry)) = r


newtype Rect2P = Rect2P (Point, Point) deriving (Show, Eq)
instance Intersect Rect2P where
  intersects :: Rect2P -> Rect2P -> Bool
  intersects
    (Rect2P (Point (xa0, ya0), Point (xa1, ya1)))
    (Rect2P (Point (xb0, yb0), Point (xb1, yb1))) =
      xIntersects && yIntersects
    where xIntersects = nRange xa0 xa1 `overlaps` nRange xb0 xb1
          yIntersects = nRange ya0 ya1 `overlaps` nRange yb0 yb1

  interiorIntersect :: Rect2P -> Rect2P -> Bool
  interiorIntersect
    (Rect2P (Point (xa0, ya0), Point (xa1, ya1)))
    (Rect2P (Point (xb0, yb0), Point (xb1, yb1))) =
      xIntersects && yIntersects
    where xIntersects = nRange xa0 xa1 `interiorOverlap` nRange xb0 xb1
          yIntersects = nRange ya0 ya1 `interiorOverlap` nRange yb0 yb1

  ininIntersect = interiorIntersect


instance FindArea Rect2P where
  area (Rect2P (p0, p1)) = area (RectVec (p0 .->. p1))


-- minimum bounding box (horizontal & vertical sides)
mbb :: [Point] -> Maybe Rect2P
mbb (p:ps) = Just $ Rect2P (foldr minMax (p, p) ps)
  where minMax :: Point -> (Point, Point) -> (Point, Point)
        minMax p' (minP, maxP) = (minimums p' minP, maximums p' maxP)

        minimums (Point (x0, y0)) (Point (x1, y1)) = Point (min x0 x1, min y0 y1)
        maximums (Point (x0, y0)) (Point (x1, y1)) = Point (max x0 x1, max y0 y1)
mbb [] = Nothing
