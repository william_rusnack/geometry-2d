{-# LANGUAGE GeneralizedNewtypeDeriving
           , MultiParamTypeClasses
           , InstanceSigs
           , FlexibleInstances
           , ScopedTypeVariables
           , StandaloneDeriving
           , UndecidableInstances
           , ViewPatterns
           , PartialTypeSignatures
           , LambdaCase
           , DataKinds
           , TypeOperators
           , TypeFamilies
           , TypeApplications #-}

module Ring where

import GeometryClasses
  ( SelfIntersecting(selfIntersecting)
  , Contain(contains')
  )
import Dimension (Absolute)
import Vector
  ( xp
  , parallel
  , getY
  )
import PLS
  ( (.->.)
  , IntersectPoint(intersectPoint')
  , intersectSegm'
  , Line(Line)
  , (..-)
  , Point
  , Segm
  , TailHeadPoints(tp, hp, thps')
  , Edges(edges)
  , DistanceBetween((|--|))
  , Intersect(intersects, intersects', interiorIntersect)
  )
import Polyline
  ( Polyline
  , Polyline_(Polyline, getPolyline)
  , ToPolyline(toPolyline)
  , isClosed
  , closePl
  )

import Utilities
  ( bimap'
  , zero
  , (<$<)
  , maybeBool
  , mkSnd
  , ifv
  )
import Utilities.Arbitrary (minChoose)

import Unified
  ( Length
  , FilterNE
  , Cons((<|), head, tail)
  , Snoc
  , Drop(drop)
  , Singleton(singleton)
  , Reverse(reverse)
  , SplitAtMay
  , Sort(sortOn)
  , Take(take)
  , uncons
  , Filter(filter)
  , sortOn
  , Empty(empty)
  , fromFoldable
  , takeWhileJust
  , init
  , headNE
  , LastNE(lastNE)
  )
import Unified.Utilities
  ( iterate
  , concat
  , mapMaybe
  , uncons3
  , take4
  , take2
  )
import Unified.Collections.List (List(List))
import Unified.Utilities.List
  ( secondCombo
  , zip3L
  , mapL
  , toPairsL
  , rotationsL
  )
import Unified.AtLeast
  ( NonEmpty
  , AtLeast(AtLeast, getAtLeast)
  , tailAL
  , consAL
  )
import Unified.Arbitrary (localUnique)

import Control.Applicative (liftA2, liftA3, (<|>))
import Control.Arrow ((***), (&&&))
import Control.Monad (mfilter, (=<<), (<=<))
import Data.Bitraversable (bisequenceA)
import Data.Foldable (minimumBy)
import Data.Maybe (fromMaybe)
import Data.Semigroup (Semigroup)
import Data.Tuple.HT (snd3)

import Test.QuickCheck (Arbitrary(arbitrary))

import Prelude hiding (sqrt, tail, init, reverse, head, last, filter, length, (!!), (++), splitAt, take, drop, concat, iterate)


type Ring = Ring_ Point
newtype Ring_ a = Ring {getRing :: Polyline_ a}
  deriving ( Show, Eq, Foldable, Semigroup
           , Cons, Snoc, Length, Reverse, SplitAtMay, Take, Drop, LastNE, FilterNE, Sort )

toRing :: ToPolyline a => a -> Maybe Ring
toRing = _toRing id

toRingUnClosed :: ToPolyline a => a -> Maybe Ring
toRingUnClosed = _toRing closePl

_toRing :: ToPolyline a => (Polyline -> Polyline) -> a -> Maybe Ring
_toRing modPolyline =
  return .
  (\r -> (if isCCW r then id else reverse) r) .
  Ring <=<
  tail <=<
  mfilter isClosed .
  ( modPolyline <$<
    toPolyline )

class IsCCW a where
  isCCW :: a -> Bool


instance {-# OVERLAPS #-} ToPolyline Ring where
  toPolyline r = Just $ getRing $ lastNE r <| r

instance IsCCW Ring where
  -- isCCW :: Ring -> Bool
  -- isCCW r = (left .->. center) `xp` (center .->. right) > zero
  --   where (left, center, right) = minimumBy sortAng angles

  --         sortAng (_,c0,_) (_,c1,_)
  --           | y0 < y1 = LT
  --           | otherwise = GT
  --           where Point (_, y0) = c0
  --                 Point (_, y1) = c1

  --         angles :: List (Point, Point, Point)
  --         angles = (zip3L :: Ring -> Ring -> Ring -> List (Point, Point, Point)) ps (drop 1 ps) (drop 2 ps)
  --         ps :: Ring
  --         ps = r <> take 2 r

  isCCW :: Ring -> Bool
  isCCW = maybeBool .
    ( (> zero) .
      (\(left, center, right) -> (left .->. center) `xp` (center .->. right)) .
      minimumBy (curry $ ifv (uncurry (<) . bimap' (getY @Absolute . snd3)) LT GT) <$<
      liftA3 (liftA3 zip3L) Just (drop 1) (drop 2) <=<
      uncurry (<>) <$<
      sequenceA .
      mkSnd (take 2) )

instance Edges Ring_ where
  edges = edges . (\pl -> lastNE pl <| pl) . getRing

instance SelfIntersecting Ring where
  selfIntersecting :: Ring -> Bool
  selfIntersecting r =
    any (uncurry interiorIntersect) (toPairsL es) ||
    (maybeBool $ any intersects' <$> secondCombo <$> tail (getAtLeast es)) ||
    (maybeBool $ any (intersects $ headNE es) <$> (drop 2 =<< init (getAtLeast es)))
    where es = edges r

-- maxDistance :: Ring -> Ring -> Distance
-- maxDistance r0@(Ring ps0)
--             r1@(Ring ps1) =
--               (sqrt . maximum . concat)
--                 [ (dists r0 ps1)
--                 , (dists r1 ps0) ]
--   where dists r ps = fmap ((|--|) r) ps

-- if a ring has intersects it is split into multiple rings at those intersects
partition :: (Singleton t, Semigroup (t Ring)) => Ring -> t Ring
partition (splitRing -> Just (r0, r1)) = partition r0 <> partition r1
partition r = singleton r

type IncEdges t = ((Segm, Segm), (NonEmpty t Segm, NonEmpty t Segm))

_incSplits :: forall t a. (Length t, Cons t, Singleton t, Drop t, Foldable t)
           => t a -> List ((a, a), (t a, t a))
-- _incSplits es | es `lengthGTEq` 4 = catMaybes $ ir =<< rotations es
--               | otherwise = empty
--   where ir (uncons3 -> Just ((x0, x1, x2, xs))) =
--           takeWhile isJust $ iterate next (Just ((x0, x2), (singleton x1, xs)))
--           where next :: Maybe ((a, a), (t a, t a)) -> Maybe ((a, a), (t a, t a))
--                 next (Just ((s0, s1), (ss, uncons -> Just (h, t))))
--                   | null t = Nothing
--                   | otherwise = Just ((s0, h), (s1 <| ss, t))
--                 next _ = Nothing
--         ir _ = error "_incSplits guards failed"

_incSplits =
  fromMaybe empty .
  ( ( (takeWhileJust :: List (Maybe b) -> List b) .
      iterate ( (=<<)
                (\case ((s0, s1), (ss, uncons -> Just (h, t@(null -> False)))) ->
                         Just ((s0, h), (s1 <| ss, t))
                       _ -> Nothing ) ) .
      (\((x0, x1, x2, _), xs) -> Just ((x0, x2), (singleton x1, xs))) ) <$<
    bisequenceA .
    (take4 &&& drop 3) )

_addP :: forall t.
         (Cons t, Length t)
      => Point -> t Point -> t Point
_addP p ((AtLeast @2 &&& take2) -> (ps, Just (p0, p1)))
  | p == p0 = getAtLeast ps
  | l0 `parallel` l1 = getAtLeast $ p `consAL` tailAL ps
  | otherwise = getAtLeast $ p <| ps
  where l0 = Line (p0, p1)
        l1 = Line (p, p0)
_addP p ps = p <| ps

-- If a ring has a self intersect the two parts will be split and returned as
-- two seperate Rings.
-- Must be called multiple times if it is going to remove all self intersects
splitRing :: Ring -> Maybe (Ring, Ring)
splitRing r = head $
  -- handles > 2 point rings
  (mapMaybe split $ _incSplits $ edges r) <>
  -- handles 2 point rings
  ( mapMaybe ( bisequenceA .
               ( (\(p0, p1, ps) -> Just $
                             if p0 == p1 then p1 <| ps
                                         else p0 <| p1 <| ps ) ***
               ( toRing . (\(sp0, sp1) -> List [sp0, sp1, sp0]) ) ) .
               (\(s, lr, ps) ->
                 case lr of
                 Left  p -> ((p, hp s, ps), (tp s, p))
                 Right p -> ((hp s, p, ps), (tp s, p)) ) ) $
    filter  ( contains' .
              (\case (s, Left  p, _) -> (s, p)
                     (s, Right p, _) -> (s, p) ) ) $
    mapMaybe (\case (Just s, lr, ps) -> Just (s, lr, ps)
                    _ -> Nothing ) $
    concat $
    mapL (\((x,y,z),r') -> fromFoldable [ (y ..- x :: Maybe Segm, Right z, r')
                                        , (y ..- z, Left  x, r') ]) $
    mapMaybe uncons3 $
    rotationsL r )
  where --split :: IncEdges t -> Maybe (Ring, Ring)
        split =  liftA2 (<|>)
                        (sf $ thps' <$< intersectSegm')
                        (sf $ (singleton :: a -> List a) <$< intersectPoint')

        -- sf :: ((Segm, Segm) -> Maybe (List Point))
        --        -> IncEdges t
        --        -> Maybe (Ring, Ring)
        sf f (ss, (es0, es1)) =
          bisequenceA . (\i -> (cp i (reverse es0), cp i es1)) =<< f ss
          where -- cp :: List Point -> List Segm -> Maybe Ring
                cp ips (toPolyline -> Just pl) =
                  toRing . (\t -> foldr _addP t nps) . getPolyline =<< tail pl
                  where nps = l <| ips' <> singleton h
                        ips' = sortOn (|--| h) ips
                        h = tp pl
                        l = hp pl
                cp _ _ = Nothing

-- -- use ViewPatterns
-- removeHairs :: Ring -> Ring
-- removeHairs r = case filter hair $ fullRotation r of
--                 (r':_) -> removeHairs $ case rmh r' of
--                                         Just r'' -> r''
--                                         _ -> error "rmh should return a ring"
--                 _ -> r
--   where rmh :: Ring -> Maybe Ring
--         rmh (Ring ps) = toRing (trace ("aps: " ++ show ps) $ if fp == lp
--                                                             then ps'
--                                                             else lp : ps')
--           where
--                 fp = head ps'
--                 lp = last ps'
--                 ps' = tail ps

--         hair :: Ring -> Bool
--         hair r' = fromMaybe False $
--           (\(e0, e1) -> any (uncurry contains') [(e0, hp e1), (e1, tp e0)]) <$>
--           take2 (edges r')


-- -- creates new offset rings a distance away from 2
-- offset :: Distance -- tolerance
--        -> Distance -- offset distance (positive to outside, negative to inside)
--        -> Ring -- ring to offset
--        -> List Ring -- resulting offset rings (may have length of 0,1,>1)
-- -- offset t o r = case partition <$> (toRing =<< spl) of
-- --                Just rs -> filter ((>= sq (abs o - t)) . (|--| r)) rs
-- --                Nothing -> []
-- offset t o r = case partition <$> rhr of
--                Just rs -> filter ((>= sq (abs o - t)) . (|--| r)) rs
--                Nothing -> []
--   where
--         -- ringFilters :: List (Ring -> Bool)
--         -- ringFilters = [ (>= sq (abs o - t)) . (|--| r)
--         --               , (\)
--         --               ]



--         -- spl :: Maybe Polyline
--         -- spl = trace ("spl: " ++ show spl') spl'
--         -- spl' = case (cpl, CM.join $ lastMay sps, l2Ward, flWard) of
--         --       -- (Just (Polyline ps), _ , Just Rightward, Just Rightward) ->
--         --       --   trace "case Rightward Rightward" $ 
--         --       (Just (Polyline ps), _ , Just Rightward, Just Leftward) ->
--         --         trace "case Rightward Leftward" $ toPolyline $ last ps' : ps'
--         --         where ps' = init $ tail ps
--         --       -- (Just (Polyline ps), Just sp, _) -> trace "case Just sp" $ toPolyline $ sp : tail ps
--         --       -- (pl, _, _) -> trace "case pl" pl


--         -- flWard = trace ("flWard: " ++ show flWard') flWard'
--         -- flWard' = ut $ bisequenceA (headMay es, snd $ unzipMaybe l2es)

--         -- l2Ward = trace ("l2Ward: " ++ show l2Ward') l2Ward'
--         -- -- l2Ward' = head es `toward` last es
--         -- l2Ward' = ut l2es

--         -- ut = fmap $ uncurry toward
--         -- l2es@(_) = last2 es
--         rhr :: Maybe Ring
--         rhr = removeHairs <$> (toRing =<< cpl)


--         cpl :: Maybe Polyline
--         cpl = trace ("cpl: " ++ show cpl') cpl'
--         cpl' = closePl <$> (
--                  uncurry (foldlM combine) =<<
--                  bisequenceA (headMay pls, tailMay pls)
--                )

--         -- correct start point
--         pls = trace ("pls: " ++ show pls') pls'
--         pls' = catMaybes pls''
--         (pls'', sps) = unzip $ offPl (last es : es) Nothing

--         -- create offset
--         offPl :: List Segm -> Maybe Point -> List (Maybe Polyline, Maybe Point)
--         offPl (s0':s1':ss) sp = (swapTP =<< pl, sp') : offPl (s1' : ss) sp'
--           where
--                 (pl, sp') = plsp
--                 plsp :: (Maybe Polyline, Maybe Point)
--                 plsp = case s0' `toward` s1' of
--                             Leftward ->
--                               case s2pl s0 `combine` a2pl a of
--                               Just pl' -> (Just pl', Nothing)
--                               Nothing -> error "polylines should share an endpoint but dont"
--                             Rightward ->
--                               case intersectPoint s0 s1 of
--                               Just p -> (toPolyline [tp s0, p, hp s1], Just $ hp s1)
--                               Nothing -> (toPolyline $ thps' s0 ++ thps' s1, Just $ hp s1)
--                             _ -> undefined

--                 a :: ArcEsT
--                 a = case nArcEsT (hp s0) (tp s1) =<< toUv s0 of
--                     Just a -> a
--                     Nothing -> error $ "bad arc: " ++ show (ArcEsT (hp s0, tp s1, _toUv s0))

--                 s0 = tr s0'
--                 s1 = tr s1'

--                 swapTP :: Polyline -> Maybe Polyline
--                 swapTP pl' =
--                   case sp of
--                   Just pt -> toPolyline =<< (((<|) pt) <$> tail pl')
--                   Nothing -> Just pl'

--         offPl _ _ = []

--         tr :: Segm -> Segm
--         tr = toRight o
--         s2pl :: Segm -> Polyline
--         s2pl = approxAsPolyline t
--         a2pl :: ArcEsT -> Polyline
--         a2pl = approxAsPolyline t

--         es = trace ("es: " ++ show (edges r)) $ edges r


-- Arbitrary Helpers --
instance Arbitrary Ring where
  arbitrary = do
   l <- minChoose 2
   Ring . Polyline . AtLeast . fromFoldable <$> localUnique l (empty :: List Point)

-- genRing :: Int -> Gen Ring
-- genRing l | l >= 2 = fromJust . fromFoldableMay <$> localUnique l (empty :: List _)
--           | otherwise = error "genRing needs a value >= 2"

