module RingArbitrary where

import Ring


import Vector
  ( toNZV
  , ang
  )
import PLS
  ( Point
  , Segm
  , edges
  , intersects'
  , thps'
  , (.<-.)
  , avgPoint
  )
import SegmArbitrary (IntersectingSegms(getIntersectingSegms))
import Polyline
  ( Polyline_(Polyline)
  )
import PolylineArbitrary (untanglePoints)

import Utilities
  ( bimap'
  , (<$<)
  , iff
  )
import Utilities.Arbitrary (minChoose, minChooseD)

import Unified
  ( splitAtMay'
  , splitAtMay
  , filter
  , head
  , empty
  , last
  , tail
  , fromFoldableMay
  , sortOn
  )
import Unified.Utilities
  ( (<$|)
  , rotations
  )
import Unified.Collections.List (List)
import Unified.Utilities.List (allCombo'')
import Unified.AtLeast (AtLeast(AtLeast))
import Unified.Arbitrary (uniqueValues, localUnique)

import Control.Monad
import Data.Maybe (fromJust)
import Data.Tuple.HT (swap)

import Test.QuickCheck
  ( Arbitrary(arbitrary)
  , Gen
  , choose
  )

import Prelude hiding (sqrt, tail, init, reverse, head, last, filter, length, (!!), (++), take, drop, concat, iterate)


-- second value is the center point that the other points are about
newtype CCWClosed = CCWClosed
  {getCCWClosed :: (Ring, Point)}
  deriving Show
instance Arbitrary CCWClosed where
  arbitrary = do
    l <- minChoose 3
    i <- choose (0, l - 2)
    ips <- uniqueValues l (empty :: List Point)
    let p = avgPoint $ AtLeast ips

    fps <- fromJust .
           ( toRing <=<
             (\ps -> last ps <$| ps) <=<
             uncurry (<>) . swap <$<
             splitAtMay i .
             sortOn (ang . fromJust . toNZV . (.<-. p)) ) <$>
           iff (elem p)
               (filter (/= p) <$< uniqueValues 1)
               return
           ips

    return $ CCWClosed (fps, p)

newtype SelfIntersectingRing = SelfIntersectingRing
  {getSelfIntersectingRing :: (Ring, Either Point Segm)}
  deriving Show
instance Arbitrary SelfIntersectingRing where
  arbitrary =
    SelfIntersectingRing <$>
    ( (\(s0, s1, i) -> do
        let mc = minChooseD 2 0

        l0 <- mc
        ps0 <- localUnique l0 $ thps' s0

        l1 <- mc
        ps1 <- localUnique l1 $ thps' s1

        let r = Ring $ Polyline $ fromJust $ fromFoldableMay $ ps0 <> ps1
        return (r, i) ) =<<
    (getIntersectingSegms <$> arbitrary) )

newtype NonSelfIntersectingRing = NonSelfIntersectingRing
  {getNonSelfIntersectingRing :: Ring}
  deriving Show
instance Arbitrary NonSelfIntersectingRing where
  arbitrary = do
    l <- minChoose 3
    nonSelfIntersectingRing l

-- l - number of points in ring
nonSelfIntersectingRing :: Int -> Gen NonSelfIntersectingRing
nonSelfIntersectingRing l =
  NonSelfIntersectingRing .
  fromJust .
  ( Ring .
    Polyline <$<
    fromFoldableMay <=<
    tail <=<
    untanglePoints <$<
  (\ups -> last ups <$| ups) ) <$>
  uniqueValues l (empty :: List Point)

newtype DisjointRings = DisjointRings
  {getDisjointRings :: (Ring, Ring)}
  deriving Show
instance Arbitrary DisjointRings where
  arbitrary = do
    n <- minChoose 4
    let is = [2..n-2]

    ring <- getNonSelfIntersectingRing <$> nonSelfIntersectingRing n

    return $ DisjointRings $ fromJust $
             ( head .
               filter ( not .
                        any intersects' .
                        uncurry allCombo'' .
                        bimap' edges ) ) =<<
             ( sequenceA $
               fmap splitAtMay' $
               allCombo'' is (rotations ring :: List Ring) )

