{-# LANGUAGE InstanceSigs
           , ViewPatterns
           , TypeSynonymInstances
           , FlexibleInstances
           , DeriveFoldable
           , GeneralizedNewtypeDeriving
           , DataKinds
           , TypeApplications
           , PartialTypeSignatures
           , ScopedTypeVariables #-}

module RingDL where


import GeometryClasses
  ( SelfIntersecting(selfIntersecting)
  )
import qualified PLS
import PLS
  ( Point
  , TailHeadPoints(tp, hp)
  , Segm
  , Intersect(intersects, intersects', interiorIntersect, interiorIntersect', ininIntersect)
  , reverse
  )
import DSegm
  ( DSegm(DSegm, setNP, getSegm)
  , NumeralPrefix(Uni, Bi, Pre, Pst, PPB)
  , nDS
  )
import Ring
  ( Ring
  , toRing
  , IsCCW(isCCW)
  )

import Unified
  ( Cons((<|), uncons)
  , Snoc
  , Length
  , headNE
  , lastNE
  , head
  , tail
  , filter
  , fromFoldable
  )
import Unified.Utilities (uncons2)
import Unified.Collections.List (List(List))
import Unified.Utilities.List
  ( secondCombo
  , allCombo''
  , zipL
  , mapL
  , permutations
  , rotationsL
  )
import Unified.AtLeast (AtLeast(AtLeast, getAtLeast), dropAL, tailAL)

import Control.Applicative (liftA2)
import Control.Monad ((<=<))
import Data.Maybe (fromJust)
import Data.Semigroup (Semigroup)

import Prelude hiding (reverse, map, head, drop, tail, init, last, filter)


type RingDL = RingDL_ DSegm
newtype RingDL_ a = RingDL (AtLeast 2 List a)
  deriving (Show, Eq, Foldable, Cons, Snoc, Semigroup, Length)

r2rdl :: Ring -> RingDL
r2rdl = RingDL . fmap (DSegm Uni) . AtLeast . getAtLeast . PLS.edges

rdl2ps :: RingDL -> List Point
rdl2ps r = (tp $ fromJust $ head r) <| mapL hp r

rds2r :: RingDL -> Ring
rds2r = fromJust . toRing . rdl2ps

edges :: RingDL -> List Segm
edges = mapL getSegm

-- linkRings :: [[RingDL]] -> -- rings to link together
--              Maybe RingDL    -- all composite rings
-- linkRings (er:irs) = link $ map ((:) er) (permutations irs)
--   where link :: [[RingDL]] -> Maybe RingDL
--         link ((r0:r1:rs):perms) = case link2Rings r0 r1 rs of
--                                   []    -> link perms
--                                   (r:_) -> link ((r:rs):perms)
--         link ([r]:_) = Just r
--         link []      = Nothing
--         link ([]:perms) = link perms
-- linkRings [] = Nothing

linkRings :: forall t.
             Foldable t
          => t RingDL -- rings to link together
          -> Maybe RingDL    -- all composite rings
linkRings (uncons . fromFoldable -> Just (er, irs)) = link $ fmap ((<|) er) (permutations irs)
  where link :: List (List RingDL) -> Maybe RingDL
        link (uncons -> Just (uncons2 -> Just ((r0, r1), rs), perms)) =
          link $
          case head $ link2Rings r0 r1 rs of
            Just r -> (r <| rs) <| perms
            Nothing -> perms
        link (head <=< head -> Just r) = Just r
        link (tail -> Just perms) = link perms
        link _ = Nothing
linkRings _ = Nothing

link2Rings :: Foldable t
           => RingDL -- ring to link
           -> RingDL -- ring to link
           -> t RingDL -- combined ring cannot intersect these rings
           -> List RingDL    -- all valid ring combinations
link2Rings (rotationsL -> rs0) (rotationsL -> rs1) ors =
  filter (liftA2 (&&) (not . selfIntersecting)
                      (not . flip any ors . intersects) ) $
  fmap (uncurry combineRings) $
  allCombo'' rs0 rs1

combineRings :: RingDL -> RingDL -> RingDL
combineRings (uncons2 -> Just ((r00,r01),rs0))
             (uncons2 -> Just ((r10,r11),rs1))
             = (pre r00 <| c         <| post r11 <| rs1) <>
               (pre r10 <| reverse c <| post r01 <| rs0)
  where c = fromJust $ nDS Bi (hp r00) (hp r10)

        pre = setD Pre
        post = setD Pst

        setD np dl@(DSegm np' _) | np == np' = dl
                                 | otherwise = dl {setNP = s np'}
          where s Uni = np
                s Bi  = Bi
                s _   = PPB
combineRings _ _ = undefined


instance IsCCW RingDL where
  isCCW = isCCW . rds2r

instance Intersect RingDL where
  intersects (mapL getSegm -> es0) (mapL getSegm -> es1) =
    any intersects' $ allCombo'' es0 es1

  interiorIntersect = intersects

  ininIntersect = interiorIntersect

instance SelfIntersecting RingDL where
  selfIntersecting (RingDL ls) =
    any interiorIntersect' (zipL (lastNE ls <| ls) ls) ||
    any intersects' (secondCombo $ tailAL ls) ||
    any (intersects $ headNE ls) (dropAL @2 ls)

