{-# LANGUAGE ScopedTypeVariables
           , TupleSections #-}

module SegmArbitrary where

import PLS
  ( TailHeadPoints(tp, hp, thps')
  , Point
  , Segm(Segm)
  , (..-)
  , f2p
  , nS''
  , nS'''
  , (.+->)
  , (.<-.)
  , hasEndpoint
  , reverse
  )

import LineArbitrary
  ( getLinePointDisjoint
  , getParallelNotCollinearLines
  )

import GeometryClasses
  ( Contain(contains)
  )
import Dimension
  ( (=*~)
  , (=+=)
  , (~*=)
  , Distance
  , fromScalar
  , negate
  , sq
  , Squared
  )
import Vector
  ( Magnitude(magSqrd)
  , magSqrd
  , NZVector(NZVector)
  , perp
  , setLength
  , ToNZV(toNZV, _toNZV)
  , ToUv(_toUv)
  , ToVec(toVec)
  , Unit
  , Vector
  , Vector
  , within90
  , parallel
  )
import Ratio (Ratio(Ratio), NZRatio(NZRatio))

import Utilities
  ( bimap'
  , t3to2
  , t4to3
  , zero
  )
import Utilities.Arbitrary (NonEqual(NonEqual))

import Unified.Utilities.List (tupleCombo)

import Data.Maybe (fromJust)
import Data.Tuple (swap)
import Data.Tuple.HT (mapFst, mapThd3)
import Control.Applicative (liftA2)

import Test.QuickCheck
  ( Arbitrary(arbitrary)
  , Gen
  , NonZero(NonZero)
  , Positive(Positive, getPositive)
  , suchThat
  )
import Unified.QuickCheck
  ( elements
  , frequency
  , oneof
  )

import Prelude hiding (map, negate, reverse, pi, sqrt)



newtype IntersectingSegms = IntersectingSegms
  {getIntersectingSegms :: (Segm, Segm, Either Point Segm)}
  deriving Show
instance Arbitrary IntersectingSegms where
  arbitrary = IntersectingSegms <$>
    ( oneof $
        ( ( (\(s0, s1, p) -> (s0, s1, Left p) ) .
            getOneSharedEndpointSegms <$> arbitrary ) :
          interiorIntersectingGens :: [Gen (Segm, Segm, Either Point Segm)] ) )

-- interior intersect
newtype InteriorIntersectingSegms = InteriorIntersectingSegms
  {getInteriorIntersectingSegms :: (Segm, Segm, Either Point Segm)}
  deriving Show
instance Arbitrary InteriorIntersectingSegms where
  arbitrary = InteriorIntersectingSegms <$> oneof interiorIntersectingGens

interiorIntersectingGens :: [Gen (Segm, Segm, Either Point Segm)]
interiorIntersectingGens =
  [ mapThd3 Left . getInteriorPointIntersectingSegms <$> arbitrary
  , mapThd3 Left . getEndInteriorIntersectingSegms <$> arbitrary
  , (\(s0, s1) -> (s0, s1, Right s0)) . getTwoSharedEndpointSegms <$> arbitrary ]

-- interior or boundry intersect
newtype SegmsIntersectingAtPoint = SegmsIntersectingAtPoint
  {getSegmsIntersectingAtPoint :: (Segm, Segm, Point)}
  deriving Show
instance Arbitrary SegmsIntersectingAtPoint where
  arbitrary = SegmsIntersectingAtPoint <$> oneof
    [ getInteriorPointIntersectingSegms <$> arbitrary
    , getEndInteriorIntersectingSegms <$> arbitrary
    , getOneSharedEndpointSegms <$> arbitrary ]

-- interior intersect
newtype InteriorPointIntersectingSegms = InteriorPointIntersectingSegms
  {getInteriorPointIntersectingSegms :: (Segm, Segm, Point)}
  deriving Show
instance Arbitrary InteriorPointIntersectingSegms where
  arbitrary = do
    p <- arbitrary
    NonEqual (u0, u1) <- arbitrary :: Gen (NonEqual Unit)
    fs0 <- arbitrary
    fs1 <- arbitrary

    let nl u (f0, f1) = fromJust ( (p .+-> setLength (tl f0) u) ..-
                                   (p .+-> setLength (negate $ tl f1) u) )
          where tl = fromScalar . getPositive

    return $ InteriorPointIntersectingSegms (nl u0 fs0, nl u1 fs1, p)

-- endpoint of one segment lies on the interior of the other
-- segments are never collinear
-- interior intersect
newtype EndInteriorIntersectingSegms = EndInteriorIntersectingSegms
  {getEndInteriorIntersectingSegms :: (Segm, Segm, Point)}
  deriving Show
instance Arbitrary EndInteriorIntersectingSegms where
  arbitrary = do
    s0@(Segm l) <- arbitrary

    NZRatio r <- arbitrary
    let p0 = tp s0 .+-> toVec s0 =*~ r
    p1 <- suchThat arbitrary (not . contains l)
    let ps = (p0, p1)

    s1 <- elements $ fmap nS''' [ps, swap ps]

    EndInteriorIntersectingSegms <$> elements [(s0, s1, p0), (s1, s0, p0)]

-- no intersect
newtype DisjointSegms = DisjointSegms
  {getDisjointSegms :: (Segm, Segm)}
  deriving Show
instance Arbitrary DisjointSegms where
  arbitrary = DisjointSegms <$> oneof
    [ getAngledDisjointSegms <$> arbitrary
    , t3to2 . getParallelNotCollinearLines <$> arbitrary
    , getCollinearDisjointSegms <$> arbitrary
    , fst . getDisjointSegmAndSegmEndpoint <$> arbitrary ]

-- no intersect
newtype AngledDisjointSegms = AngledDisjointSegms
  {getAngledDisjointSegms :: (Segm, Segm)}
  deriving Show
instance Arbitrary AngledDisjointSegms where
  arbitrary = do
    p <- arbitrary
    NonEqual (u0, u1) <- arbitrary

    NonEqual fs0' <- arbitrary
    let (f00, f01) = bimap' (fromScalar . getPositive) fs0'
    let mp0 f = p .+-> setLength f u0
    let s0 = fromJust (mp0 f00 ..- mp0 f01)

    NonEqual fs1' <- arbitrary
    let (f10, f11) = bimap' (fromScalar . getPositive) fs1'
    let vl0 = toVec s0
    f12 <- arbitrary
    let mp1 = ((.+->) p) .
              (=+= vl0 =*~ f12) . -- translates s1 along s0's vec so they don't always point directly at p
              (`setLength` u1)
    let s1 = fromJust (mp1 f10 ..- mp1 f11)

    -- random numbers so that the order of s0 and s1 can be interchangeable
    NonEqual (r0, r1) <- arbitrary

    return $ AngledDisjointSegms (if r0 < (r1 :: Int) then (s0, s1) else (s1, s0))

-- interior or boundry or interior boundry intersect
newtype SharedEndpointSegms = SharedEndpointSegms
  {getSharedEndpointSegms :: (Segm, Segm, Point)}
  deriving Show
instance Arbitrary SharedEndpointSegms where
  arbitrary = frequency $ fmap (fmap $ fmap SharedEndpointSegms)
    [ (1,  (\(s0, s1) -> (s0, s1, tp s0)) . getTwoSharedEndpointSegms <$> arbitrary)
    , (10, getOneSharedEndpointSegms <$> arbitrary)]

-- boundry or interior boundry intersect
newtype OneSharedEndpointSegms = OneSharedEndpointSegms
  {getOneSharedEndpointSegms :: (Segm, Segm, Point)}
  deriving Show
instance Arbitrary OneSharedEndpointSegms where
  arbitrary = do
    s <- arbitrary

    let sps = thps' s
    p0 <- elements sps
    p1 <- suchThat arbitrary (not . (`elem` sps))

    ps <- elements [(p0, p1), (p1, p0)]

    return $ OneSharedEndpointSegms (s, fromJust $ uncurry (..-) ps, p0)

-- interior boundry intersect
newtype TwoSharedEndpointSegms = TwoSharedEndpointSegms
  {getTwoSharedEndpointSegms :: (Segm, Segm)}
  deriving Show
instance Arbitrary TwoSharedEndpointSegms where
  arbitrary = TwoSharedEndpointSegms <$> oneof [ getIdenticalSegms <$> arbitrary
                                               , getReversedSegms <$> arbitrary ]

newtype HeadToTailSegms = HeadToTailSegms
  {getHeadToTailSegms :: (Segm, Segm)}
  deriving Show
instance Arbitrary HeadToTailSegms where
  arbitrary = do
    s0 <- arbitrary

    let h = hp s0
    NZVector v <- arbitrary
    let s1 = nS'' h $ h .+-> v

    return $ HeadToTailSegms (s0, s1)

newtype HeadToHeadSegms = HeadToHeadSegms
  {getHeadToHeadSegms :: (Segm, Segm)}
  deriving Show
instance Arbitrary HeadToHeadSegms where
  arbitrary = HeadToHeadSegms .
              fmap reverse .
              getHeadToTailSegms <$>
              arbitrary

newtype TailToTailSegms = TailToTailSegms
  {getTailToTailSegms :: (Segm, Segm)}
  deriving Show
instance Arbitrary TailToTailSegms where
  arbitrary = TailToTailSegms .
              mapFst reverse .
              getHeadToTailSegms <$>
              arbitrary

newtype IdenticalSegms = IdenticalSegms
  {getIdenticalSegms :: (Segm, Segm)}
  deriving Show
instance Arbitrary IdenticalSegms where
  arbitrary = IdenticalSegms . (\s -> (s, s)) <$> arbitrary

newtype ReversedSegms = ReversedSegms
  {getReversedSegms :: (Segm, Segm)}
  deriving Show
instance Arbitrary ReversedSegms where
  arbitrary = ReversedSegms . (\s -> (s, reverse s)) <$> arbitrary

-- third Segm is the intersecting overlap of the first and second Segms
newtype CollinearIntersectingSegms = CollinearIntersectingSegms
  {getCollinearIntersectingSegms :: (Segm, Segm, Segm)}
  deriving Show
instance Arbitrary CollinearIntersectingSegms where
  arbitrary = CollinearIntersectingSegms <$> oneof
    [ getBoundryBoundryInteriorIntersectingSegms <$> arbitrary
    , t4to3 . getCollinearEndInteriorIntersectingSegms <$> arbitrary ]

-- second segment lies completely within the first
-- does not share endpoints
newtype SegmAndInteriorSegment = SegmAndInteriorSegment
  {getSegmAndInteriorSegment :: (Segm, Segm)}
  deriving Show
instance Arbitrary SegmAndInteriorSegment where
  arbitrary = do
    s0 <- arbitrary
    let v = toVec s0
    let mv = (.+->) (tp s0) . (~*= v)

    NonEqual (NZRatio r0, NZRatio r1) <- arbitrary
    let Just s1 = mv r0 ..- mv r1

    return $ SegmAndInteriorSegment (s0, s1)


-- third Segm is the intersecting Segm
-- never shares an endpoint
-- one endpoint of the other segment always lies in the interior of the segment
newtype BoundryBoundryInteriorIntersectingSegms = BoundryBoundryInteriorIntersectingSegms
  {getBoundryBoundryInteriorIntersectingSegms :: (Segm, Segm, Segm)}
  deriving Show
instance Arbitrary BoundryBoundryInteriorIntersectingSegms where
  arbitrary = do
    s0 <- arbitrary
    let p = tp s0
    let v = toVec s0

    NZRatio r0 <- arbitrary
    Positive r1 <- fmap (+ 1) <$> arbitrary

    let mp r = p .+-> v =*~ r
    let Just s1 = mp r0 ..- mp r1

    (s, s') <- elements $ tupleCombo reverse (s0, s1)

    let Just sc = tp s1 ..- hp s0

    BoundryBoundryInteriorIntersectingSegms . (\s'' -> (s, s', s'')) <$>
      elements [sc, reverse sc]

-- point is the shared endpoint
-- third Segm is the intersecting overlap of the first and second Segms
-- has interior overlap
newtype CollinearEndInteriorIntersectingSegms = CollinearEndInteriorIntersectingSegms
  {getCollinearEndInteriorIntersectingSegms :: (Segm, Segm, Segm, Point)}
  deriving Show
instance Arbitrary CollinearEndInteriorIntersectingSegms where
  arbitrary = do
    s0 <- arbitrary

    let p = tp s0
    Positive f <- arbitrary
    let Just s1 = p ..- (p .+-> toVec s0 =*~ f)

    let s2 = if f < 1
             then s1
             else s0

    CollinearEndInteriorIntersectingSegms . (\(s, s') -> (s, s', s2, p)) <$>
      elements [(s0, s1), (s1, s0)]

-- no interior overlap
newtype CollinearEndsIntersectingSegms = CollinearEndsIntersectingSegms
  {getCollinearEndsIntersectingSegms :: (Segm, Segm, Point)}
  deriving Show
instance Arbitrary CollinearEndsIntersectingSegms where
  arbitrary = do
    s0 <- arbitrary

    Positive f <- arbitrary
    let p = hp s0
    let Just s1 = f2p (p, p .+-> toVec s0 =*~ f)

    (s, s') <- elements $ tupleCombo reverse (s0, s1)

    return $ CollinearEndsIntersectingSegms (s, s', p)

newtype CollinearDisjointSegms = CollinearDisjointSegms
  {getCollinearDisjointSegms :: (Segm, Segm)}
  deriving Show
instance Arbitrary CollinearDisjointSegms where
  arbitrary = do
    s0 <- arbitrary

    NonZero f' <- (arbitrary :: Gen (NonZero Double))
    let f = if f' < 0 then f' else f' + 1
    Positive l' <- (arbitrary :: Gen (Positive Double))
    let l = if f < 0 then (-l') else l'
    let v = toVec s0
    let p = tp s0
    let p0 = p .+-> v =*~ f
    let p1 = p .+-> v =*~ (f + l)
    s1 <- fromJust . f2p <$> elements [(p0, p1), (p1, p0)]

    CollinearDisjointSegms <$> elements [(s0, s1), (s1, s0)]

-- the point lies along side the segment
-- a perpendicular vector to the segment can reach the point while still on the line
newtype PointAlongSegm = PointAlongSegm
  {getPointAlongSegm :: (Segm, Point, Squared Distance)}
  deriving Show
instance Arbitrary PointAlongSegm where
  arbitrary = do
    s <- arbitrary

    let Just v'@(NZVector v0) = toNZV s
    d <- arbitrary
    let v1 = setLength d $ _toUv $ perp v'
    Ratio f <- arbitrary
    let p = tp s .+-> ((v0 =*~ f) =+= v1)

    return $ PointAlongSegm (s, p, sq d)

-- the point lies closer to the segment's endpoint than its interior
newtype PointNearSegmEndPoint = PointNearSegmEndPoint
  {getPointNearSegmEndPoint :: (Segm, Point, Squared Distance)}
  deriving Show
instance Arbitrary PointNearSegmEndPoint where
  arbitrary = do
    s <- arbitrary

    let sv = _toNZV s
    v :: Vector <- suchThat arbitrary (\v -> within90 sv v || v == zero)

    let p = hp s .+-> v

    PointNearSegmEndPoint . (\s' -> (s', p, magSqrd v)) <$>
      elements [s, reverse s]

newtype DisjointSegmPoint = DisjointSegmPoint
  {getDisjointSegmPoint :: (Segm, Point)}
  deriving Show
instance Arbitrary DisjointSegmPoint where
  arbitrary = DisjointSegmPoint <$> oneof
    [ getDisjointCollinearSegmPoint <$> arbitrary
    , t3to2 . getLinePointDisjoint <$> arbitrary
    ]

newtype DisjointCollinearSegmPoint = DisjointCollinearSegmPoint
  {getDisjointCollinearSegmPoint :: (Segm, Point)}
  deriving Show
instance Arbitrary DisjointCollinearSegmPoint where
  arbitrary = do
    s <- arbitrary

    NonZero f <- arbitrary
    let p = tp s .+-> toVec s =*~ (if f > 0 then f + 1 else f)

    return $ DisjointCollinearSegmPoint (s, p)

-- Collinear segment and segment endpoint
-- the segments are disjoint
-- point is the collinear point
newtype DisjointSegmAndSegmEndpoint = DisjointSegmAndSegmEndpoint
  {getDisjointSegmAndSegmEndpoint :: ((Segm, Segm), Point)}
  deriving Show
instance Arbitrary DisjointSegmAndSegmEndpoint where
  arbitrary = do
    (s0, p) <- getDisjointCollinearSegmPoint <$> arbitrary
    s1 <- nS'' p <$>
          ( suchThat arbitrary $
            liftA2 (&&) ((> zero) . magSqrd)
                        (not . parallel s0) .
            (.<-. p) )

    DisjointSegmAndSegmEndpoint . (, p) <$> elements (tupleCombo reverse (s0, s1))

newtype SegmPointCoincide = SegmPointCoincide
  {getSegmPointCoincide :: (Segm, Point)}
  deriving Show
instance Arbitrary SegmPointCoincide where
  arbitrary = do
    s <- arbitrary
    p <- pointOnSegment s
    return $ SegmPointCoincide (s, p)
pointOnSegment :: Segm -> Gen Point
pointOnSegment s = do
  f <- elements [endpointOfSegment, interiorPointOnSegment]
  f s

newtype SegmAndEndPoint = SegmAndEndPoint
  {getSegmAndEndPoint :: (Segm, Point)}
  deriving Show
instance Arbitrary SegmAndEndPoint where
  arbitrary = do
    s <- arbitrary
    p <- endpointOfSegment s
    return $ SegmAndEndPoint (s, p)
endpointOfSegment :: Segm -> Gen Point
endpointOfSegment s = elements $ thps' s

newtype SegmAndInteriorPoint = SegmAndInteriorPoint
  {getSegmAndInteriorPoint :: (Segm, Point)}
  deriving Show
instance Arbitrary SegmAndInteriorPoint where
  arbitrary = do
    s <- arbitrary
    p <- interiorPointOnSegment s
    return $ SegmAndInteriorPoint (s, p)
interiorPointOnSegment :: Segm -> Gen Point
interiorPointOnSegment s = do
  NZRatio f <- arbitrary
  return $ tp s .+-> toVec s =*~ f

newtype NoSharedEndpoint a = NoSharedEndpoint
  {getNoSharedEndpoint :: (a, a)}
  deriving Show
instance ( Arbitrary a
         , TailHeadPoints a )
         => Arbitrary (NoSharedEndpoint a) where
  arbitrary = do
    s0 <- arbitrary
    s1 <- suchThat arbitrary $ not . any (hasEndpoint s0) . thps'
    return $ NoSharedEndpoint (s0, s1)
