{-# LANGUAGE InstanceSigs
           , MultiParamTypeClasses
           , ViewPatterns
           , TypeSynonymInstances
           , FlexibleInstances
           , PartialTypeSignatures
           , UndecidableInstances
           , DataKinds #-}

module Triangle where

import GeometryClasses
  ( Contain(contains, contains', interiorContains)
  )
import Dimension
  ( (.-.)
  , (=+=)
  , (=/~)
  , _toRelative
  , abs
  , fromScalar
  , Rx
  , Ry
  )
import Area (FindArea(area), Area, (=**=))
import Vector
  ( NZVector
  , xp
  , parallel
  , ang
  , setLength
  , _toNZV
  , between
  )
import PLS
  ( Point(Point)
  , (.+->)
  , (.->.)
  , (.<-.)
  , crossIntersect'
  , nS'''
  , Segm
  , thps'
  , Intersect(intersects, intersects', interiorIntersect)
  , Edges(edges)
  , f2p
  )
import LineArbitrary (pointOnEachSide)
import SegmArbitrary (getCollinearIntersectingSegms)

import Utilities (zero, bimap', (<$<))

import Unified
  ( splitAtMay
  , fromFoldable
  , sortOn
  , HeadNE(headNE)
  , empty
  , (<|)
  )
import Unified.Utilities
  ( take2
  , take3
  , mapMaybe
  )
import Unified.Collections.List (List(List))
import Unified.Utilities.List
  ( filterL
  , zipL
  , allCombo
  , allCombo''
  , circlePairs
  , rotationsL
  )
import Unified.AtLeast (AtLeast(AtLeast))
import Unified.Arbitrary (uniqueValues)

import Control.Applicative (liftA2)
import Control.Arrow ((&&&))
import Control.Monad ((<=<))
import Data.Bifunctor (bimap, second)
import Data.Bitraversable (bisequenceA)
import Data.Foldable (Foldable(length, toList, foldMap))
import Data.Function ((&))
import Data.Maybe (fromJust)
import Data.Tuple (swap)
import Data.Tuple.HT (uncurry3, mapThd3)

import Test.QuickCheck
  ( Arbitrary(arbitrary)
  , Gen
  , getPositive
  )
import Unified.QuickCheck
  ( infiniteListOf
  , elements
  , shuffle
  , oneof
  )

import Prelude hiding (abs, splitAt, zip, map)


type Triangle = Triangle_ Point
data Triangle_ a = Triangle a a a deriving Show

nTriangle :: Point -> Point -> Point -> Maybe Triangle
nTriangle p0 p1 p2 =
  if any (uncurry (==)) (allCombo [p0, p1, p2]) ||
     v01 `parallel` v12
  then Nothing
  else Just $ Triangle p0 p1 p2
  where v01 = (p0 .->. p1)
        v12 = (p1 .->. p2)


nTriangle' :: (Point, Point, Point) -> Maybe Triangle
nTriangle' = uncurry3 nTriangle

triSegms :: TriPoints a => Triangle_ a -> AtLeast 3 List Segm
triSegms = AtLeast .
           fmap fromJust .
           fmap f2p .
           circlePairs .
           triPoints

triVectors :: TriPoints a => Triangle_ a -> AtLeast 3 List NZVector
triVectors = fmap _toNZV . triSegms

class TriPoints a where
  triPoints :: Triangle_ a -> AtLeast 3 List Point

instance Foldable Triangle_ where
  foldMap f = mconcat . fmap f . toList
  length = const 3
  toList (Triangle p0 p1 p2) = [p0, p1, p2]

instance TriPoints Point where
  triPoints = AtLeast . fromFoldable

instance HeadNE Triangle_ where
  headNE (Triangle p _ _) = p

instance Contain Triangle Point where
  -- interior and edges
  -- Reference: https://math.stackexchange.com/a/51328
  contains' :: (Triangle, Point) -> Bool
  contains' = all ((>= zero) . uncurry xp) .
              uncurry zipL .
              uncurry (&) .
              bimap (triVectors &&& triPoints)
                    (second . fmap . (.<-.))

  -- interior but not edges
  interiorContains :: Triangle -> Point -> Bool
  interiorContains t p =
    contains t p &&
    (not $ any (flip contains p) (edges t))


instance Edges Triangle_ where
  edges = AtLeast . fmap nS''' . circlePairs . triPoints

instance Intersect Triangle where
  intersects :: Triangle -> Triangle -> Bool
  intersects = curry $ liftA2 (||)
    (any intersects' . uncurry allCombo'' . bimap' edges)
    (_containsOther contains)

  interiorIntersect :: Triangle -> Triangle -> Bool
  interiorIntersect = curry $ liftA2 (||)
    (any crossIntersect' . uncurry allCombo'' . bimap' edges)
    (_containsOther interiorContains)

  -- is not the same as interiorIntersect because polygons do have boundries
  -- ininIntersect = 

_containsOther :: (Triangle -> Point -> Bool) -> (Triangle, Triangle) -> Bool
_containsOther contFunc ts = any (uncurry contFunc . second headNE) [ts, swap ts]

instance FindArea Triangle where
  -- Reference: http://www.mathopenref.com/coordtrianglearea.html
  area :: Triangle -> Area
  area =  abs .
          (=/~ 2) .
          (\(Triangle (Point (ax, ay)) (Point (bx, by)) (Point (cx, cy))) ->
            (_toRelative ax :: Rx) =**= (by .-. cy :: Ry) =+=
            (_toRelative bx :: Rx) =**= (cy .-. ay :: Ry) =+=
            (_toRelative cx :: Rx) =**= (ay .-. by :: Ry) )

-- Arbitrary Helpers --

newtype SharedBoundryNoInterior = SharedBoundryNoInterior
  {getSharedBoundryNoInterior :: (Triangle, Triangle, Either Point Segm)}
  deriving Show
instance Arbitrary SharedBoundryNoInterior where
  arbitrary = SharedBoundryNoInterior <$> oneof
    [ mapThd3 Right . getSharesOneEdgeNoInterior <$> arbitrary
    , mapThd3 Right . getSharesPartialEdgeNoInterior <$> arbitrary
    , mapThd3 Left  . getSharesPointNoInterior <$> arbitrary ]

-- each triangle has the shared segment
-- has no interior overlap only a boundry overlap
newtype SharesOneEdgeNoInterior = SharesOneEdgeNoInterior
  {getSharesOneEdgeNoInterior :: (Triangle, Triangle, Segm)}
  deriving Show
instance Arbitrary SharesOneEdgeNoInterior where
  arbitrary = do
    s <- arbitrary :: Gen Segm
    (p0, p1) <- pointOnEachSide s

    let toT :: Point -> Gen Triangle
        toT = fromJust . (nTriangle' <=< take3) <$<
              shuffle . (<| thps' s)
    t0 <- toT p0
    t1 <- toT p1

    return $ SharesOneEdgeNoInterior (t0, t1, s)

newtype SharesPartialEdgeNoInterior = SharesPartialEdgeNoInterior
  {getSharesPartialEdgeNoInterior :: (Triangle, Triangle, Segm)}
  deriving Show
instance Arbitrary SharesPartialEdgeNoInterior where
  arbitrary = do
    (s0, s1, s2) <- getCollinearIntersectingSegms <$> arbitrary

    (p0, p1) <- pointOnEachSide s0

    (t0, t1) <- bimap' (fromJust . (nTriangle' <=< take3)) <$>
                ( bisequenceA . bimap' (shuffle . uncurry (<|) . second thps') =<<
                  elements [((p0, s0), (p1, s1)), ((p0, s1), (p1, s0))] )

    return $ SharesPartialEdgeNoInterior (t0, t1, s2)

newtype SharesPointNoInterior = SharesPointNoInterior
  {getSharesPointNoInterior :: (Triangle, Triangle, Point)}
  deriving Show
instance Arbitrary SharesPointNoInterior where
  arbitrary = do
    p <- arbitrary

    let notBetween :: ((NZVector, NZVector), (NZVector, NZVector)) -> Bool
        notBetween = not . uncurry (uncurry between) . second fst
    (t0, t1) <-
      return . bimap' (fromJust . (nTriangle' <=< take3)) =<<
      bisequenceA . bimap' (shuffle . (\(p0, p1) -> List [p0, p1, p])) =<<
      return . bimap' (bimap' ((.+->) p :: NZVector -> Point)) =<<
      elements .
      filterL (liftA2 (&&) notBetween (notBetween . swap) :: ((NZVector, NZVector), (NZVector, NZVector)) -> Bool) .
      mapMaybe (bisequenceA . bimap' take2 <=< splitAtMay 2 :: List NZVector -> Maybe ((NZVector, NZVector), (NZVector, NZVector))) .
      rotationsL .
      sortOn ang .
      fmap (_toNZV . uncurry setLength) =<<
      liftA2 (zipL :: List a -> List b -> List (a, b))
        (infiniteListOf (fromScalar . getPositive <$> arbitrary))
        (uniqueValues 4 empty)

    return $ SharesPointNoInterior (t0, t1, p)

-- geometry-2d/reference/Shape\ Distributions.pdf
-- genPointInTriangle :: Triangle -> Gen Point

-- newtype SharedInterior = SharedInterior
--   {getSharedInterior :: (Triangle, Triangle)}
--   deriving Show

-- newtype InsideTriangle = InsideTriangle
--   {getInsideTriangle :: (Triangle, Triangle)}
--   deriving Show
