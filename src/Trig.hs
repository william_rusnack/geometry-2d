{-# LANGUAGE MultiParamTypeClasses
           , InstanceSigs
           , FlexibleContexts
           , TypeSynonymInstances
           , FlexibleInstances #-}


module Trig where

import Dimension
  ( Rx, Ry
  , fromScalar, ftScalar, toScalar
  )
import Angle (Aa, Ra)

import Normalized (Normalized(getNormalized))


import Prelude ((.), Double, ($))
import qualified Prelude as P


class Sin a b where
  sin :: a -> b

class Cos a b where
  cos :: a -> b

-- input should probably be normalized
asin :: Double -> Ra
asin = fromScalar . P.asin

-- input should probably be normalized
acos :: Double -> Ra
acos = fromScalar . P.acos

instance Sin Aa Ry where
  sin :: Aa -> Ry
  sin = ftScalar P.sin

instance Sin Ra Double where
  sin :: Ra -> Double
  sin = P.sin . toScalar

instance Sin a Double => Sin (Normalized a) Double where
  sin = sin . getNormalized

instance Cos Aa Rx where
  cos :: Aa -> Rx
  cos = ftScalar P.cos

instance Cos Ra Double where
  cos :: Ra -> Double
  cos = P.cos . toScalar

instance Cos a Double => Cos (Normalized a) Double where
  cos = cos . getNormalized


atan2 :: Ry -> Rx -> Aa
-- atan2 (Ry (Relative y)) (Rx (Relative x)) = nAa (P.atan2 y x)
atan2 x y = fromScalar $ P.atan2 (toScalar x) (toScalar y)
