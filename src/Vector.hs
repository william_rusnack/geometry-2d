{-# LANGUAGE GeneralizedNewtypeDeriving
           , MultiParamTypeClasses
           , InstanceSigs
           , ViewPatterns
           , FlexibleInstances
           , FlexibleContexts
           , UndecidableInstances
           , ScopedTypeVariables
           , PartialTypeSignatures
           , StandaloneDeriving
           , TypeApplications #-}

module Vector where

import IsClose (IsClose(isCloseNZ, isCloseNZ', isCloseAR, isCloseA, isCloseRO))
import Dimension
import Angle
import Area ((=**=))
import Trig (Sin(sin), Cos(cos), acos, atan2)

import Utilities (Zero(zero), bimap', (<$<), maybeBool)
import Ratio (NZRatio(NZRatio))
import Normalized (NormalizedLU)

import Unified.Collections.List (getList)
import Unified.Utilities.List (allCombo'')

import Control.Arrow ((&&&))
import Control.Monad (mfilter)
import Data.Bifunctor (bimap)
import Data.Bitraversable (bisequence)
import Data.Maybe (fromJust, mapMaybe)
import Data.Tuple (swap)

import qualified Prelude as P
import Prelude hiding (atan2, sqrt, acos, cos, sin, negate, pi, reverse, isNaN)

import Test.QuickCheck
  ( Arbitrary(arbitrary, shrink)
  , Gen
  , NonZero(NonZero)
  , Positive(Positive)
  , suchThat
  )
import Unified.QuickCheck
  ( elements
  , oneof
  )


-- can have a zero vector
newtype Vector = Vector (Rx, Ry) deriving (Show, Eq, Arbitrary)

-- non-zero vector
-- ScalarOps cannot be defined for NZVector because it could be multiplied by zero
newtype NZVector = NZVector {getNZVector :: Vector}
  deriving ( Show
           , Eq
           , Magnitude
           , RelativeAddition
           , IsClose
           , ToVec
           , ScalarDivison )

newtype Unit = Unit {getUnit :: NZVector}
  deriving ( Show
           , Eq
           , IsClose
           , RelativeAddition
           , ToVec
           , NeverZero )

within90 :: ( GetDimension Relative a
            , ToVec a, ToVec b
            , ToUv a, ToUv b
            , FromCoords a ) =>
            a -> b -> Bool
within90 (perp -> pv) v = pv `xp` v < zero &&
                      not (pv `parallel` v)

-- within90 v0 v1 = pv `xp` v1 < zero &&
--                  not (pv `parallel` v1)
--   where pv = perp v0

-- vector to a 1d number line along refVec based on magSqrd of v
to1Dsq :: forall a b.
          ( GetDimension Relative b
          , ToVec a, ToVec b
          , Magnitude b
          , ToUv a, ToUv b
          , FromCoords b
          , NeverZero a ) =>
          a -> b -> Squared (Absolute ())
to1Dsq refVec v =
  _toAbsolute .
  (fConv :: Distance -> Relative ()) <$>
  ( (if v `within90` refVec then id else negate) $
    magSqrd
    v )

-- to1Dsq refVec v = _toAbsolute rel
--   where Squared (Distance rel) = if v `within90` refVec
--                                then l
--                                else negate l
--         l = magSqrd v

-- class CrossProduct a b where
xp :: (ToVec a, ToVec b) => a -> b -> Rz
xp (getXY . toVec -> (x0, y0)) (getXY . toVec -> (x1, y1)) =
  Relative $ toScalar $ (x0 =**= y1) =-= (x1 =**= y0)

-- xp :: (ToVec a, ToVec b) => a -> b -> Rz
-- xp v0 v1 = Rz z
--   where Area (Squared z) = (x0 =**= y1) =-= (x1 =**= y0)
--         (x0, y0) = getXY $ toVec v0
--         (x1, y1) = getXY $ toVec v1

-- xp = curry $
--   uncurry (=-=) .
--   bimap' (uncurry (=**=)) .
--   ((…) :: ((a,b),(c,d)) -> ((a,d),(c,b))) .
--   bimap' getXY .
--   bimap toVec toVec


-- class Collinear a b where
collinearV :: (ToUv a, ToUv b) => a -> b -> Bool
collinearV = curry collinearV'
collinearV' :: (ToUv a, ToUv b) => (a, b) -> Bool
collinearV' = maybeBool .
              ( isCloseNZ' <$<
                bisequence .
                bimap toUv toUv )


-- creates a ccw Perpendicular vector
perp :: forall a.
        ( GetDimension Relative a
        , FromCoords a )
     => a -> a
perp =
  uncurry _vec' .
  bimap (fConv . negate)
        fConv .
  swap .
  (getXY @Relative @a)


perpendicular :: (ToUv a, ToUv b) => a -> b -> Bool
perpendicular (toUv -> Just u0) (toUv -> Just u1) = u0 `dp` u1 `isCloseNZ` zero
perpendicular _ _ = False

-- dot product
dp :: forall a b.
      ( GetDimension Relative a
      , GetDimension Relative b )
   => a -> b -> Squared (Relative ())
dp = curry $
  (\((x0, y0), (x1, y1)) -> (x0 =*= x1) ^+^ (y0 =*= y1)) .
  bimap (getXY @Relative @a)
        (getXY @Relative @b)

ang :: (NeverZero a, GetDimension Relative a) => a -> Aa
ang = uncurry (flip atan2) . getXY

vecAngle :: ( ToUv a, ToUv b
            , Magnitude a, Magnitude b
            , GetDimension Relative a, GetDimension Relative b
            , NeverZero a, NeverZero b)
         => a -> b -> NormalizedLU (HalfCircle Ra)
vecAngle (_toUv -> u0) (_toUv -> u1) =
  normalize $ HalfCircle $
    ( case acos $ toScalar $ u0 `dp` u1 of x | isNaN x -> zero
                                           x -> x )

parallel :: (ToUv a, ToUv b) => a -> b -> Bool
parallel (toUv -> Just u0) (toUv -> Just u1) =
  isCloseNZ u0 u1 || isCloseNZ u0 (negate u1)
parallel _ _ = False

parallel' :: (ToUv a, ToUv b) => (a, b) -> Bool
parallel' = uncurry parallel

-- vector that connects the heads of 2 vectors
diff :: (GetDimension Relative a, GetDimension Relative b) => a -> b -> Vector
diff v0 v1 = Vector (x0 =-= x1, y0 =-= y1)
  where (x0, y0) = getXY v0
        (x1, y1) = getXY v1


rotate :: (GetDimension Relative a, FromCoords a) => Ra -> a -> a
rotate (cos &&& sin -> (c, s)) (getXY -> (x, y)) =
  _vec' (x       =*~ c =-= fConv y =*~ s)
        (fConv x =*~ s =+= y       =*~ c)

reverse :: (GetDimension Relative a, FromCoords a) => a -> a
reverse v = _vec' (negate x) (negate y)
  where (x, y) = getXY v

-- true if v2 is between the non reflex angle made by v0 and v1
-- not sure how to handle the 180deg case, so defaults to floating point error

between :: ( ToNZV a, ToNZV b, ToNZV c
           , GetDimension Relative a, GetDimension Relative b, GetDimension Relative c )
        => a -> b -> c -> Bool
between (toNZV -> Just v0) (toNZV -> Just v1) (toNZV -> Just v2) =
  v0' `xp` v2  >= zero &&
  v2  `xp` v1' >= zero
  where (v0', v1') = if v0 `xp` v1 > zero
                     then (v0, v1)
                     else (v1, v0)
between _ _ _ = False

-- b0 and b1 are the bounding vectors
-- clock from b0
-- boundry inclusive
between' :: (ToNZV a, ToNZV b, ToNZV c)
         => a -> b -> Clock -> c -> Bool
between' (toNZV -> Just a) (toNZV -> Just b) d (toNZV -> Just v) = do
  let bs@(b0, b1) = case d of CCW -> (a, b)
                              CW  -> (b, a)
  let (x0, x1) = bimap' (`xp` v) bs

  if x0 == zero || x1 == zero
    then True
    else if b0 `xp` b1 >= zero
      then x0 > zero && x1 < zero
      else not (x0 < zero && x1 > zero)
between' _ _ _ _ = False

class FromCoords a where
  vec :: Double -> Double -> Maybe a
  vec x y = vec' (fromScalar x) (fromScalar y)

  vec' :: Rx -> Ry -> Maybe a

  _vec :: Double -> Double -> a
  _vec x y = fromJust $ vec x y

  _vec' :: Rx -> Ry -> a
  _vec' x y = fromJust $ vec' x y

class Magnitude a where
  magSqrd :: a -> Squared Distance

  mag :: a -> Distance
  mag = sqrt . magSqrd

  magScalar :: a -> Double
  magScalar = getRelative . mag


data Ward = Leftward | Rightward | Forward | Backward | Stopped
            deriving (Show, Eq)
toward :: ( ToVec a, ToVec b
          , ToUv a, ToUv b ) => a -> b -> Ward
toward v0 v1 | v0 `collinearV` v1 = Forward
             | v0 `parallel` v1   = Backward
             | vxp > zero         = Leftward
             | vxp < zero         = Rightward
             | otherwise          = Stopped
  where vxp = v0 `xp` v1

class GetDimension t a where
  getX :: a -> t X
  getX = fst . getXY

  getY :: a -> t Y
  getY = snd . getXY

  getXY :: a -> (t X, t Y)
  getXY = getX &&& getY

class ToVec a where
  toVec :: a -> Vector

class ToNZV a where
  toNZV :: a -> Maybe NZVector

  _toNZV :: a -> NZVector
  _toNZV = fromJust . toNZV

class ToUv a where
  toUv :: a -> Maybe Unit

  _toUv :: a -> Unit
  _toUv = fromJust . toUv

class FromVec a where
  fromVec :: Vector -> Maybe a

class FromNZV a where
  fromNZV :: NZVector -> a

class FromUv a where
  fromUv :: Unit -> a

class NeverZero a

-- vector --

instance Magnitude Vector where
  magSqrd :: Vector -> Squared Distance
  -- magSqrd (Vector (x, y)) = Squared $ Distance v
  --   where Squared v = sq x ^+^ sq y
  magSqrd = fConv . uncurry (^+^) . bimap sq sq . (getXY @Relative @Vector)

instance IsClose Vector where
  isCloseAR :: Double -> Double -> Vector -> Vector -> Bool
  isCloseAR a r (Vector (x, y)) (Vector (x', y')) = isCloseAR a r x x' &&
                                                  isCloseAR a r y y'

  isCloseA :: Double -> Vector -> Vector -> Bool
  isCloseA a (Vector (x, y)) (Vector (x', y')) = isCloseA a x x' &&
                                                 isCloseA a y y'

  isCloseRO :: Double -> Vector -> Vector -> Bool
  isCloseRO r (Vector (x, y)) (Vector (x', y')) = isCloseRO r x x' &&
                                                  isCloseRO r y y'

instance ScalarDivison Vector where
  (=/~) :: Vector -> Double -> Vector
  (=/~) (Vector (x, y)) s = Vector (x =/~ s, y =/~ s)

instance ScalarMultiplicaton Vector where
  (=*~) :: Vector -> Double -> Vector
  (=*~) (Vector (x, y)) s = Vector (x =*~ s, y =*~ s)

instance GetDimension Relative Vector where
  getX (Vector (x, _)) = x
  getY (Vector (_, y)) = y

instance RelativeAddition Vector where
  (=+=) (Vector (x0, y0)) (Vector (x1, y1)) = Vector (x0 =+= x1, y0 =+= y1)
  negate (Vector (x, y)) = Vector (negate x, negate y)

instance Zero Vector where
  zero = _vec 0 0

instance FromCoords Vector where
  vec' x y = Just $ Vector (x, y)
  _vec' x y = Vector (x, y)

instance ToVec Vector where
  toVec = id

instance ToVec (Rx, Ry) where
  toVec = Vector

instance {-# OVERLAPPABLE #-} ToVec a => ToNZV a where
  toNZV = NZVector <$< mfilter (/= zero) . Just . toVec

instance {-# OVERLAPPABLE #-} ToVec a => ToUv a where
  toUv = Unit . (\nzv -> nzv =/~ magScalar nzv) <$< toNZV

instance FromVec Vector where
  fromVec = Just

instance Semigroup Vector where
  (<>) = (=+=)

--- non-zero vector --

instance ToUv NZVector where
  toUv = Just . _toUv

  -- _toUv (NZVector v) = Unit $ NZVector (v =/~ m)
  --   where Distance (Relative m) = mag v
  _toUv = Unit . NZVector . (\v -> v =/~ toScalar (mag v)) . getNZVector

instance ToNZV NZVector where
  toNZV = Just
  _toNZV = id

instance FromCoords NZVector where
  vec' :: Rx -> Ry -> Maybe NZVector
  vec' x y | x == zero && y == zero = Nothing
           | otherwise = NZVector <$> vec' x y

instance NeverZero NZVector

instance FromVec NZVector where
  fromVec = toNZV

instance GetDimension Relative NZVector where
  getX = getX . toVec
  getY = getY . toVec


-- unit vector --

setLength :: Distance -> Unit -> Vector
setLength (Relative l) (Unit (NZVector v)) = l ~*= v

instance FromCoords Unit where
  vec' :: Rx -> Ry -> Maybe Unit
  vec' x y = toUv =<< (vec' x y :: Maybe NZVector)

instance Magnitude Unit where
  magSqrd = const $ fromScalar 1
  mag = const $ fromScalar 1

instance ToUv Unit where
  toUv = Just
  _toUv = id

instance ToUv Aa where
  toUv = Just . _toUv
  _toUv a = _vec' (cos a) (sin a)

instance ToNZV Unit where
  toNZV (Unit v) = Just v
  _toNZV (Unit v) = v

instance GetDimension Relative Unit where
  getX = getX . toVec
  getY = getY . toVec


-- Arbitrary Helpers --

instance Arbitrary NZVector where
  arbitrary = do
    v <- suchThat arbitrary (\v -> v /= zero)
    return $ NZVector v

  -- shrink (getXY -> (x, y)) = mapMaybe toNZV $ getList $ allCombo'' (shrink x) (shrink y)
  shrink = mapMaybe toNZV .
           getList .
           uncurry allCombo'' .
           bimap shrink shrink .
           (getXY @Relative @NZVector)

instance Arbitrary Unit where
  arbitrary = (fromJust . toUv) <$> (arbitrary :: Gen NZVector)

newtype ParallelVectors a =
  ParallelVectors {getParallelVectors :: (a, a)}
  deriving Show
instance ( Arbitrary a
         , FromVec a
         , ToVec a
         , Magnitude a ) => Arbitrary (ParallelVectors a) where
  arbitrary = do
    v0 <- suchThat arbitrary ((> zero) . magSqrd)
    NonZero f <- arbitrary

    let Just v1 = fromVec $ (=*~ f) $ toVec v0

    return $ ParallelVectors (v0, v1)

newtype NonParallelVectors a =
  NonParallelVectors {getNonParallelVectors :: (a, a)}
  deriving Show
instance ( GetDimension Relative a
         , Arbitrary a
         , ToVec a
         , ToUv a
         , FromVec a
         , FromCoords a
         , Magnitude a ) => Arbitrary (NonParallelVectors a) where
  arbitrary = oneof $ map (fmap NonParallelVectors) [
      getNonParallelRandomVectors <$> arbitrary
    , getPerpendicularVectors <$> arbitrary
    ]

newtype NonParallelRandomVectors a =
  NonParallelRandomVectors {getNonParallelRandomVectors :: (a, a)}
  deriving Show
instance (Arbitrary a, ToUv a) => Arbitrary (NonParallelRandomVectors a) where
  arbitrary = do
    v0 <- arbitrary
    v1 <- case toUv v0 of
      Nothing -> do
        v1 <- arbitrary
        return v1
      Just u0 -> do
        let u0' = negate u0
        let notParallel (Just v) = not $ any (isCloseNZ v) [u0, u0']
            notParallel Nothing = True
        v1 <- suchThat arbitrary (notParallel . toUv)
        return v1

    return $ NonParallelRandomVectors (v0, v1)

newtype PerpendicularVectors a =
  PerpendicularVectors {getPerpendicularVectors :: (a, a)}
  deriving Show
instance ( GetDimension Relative a
         , ToVec a
         , FromVec a
         , FromCoords a
         , Arbitrary a
         , Magnitude a ) => Arbitrary (PerpendicularVectors a) where
  arbitrary = do
    v0 <- suchThat arbitrary ((> zero) . magSqrd)

    a <- elements $ map (=/~ 2) [pi, piN]
    NonZero f <- arbitrary
    let Just v1 = fromVec $ (=*~ f) $ toVec $ rotate a v0

    return $ PerpendicularVectors (v0, v1)


newtype NonPerpendicularVectors a =
  NonPerpendicularVectors {getNonPerpendicularVectors :: (a, a)}
  deriving Show
instance ( GetDimension Relative a
         , Magnitude a
         , Arbitrary a
         , FromVec a
         , ToVec a
         ) => Arbitrary (NonPerpendicularVectors a) where
  arbitrary = oneof $ map (fmap NonPerpendicularVectors) [
      getNonPerpendicularRandomVectors <$> arbitrary
    , getParallelVectors <$> arbitrary
    ]

newtype NonPerpendicularRandomVectors a =
  NonPerpendicularRandomVectors {getNonPerpendicularRandomVectors :: (a, a)}
  deriving Show
instance ( Magnitude a
         , ToVec a
         , Arbitrary a ) => Arbitrary (NonPerpendicularRandomVectors a) where
  arbitrary = do
    v0 <- arbitrary
    v1 <- if magSqrd v0 == zero
          then arbitrary
          else suchThat arbitrary (\v -> magSqrd v == zero || v `xp` v0 /= zero)

    return $ NonPerpendicularRandomVectors (v0, v1)

newtype CollinearVectors a =
  CollinearVectors {getCollinearVectors :: (a, a)}
  deriving Show
instance ( Arbitrary a
         , Magnitude a
         , FromVec a
         , ToVec a ) => Arbitrary (CollinearVectors a) where
  arbitrary = do
    v0 <- suchThat arbitrary ((> zero) . magSqrd)

    Positive f <- arbitrary
    let Just v1 = fromVec $ (=*~ f) $ toVec v0

    return $ CollinearVectors (v0, v1)

newtype NonCollinearVectors a =
  NonCollinearVectors {getNonCollinearVectors :: (a, a)}
  deriving Show
instance ( GetDimension Relative a
         , ToVec a
         , FromVec a
         , Arbitrary a
         , Magnitude a
         , FromCoords a ) => Arbitrary (NonCollinearVectors a) where
  arbitrary = oneof $ map (fmap NonCollinearVectors) [
      getReversedVectors <$> arbitrary
    , getPerpendicularVectors <$> arbitrary
    ]

newtype NonCollinearRandomVectors a =
  NonCollinearRandomVectors {getNonCollinearRandomVectors :: (a, a)}
  deriving Show
instance ( Arbitrary a
         , ToUv a ) => Arbitrary (NonCollinearRandomVectors a) where
  arbitrary = do
    v0 <- arbitrary

    v1 <- case toUv v0 of
          Nothing -> arbitrary
          Just u0 -> do
            let notCollinear (Just u1) = not $ u0 `isCloseNZ` u1
                notCollinear Nothing = True
            suchThat arbitrary (notCollinear . toUv)

    return $ NonCollinearRandomVectors (v0, v1)

newtype ReversedVectors a =
  ReversedVectors {getReversedVectors :: (a, a)}
  deriving Show
instance ( GetDimension Relative a
         , Magnitude a
         , Arbitrary a
         , FromCoords a) => Arbitrary (ReversedVectors a) where
  arbitrary = do
    v <- suchThat arbitrary ((> zero) . magSqrd)
    return $ ReversedVectors (v, reverse v)

newtype Within90DegVectors a =
  Within90DegVectors {getWithin90DegVectors :: (a, a)}
  deriving Show
instance ( Arbitrary a
         , FromVec a
         , ToVec a
         , GetDimension Relative a
         , FromCoords a
         , Magnitude a ) => Arbitrary (Within90DegVectors a) where
  arbitrary = do
    v0 <- suchThat arbitrary ((> zero) . magSqrd)

    NZRatio r <- arbitrary
    let a = pi =*~ (r - 1/2)
    Positive l <- arbitrary

    let Just v1 = fromVec $ (toVec $ rotate a v0) =*~ l

    return $ Within90DegVectors (v0, v1)

newtype Outside90DegVectors a =
  Outside90DegVectors {getOutside90DegVectors :: (a, a)}
  deriving Show
instance ( Arbitrary a
         , FromVec a
         , ToVec a
         , GetDimension Relative a
         , FromCoords a
         , Magnitude a ) => Arbitrary (Outside90DegVectors a) where
  arbitrary = do
    v0 <- suchThat arbitrary ((> zero) . magSqrd)

    NZRatio r <- arbitrary
    let a = pi =*~ r =+= pi =/~ 2
    Positive l <- arbitrary

    let Just v1 = fromVec $ (toVec $ rotate a v0) =*~ l

    return $ Outside90DegVectors (v0, v1)
