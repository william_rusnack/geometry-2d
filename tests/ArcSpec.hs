{-# LANGUAGE FlexibleContexts
           , MultiWayIf
           , ViewPatterns #-}

module ArcSpec where

import Arc
  ( ArcEsT(ArcEsT)
  , ArcOps(radius, angle, chord, chordVec, center, tangent)
  , nArcEsT
  , ArcRadiusAngleCenter(ArcRadiusAngleCenter, getArcRadiusAngleCenter)
  )

import Angle (pi, twoPi)
import Vector
  ( parallel
  , perpendicular
  , within90
  , _toUv
  , _toNZV
  , between
  , reverse
  , collinearV
  )
import PLS
  ( (.->.)
  , (..-)
  , Segm
  , thps
  , tp
  , (|--|)
  )

import Utilities (zero, bimap')
import IsClose (IsClose(isClose, isClose', isCloseAR), relErr)
import Normalized (getNormalizedLU)

import Control.Applicative (liftA2)
import Data.Bifunctor (bimap, first)
import Data.Maybe (fromJust, isJust, isNothing)
import Test.Hspec hiding (parallel)
import Test.QuickCheck

import Prelude hiding (pi, reverse)

main :: IO ()
main = hspec spec


spec :: Spec
spec = do
  describe "ArcOps ArcEsT" $ do
    it "radius" $ do
      let test :: ArcRadiusAngleCenter -> Bool
          test (ArcRadiusAngleCenter (a, r, _, _)) =
            isCloseAR 1e-5 relErr (radius a) r
      property test

    it "angle" $ do
      -- let test :: ArcRadiusAngleCenter -> Bool
      --     test (ArcRadiusAngleCenter (a, _, a', _)) = angle a `isClose` a'
      property $ isClose' .
                 bimap' getNormalizedLU .
                 first angle .
                 (\(x, _, y, _) -> (x, y)) .
                 getArcRadiusAngleCenter

    it "chord" $ do
      let test :: ArcEsT -> Bool
          test a = (fromJust $ uncurry (..-) $ thps a) == chord a
      property test

    it "chordVec" $ do
      let test :: ArcEsT -> Bool
          test a = _toNZV (uncurry (.->.) $ thps a) == chordVec a
      property test

    it "center" $ do
      let test :: ArcRadiusAngleCenter -> Bool
          test (ArcRadiusAngleCenter (a, _, _, c)) =
            isCloseAR 1e-5 relErr (center a) c
      property test

    it "tangent" $ do
      let test :: ArcRadiusAngleCenter -> Bool
          test (ArcRadiusAngleCenter (a@(ArcEsT(_, _, u)), _, _, _)) =
            tangent a == u
      property test

  describe "nArcEsT" $ do
    it "good arc" $ do
      let test :: ArcRadiusAngleCenter -> Bool
          test (ArcRadiusAngleCenter ((ArcEsT (p0, p1, u)), _, _, _)) =
            isJust $ nArcEsT p0 p1 u
      property test

    it "Nothing from same start-point and endpoint" $ do
      property (\p u -> nArcEsT p p u == Nothing)

    it "Nothing from collinear start tangent" $ do
      let test :: Segm -> Bool
          test s = isNothing $ uncurry nArcEsT (thps s) (_toUv s)
      property test

    it "Nothing from parallel start tangent" $ do
      let test :: Segm -> Bool
          test s = isNothing $ uncurry nArcEsT (thps s) (reverse $ _toUv s)
      property test

  describe "Arbitrary Helpers" $ do
    describe "ArcEsT" $ do
      it "different arc points" $ do
        let test (ArcEsT (p0, p1, _)) = p0 /= p1
        property test

      it "no infinite arcs" $ do
        let test :: ArcEsT -> Bool
            test a = not $ chordVec a `parallel` tangent a
        property test

    describe "ArcRadiusAngleCenter" $ do
      it "different arc points" $ do
        let test :: ArcRadiusAngleCenter -> Bool
            test (ArcRadiusAngleCenter (ArcEsT (p0, p1, _), _, _, _)) = p0 /= p1
        property test

      it "no infinite arcs" $ do
        let test :: ArcRadiusAngleCenter -> Bool
            test (ArcRadiusAngleCenter (a, _, _, _)) =
              not (chordVec a `parallel` tangent a)
        property test

      it "radius > 0" $ do
        let test :: ArcRadiusAngleCenter -> Bool
            test (ArcRadiusAngleCenter (_, r, _, _)) = r > zero
        property test

      it "tangent is perpendicular to tail point to center" $ do
        let test :: ArcRadiusAngleCenter -> Bool
            test (ArcRadiusAngleCenter (a, _, _, c)) =
              tangent a `perpendicular` (tp a .->. c)
        property test

      describe "angle" $ do
        it "angle (0, 2*pi)" $ do
          property $ liftA2 (&&) (> zero) (< twoPi) .
                     getNormalizedLU .
                     (\(_, _, a, _) -> a) .
                     getArcRadiusAngleCenter

        it "angle correct part of 2*pi" $ do
          property $ uncurry ($) .
                     bimap (\a -> if (chordVec a) `within90` tangent a
                                    then liftA2 (&&) (> zero) (< pi)
                                    else liftA2 (&&) (>= pi) (< twoPi) )
                           getNormalizedLU .
                     (\(x, _, y, _) -> (x, y)) .
                     getArcRadiusAngleCenter

      describe "center" $ do
        it "points equidistant" $ do
          let test :: ArcRadiusAngleCenter -> Bool
              test (ArcRadiusAngleCenter (ArcEsT (p0, p1, _), _, _, c)) =
                (c |--| p0) `isClose` (c |--| p1)
          property test

        it "correct with respect to angle" $ do
          -- let test :: ArcRadiusAngleCenter -> Bool
          --     test (ArcRadiusAngleCenter (a, _, a', c))
          --       | a' < pi    = between cv t chV
          --       | a' == pi   = cv `collinearV` chV
          --       | a' < twoPi = between chV t cv
          --       | otherwise  = False
          --       where chV = chordVec a
          --             cv = tp a .->. c
          --             t = tangent a
          property $ (\(a, _, getNormalizedLU -> a', c) -> let
                      chV = chordVec a
                      cv = tp a .->. c
                      t = tangent a
                      in if | a' < pi    -> between cv t chV
                            | a' == pi   -> cv `collinearV` chV
                            | a' < twoPi -> between chV t cv
                            | otherwise  -> False ) .
                     getArcRadiusAngleCenter

--   describe "ApproxAsPolyline" $ do
--     it "single line" $ do
--       let sp = pt 2 0
--       let ep = pt 0 2
--       let a = ArcEsT (sp, ep, nUv 0 1)
--       approxAsPolyline 1 a `shouldBe` Polyline [sp, ep]

--     it "double line" $ do
--       let sp = pt 1 0
--       let ep = pt 0 1
--       let c = cos (pi / 4)
--       let ip = pt c c
--       let a = ArcEsT (sp, ep, nUv 0 1)
--       approxAsPolyline 0.2 a `isClose'` Polyline [sp, ip, ep]

--     it "same direction" $ do
--       let t = 1.7958962368722062
--       let a = ArcEsT $ (pt 0.0 55.0, pt 2.728003257124197 0.0, nUv (-0.43880688741567997) (-0.8985813906133171))
--       let Polyline ps = approxAsPolyline t a
--       let [p0, p1] = take 2 ps
--       let cp = center a
--       let sign (p, p') = (cp .->. p) `xp` (cp .->. p') > 0
--       all ((== sign (p0, p1)) . sign) (toPairs ps) `shouldBe` True

--     describe "property" $ do
--       it "exact endpoints" $ do
--         let test t a@(ArcEsT (sp, ep, _)) = t == 0 ||
--                                             thps pl == (sp, ep)
--               where pl = approxAsPolyline (abs t) a
--         property test

--       it "within tolerance" $ do
--         let test :: Length -> ArcEsT -> Bool
--             test t' a = t' == 0 ||
--                         all ((<= t) . abs . ((-) r) . (|-| cp)) ps
--               where t = abs t'
--                     cp = center a
--                     r = radius a
--                     Polyline ps = approxAsPolyline t a
--         property test

--       it "same direction" $ do
--         let test :: Length -> ArcEsT -> Bool
--             test t a = t == 0 ||
--                        all ((== sign (p0, p1)) . sign) (toPairs ps)
--               where Polyline ps = approxAsPolyline (abs t) a
--                     cp = center a
--                     [p0, p1] = take 2 ps

--                     sign (p, p') = (cp .->. p) `xp` (cp .->. p') > 0
--         property test
