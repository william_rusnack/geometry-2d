{-# LANGUAGE LambdaCase
           , ScopedTypeVariables #-}

module DSegmSpec where

import DSegm
  ( DSegm(DSegm)
  , NumeralPrefix(Uni, Bi, Pre, Pst, PPB)
  , nDS'', nDS'''
  , getInteriorIntersectsDSegm
  )

import PLS
  ( pt
  , Segm
  , TailHeadPoints(tp, hp)
  , Intersect(intersects, interiorIntersect, interiorIntersect')
  , reverse
  )

import Utilities.Arbitrary (NonEqual(NonEqual))

import Unified.Utilities.List (allCombo)

import Control.Applicative (liftA2)
import Data.Foldable (traverse_, for_)
import Test.Hspec
import Test.QuickCheck

import Prelude hiding (reverse)


main :: IO ()
main = hspec spec

nps :: [NumeralPrefix]
nps = [Uni, Bi, Pre, Pst, PPB]

spec :: Spec
spec = do

  describe "intersects DSegm" $ do
    let test cases results (np0, np1) = describe (show np0 ++ " " ++ show np1) $ do
          let testSpecific ((ps0, ps1, testName), result) = it testName $ do
                length cases `shouldBe` length results

                nDS''' np0 ps0 `intersects` nDS''' np1 ps1 `shouldBe` result

          traverse_ testSpecific (zip cases results)

    -- Reference: ../imgs/numeric-prefix-intersections.png
    -- True False Lists zip with casesSelect
    let npCombosNResults = [
            ((Pre, Pst), [True, False, True, True, True])
          , ((Pst, Pre), [True, True, False, True, True])
          , ((Pre, PPB), [True, False, True, True, True])
          , ((PPB, Pre), [True, True, False, True, True])
          , ((Pst, PPB), [True, True, False, True, True])
          , ((PPB, Pst), [True, False, True, True, True])

          , ((Bi, Pre), [True, True, True, True, False])
          , ((Pre, Bi), [True, True, True, True, False])
          , ((Bi, Pst), [True, True, True, False, True])
          , ((Pst, Bi), [True, True, True, False, True])

          , ((Bi, PPB), [True, True, True, False, False])
          , ((PPB, Bi), [True, True, True, False, False])

          -- contains Uni
          , ((Uni, Bi), [True, True, True, True, True])
          , ((Bi, Uni), [True, True, True, True, True])
          , ((Uni, Pre), [True, True, True, True, True])
          , ((Pre, Uni), [True, True, True, True, True])
          , ((Uni, Pst), [True, True, True, True, True])
          , ((Pst, Uni), [True, True, True, True, True])
          , ((Uni, PPB), [True, True, True, True, True])
          , ((Uni, Uni), [True, True, True, True, True])
          , ((PPB, Uni), [True, True, True, True, True])

          , ((Bi, Bi), [False, False, False, False, False])
          , ((Pre, Pre), [True, True, True, True, True])
          , ((Pst, Pst), [True, True, True, True, True])
          , ((PPB, PPB), [True, True, True, True, True])
          ]

    let casesTrue = [ ( (pt 0 0, pt 1 1)
                      , (pt 0 0, pt 1 1)
                      , "same line" )
                    , ( (pt 0 0, pt 1 1)
                      , (pt 0 1, pt 1 0)
                      , "crosses" )
                    , ( (pt 0 0, pt 2 0)
                      , (pt 1 0, pt 1 1)
                      , "center left" )
                    , ( (pt 0 0, pt 2 0)
                      , (pt 1 1, pt 1 0)
                      , "center right" )
                    , ( (pt 1 0, pt 1 1)
                      , (pt 0 0, pt 2 0)
                      , "left center" )
                    , ( (pt 1 1, pt 1 0)
                      , (pt 0 0, pt 2 0)
                      , "right center" )
                    ]
    traverse_ (test casesTrue (map (const True) casesTrue))
       (map fst npCombosNResults)

    let casesFalse = [ ( (pt 0 0, pt 1 0)
                       , (pt 0 1, pt 1 1)
                       , "disjoint" )
                     ]
    traverse_ (test casesFalse (map (const False) casesFalse))
       (map fst npCombosNResults)

    let casesSelect = [ ( (pt 0 0, pt 1 1)
                        , (pt 1 1, pt 0 0)
                        , "reversed" )
                      , ( (pt 0 0, pt 1 1)
                        , (pt 1 1, pt 2 2)
                        , "head 2 tail" )
                      , ( (pt 1 1, pt 2 2)
                        , (pt 0 0, pt 1 1)
                        , "tail 2 head" )
                      , ( (pt 1 1, pt 0 0)
                        , (pt 1 1, pt 2 2)
                        , "tail 2 tail" )
                      , ( (pt 0 0, pt 1 1)
                        , (pt 2 2, pt 1 1)
                        , "head 2 head" )
                      ]
    traverse_ (uncurry $ flip $ test casesSelect)
       npCombosNResults

  describe "interiorIntersect" $ do
    for_ (allCombo nps) $ \(np0, np1) -> it (show np0 ++ " " ++ show np1) $ do
          let dl0 = nDS'' np0 (pt 0 0) (pt 1 1)
          let dl1 = nDS'' np1 (pt 1 0) (pt 0 1)
          let dl2 = nDS'' np1 (pt 1 0) (pt 2 1)

          -- crosses
          dl0 `interiorIntersect` dl1 `shouldBe` True

          -- disjoint
          dl0 `interiorIntersect` dl2 `shouldBe` False

    it "interiorIntersect with InteriorIntersectingSegms" $ do
      property $ uncurry interiorIntersect . getInteriorIntersectsDSegm

    describe "Bi Bi" $ do
      it "same line" $ do
        property $ \(s :: Segm) ->
          let ds = DSegm Bi s
          in ds `interiorIntersect` ds

      it "reverse line" $ do
        property $ \(s :: Segm) ->
          let ds = DSegm Bi s
          in not $ ds `interiorIntersect` reverse ds

  describe "TailHeadPoints" $ do
    it "tp" $ do
      property $ \(NonEqual (p0, p1)) np -> tp (nDS'' np p0 p1) == p0

    it "hp" $ do
      property $ \(NonEqual (p0, p1)) np -> hp (nDS'' np p0 p1) == p1

  describe "Arbitrary Helpers" $ do
    it "InteriorIntersectsDSegm" $ do
      property $ liftA2 (&&)
                   (\case (DSegm Bi s0, DSegm Bi s1) -> s0 /= reverse s1
                          _ -> True )
                   interiorIntersect' .
                 getInteriorIntersectsDSegm

