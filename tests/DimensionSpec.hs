module DimensionSpec where

import Dimension

import Test.Hspec


main :: IO ()
main = hspec spec


spec :: Spec
spec = do
  describe "x axis" $ do
    it "add" $ do
      (fromScalar 1 :: Ax) .+= (fromScalar 2 :: Rx) `shouldBe` (fromScalar 3 :: Ax)
      (fromScalar 1 :: Rx) =+. (fromScalar 2 :: Ax) `shouldBe` (fromScalar 3 :: Ax)

    it "subtract" $ do
      ((fromScalar 4 :: Ax)) .-. ((fromScalar 3 :: Ax)) `shouldBe` (fromScalar 1 :: Rx)

    it "multiply" $ do
      (fromScalar 2 :: Rx) =*= (fromScalar 3 :: Rx) `shouldBe` Squared (fromScalar 6 :: Rx)

    it "sq (square)" $ do
      sq ((fromScalar 3 :: Rx)) `shouldBe` Squared (fromScalar 9 :: Rx)

    -- describe "AbsoluteOps" $ do


  describe "squared" $ do
    it "xSqAdd" $ do
      let x2 = Squared (fromScalar 2 :: Rx)
      let y2 = Squared (fromScalar 3 :: Ry)
      x2 ^+^ y2 `shouldBe` (Squared $ Relative 5)

  it "ToScalar" $ do
    toScalar (Relative $ 5) == (5 :: Double)

