{-# LANGUAGE ScopedTypeVariables #-}

module IsCloseSpec where

import IsClose
  ( IsClose(isCloseAR, isCloseA, isCloseR, isClose, isCloseNZ)
  , absErr
  )

import Ratio (Ratio(Ratio))

import Data.Foldable (traverse_)

import Test.Hspec
import Test.QuickCheck


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "IsClose" $ do
    describe "isClose" $ do
      it "single float error" $ do
        property (\(x :: Double) y -> ((x + y) - y) `isClose` x)

      it "over absolute error" $ do
        let offset = 2 * absErr
        let test (Ratio x) = all (not . isClose x . ($ offset) . ($ x)) [(+), (-)]
        property test

    describe "isCloseNZ" $ do
      it "single float error" $ do
        property (\(x :: Double) y -> ((x + y) - y) `isCloseNZ` x)

      it "over absolute error" $ do
        let offset = 2 * absErr
        let test (Ratio x) = all (not . isCloseNZ x . ($ offset) . ($ x)) [(+), (-)]
        property test

    it "isCloseNZ" $ do
      property (\(Ratio x) (Ratio y) -> ((x + y) - y) `isCloseNZ` x)


    let absoluteCases = [ (1, 0, 1, 2, True,  "close on edge")
                        , (2, 0, 1, 2, True,  "close within")
                        , (1, 0, 2, 0, False, "far") ]

    let relativeCases = [ (0, 0.5, 1, 2, True,  "close on edge")
                        , (0, 0.6, 1, 2, True,  "close within")
                        , (0, 0.2, 1, 2, False, "far") ]

    let test func (a, r, n, n' :: Double, result, testName) = it testName $ do
            let ic = func a r
            ic n  n' `shouldBe` result
            ic n' n  `shouldBe` result

    describe "isCloseAR" $ do
      describe "absolute difference" $ do
        traverse_ (test isCloseAR) absoluteCases

      describe "relative difference" $ do
        traverse_ (test isCloseAR) relativeCases

    describe "isCloseA" $ do
      traverse_ (test (\a _ -> isCloseA a)) absoluteCases

      it "call isCloseA" $ do
        isCloseA 1 1 (2 :: Double) `shouldBe` True

    describe "isCloseR" $ do
      traverse_ (test (\_ r -> isCloseR r)) relativeCases
