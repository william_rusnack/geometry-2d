{-# LANGUAGE ScopedTypeVariables #-}

module LineArbitrarySpec where

import LineArbitrary
  (
    NonParallel(NonParallel)
  , ParallelLines(ParallelLines)
  , LinePointDisjoint(LinePointDisjoint)
  , CollinearLines(CollinearLines)
  , NonCollinearLines(NonCollinearLines)
  , ParallelNotCollinearLines(ParallelNotCollinearLines)
  , pointOnEachSide
  , twoPointsOnOneSide
  )

import Dimension
  ( Squared(getSquared)
  , sq
  , fConv
  )
import Vector
  ( xp
  , toVec
  , parallel
  , _toUv
  , dp
  , perp
  )
import PLS
  ( Line(Line)
  , (.<-.)
  , (.->.)
  , tp
  )

import Utilities
  ( bimap'
  , zero
  )
import IsClose (isClose)

import Unified
  ( singleton
  )
import Unified.Collections.List (List(List))
import Unified.Utilities.List (zipL)

import Data.Bifunctor (bimap)
import Data.Tuple (swap)
import Test.Hspec hiding (parallel)
import Test.QuickCheck


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  it "Arbitrary" $ do
    property (\(Line (p0, p1)) -> p0 /= p1)

  it "NonParallel" $ do
    let test :: NonParallel Line -> Bool
        test (NonParallel (l0, l1)) = not $ toVec l0 `parallel` toVec l1
    property test

  it "ParallelLines" $ do
    let test :: ParallelLines Line -> Bool
        test (ParallelLines (l0, l1, _)) = toVec l0 `parallel` toVec l1
    property test

  it "LinePoint, distance is correct" $ do
    -- let test :: LinePointDisjoint Line -> Bool
    --     test (LinePointDisjoint (l, p, d)) =
    --       d `isClose` sq d'
    --       where Squared d' = u `dp` vp
    --             vp = tp l .->. p
    --             u = perp $ _toUv l
    -- property test

    property $ \(LinePointDisjoint (l :: Line, p, d)) -> let
      vp = tp l .->. p
      u = perp $ _toUv l
      in d `isClose` (getSquared $ sq $ fConv $ u `dp` vp)

  it "LinePointDisjoint, point not on line" $ do
    let test :: LinePointDisjoint Line -> Bool
        test (LinePointDisjoint (l, p, _)) =
          not $ parallel (tp l .->. p) (toVec l) && tp l /= p
    property test

  it "CollinearLines" $ do
    let test :: CollinearLines Line -> Bool
        test (CollinearLines (l0, l1)) =
          v0 `parallel` toVec l1 &&
          ( (tp l0 .->. tp l1) `parallel` v0 ||
            tp l0 == tp l1 )
          where v0 = toVec l0
    property test

  it "NonCollinearLines" $ do
    let test :: NonCollinearLines Line -> Bool
        test (NonCollinearLines (l0, l1)) =
          not (v0 `parallel` toVec l1) ||
          not ( v0 `parallel` (tp l0 .->. tp l1) ||
                tp l0 == tp l1)
          where v0 = toVec l0
    property test

  it "ParallelNotCollinearLines" $ do
    let test :: ParallelNotCollinearLines Line -> Bool
        test (ParallelNotCollinearLines (l0, l1, _)) =
          v0 `parallel` toVec l1 &&
          not (v0 `parallel` (tp l0 .->. tp l1)) &&
          tp l0 /= tp l1
          where v0 = toVec l0
    property test

  describe "pointOnEachSide" $ do
    it "point on each side" $ do
      property $ \(l :: Line) -> forAll (pointOnEachSide l) $
        any (uncurry (&&) . bimap (> zero) (< zero)) .
        (\p -> [p, swap p]) .
        (\v (v0, v1) -> (v `xp` v0, v `xp` v1)) (toVec l) .
        (\(p0, p1) -> (tp l .->. p0, tp l .->. p1))

    it "points are not equal" $ do
      property $ \(l :: Line) -> forAll (pointOnEachSide l) $ uncurry (/=)

  describe "twoPointsOnOneSide" $ do
    it "non equal points" $ do
      property $ \(l :: Line) -> forAll (twoPointsOnOneSide l) $ uncurry (/=)

    it "on same side of line" $ do
      property $ \(l :: Line) -> forAll (twoPointsOnOneSide l) $
        any (uncurry (&&)) .
        uncurry zipL .
        bimap' ( (List [(> zero), (< zero)] <*>) .
                 singleton .
                 (`xp` toVec l) .
                 (.<-. tp l) )
