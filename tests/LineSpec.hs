{-# LANGUAGE FlexibleContexts
           , ScopedTypeVariables
           , PartialTypeSignatures #-}

module LineSpec where

import PLS
  ( Line(Line)
  , intersectPoint
  , intersectPoint'
  , TailHeadPoints(tp, hp, thps, thps')
  , ToRight(toRight)
  , From2Points((..-))
  , collinearL
  , (|--|)

  -- Point
  , Point
  , Translate(translate)
  , pt
  , (.+->), (.->.)

  -- Segm
  , getLine
  )
import LineArbitrary
  ( NonParallel(NonParallel, getNonParallel)
  , LinePointDisjoint(LinePointDisjoint)
  , ParallelLines(ParallelLines, getParallelLines)
  , getNonCollinearLines
  , getCollinearLines
  )
import SegmArbitrary (getCollinearIntersectingSegms)

import IsClose (IsClose(isClose))
import GeometryClasses (Contain(contains))
import Dimension
  ( Distance
  , (=*~)
  , negate
  , sq
  , Squared
  , fromScalar
  )
import Vector
  ( Vector
  , NZVector(NZVector)
  , perpendicular
  , parallel
  , collinearV
  , toVec
  , _toNZV
  , _toUv
  , toward
  , Ward(Rightward, Leftward)
  )

import Utilities (zero, t3to2, bimap')
import Utilities.Arbitrary (NonEqual(NonEqual))

import Unified.Collections.List (List(List))

import Data.Maybe (isNothing, fromJust)
import Test.Hspec hiding (parallel)
import Test.QuickCheck

import Prelude hiding (negate, getLine)


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "From2Points Line" $ do
    it "Nothing if Points equal" $ do
      property (\p -> (p ..- p :: Maybe Line) == Nothing)

    it "Just if Points disjoint" $ do
      property (\(NonEqual (p0, p1)) -> (p0 ..- p1) == (Just $ Line (p0, p1)))

  it "TailHeadPoints" $ do
    let p0 = pt 0 1
    let p1 = pt 2 3
    let l = Line (p0, p1)
    tp l `shouldBe` p0
    hp l `shouldBe` p1
    thps l `shouldBe` (p0, p1)
    thps' l `shouldBe` List [p0, p1]

  describe "intersectPoint" $ do
    describe "specific cases" $ do
      it "outside points" $ do
        Line (pt 2 0, pt 2 1) `intersectPoint` Line (pt 0 2, pt 1 2)
        `shouldBe` Just (pt 2 2)

      it "inside points" $ do
        Line (pt 2 0, pt 0 2) `intersectPoint` Line (pt 0 0, pt 2 2)
        `shouldBe` Just (pt 1 1)

    it "parallel has no intersect point" $ do
      property $ isNothing .
                 (uncurry intersectPoint :: (Line, Line) -> Maybe Point) .
                 t3to2 .
                 getParallelLines

    it "intersecting lines contain intersect point" $ do
      let test :: NonParallel Line -> Bool
          test (NonParallel (l0, l1)) = all (`contains` ip) [l0, l1]
            where Just ip = intersectPoint l0 l1
      property test

    it "collinear lines with segment overlap do not have an intersect point" $ property $
      isNothing .
      intersectPoint' .
      bimap' getLine .
      t3to2 .
      getCollinearIntersectingSegms

    it "specific collinear intersect" $ do
      fromJust ((pt (-5.005235899550797) 2.2760524417775456) ..- (pt (-13.244771127731275) 0.3061315277516876) :: Maybe Line)
      `intersectPoint`
      fromJust ((pt (-5.005235899550797) 2.2760524417775456) ..- (pt (-76.6515837899691) (-14.853267988845166)))
      `shouldBe` Nothing

  describe "contains" $ do
    it "does contain" $ do
      let test :: Line -> Double -> Bool
          test l f = l `contains` (tp l .+-> toVec l =*~ f)
      property test

    it "does not contain" $ do
      let test :: LinePointDisjoint Line -> Bool
          test (LinePointDisjoint (l, p, _)) = not $ contains l p
      property test

  it "translate (both moved by vector)" $ do
    let test :: Line -> Vector -> Bool
        test l@(Line (p0, p1)) v = all ((isClose v) . (uncurry (.->.))) [(p0, p0'), (p1, p1')]
          where (Line (p0', p1')) = translate v l
    property test

  describe "parallel" $ do
    it "is parallel" $ do
      property $ uncurry (parallel :: Line -> Line -> Bool) .
                 t3to2 .
                 getParallelLines

    it "not parallel" $ do
      property $ not .
                 uncurry (parallel :: Line -> Line -> Bool) .
                 getNonParallel


  describe "collinearL" $ do
    it "is collinear" $ do
      property $ uncurry (collinearL :: Line -> Line -> Bool) .
                 getCollinearLines

    it "not collinear" $ do
      property $ not .
                 uncurry (collinearL :: Line -> Line -> Bool) .
                 getNonCollinearLines

  describe "DistanceBetween" $ do
    describe "Line Point" $ do
      it "contained point is near zero" $ do
        let test :: Line -> Double -> Bool
            test l f = l |--| p `isClose` zero
              where p = hp l .+-> (v =*~ f)
                    v = toVec l
        property test

      it "always positive" $ do
        property $ (>= zero) .
                   (uncurry (|--|) :: (Line, Point) -> Squared Distance)

      it "correct distance" $ do
        property $ \(LinePointDisjoint ((l :: Line), p, d)) ->
          l |--| p `isClose` d

    describe "Line Line" $ do
      it "non-parallel is zero" $ do
        property $ (== zero) .
                   (uncurry (|--|) :: (Line, Line) -> Squared Distance) .
                   getNonParallel

      it "parallel line distance" $ do
        property $ \(ParallelLines (l0 :: Line, l1, d)) ->
          (l0 |--| l1) `isClose` d

  describe "toRight" $ do
    it "is parallel" $ do
      property $ \(l :: Line) (d :: Distance) ->
        toRight d l `parallel` l

    it "correct distance" $ do
      let test :: Line -> Distance -> Bool
          test l d = (toRight d l |--| l) `isClose` sq d
      property test

    it "perpendicular points" $ do
      let test :: Line -> NonZero Double -> Bool
          test l (NonZero d) = l `perpendicular` (tp l .->. tp l') &&
                               l `perpendicular` (hp l .->. hp l')
            where l' = toRight (fromScalar d) l
      property test

    it "right if positive and left if negative" $ do
      let test :: Line -> NonZero Double -> Bool
          test l (NonZero d) = if d > 0
                               then l `toward` v == Rightward
                               else l `toward` v == Leftward
            where v = tp l .->. tp (toRight d' l)
                  d' = fromScalar d
      property test

  describe "vectorize" $ do
    it "toVec" $ do
      let p0 = pt 1 2
      let p1 = pt 3 5
      _toNZV (Line (p0, p1)) `shouldBe` NZVector (p0 .->. p1)

    it "toUv" $ do
      let test :: Line -> Bool
          test l = _toUv l `collinearV` _toNZV l
      property test


  it "IsClose" $ do
    let test :: Line -> Vector -> Bool
        test l v = foldr translate l [v, negate v] `isClose` l
    property test
