{-# LANGUAGE GeneralizedNewtypeDeriving
           , PartialTypeSignatures
           , TypeApplications
           #-}

module NormalizedSpec where

import Normalized
  ( Normalized(Normalized, getNormalized)
  , normalize
  , NormalizedBounded(lowerBound, upperBound)
  , normalizeLU
  , NormalizedLU(getNormalizedLU)

  , getNormalizedRangeLowerToUpper
  , getNormalizedRangeUpperToLower
  )

import GeometryClasses (contains, contains', interiorContains, interiorContains')

import IsClose (IsClose, isCloseAR)
import Range (Range, lower, upper)

import Utilities (zero, mkSnd, Zero, (<$<), bimap')
import Utilities.Arbitrary (chooseXU, chooseXL, chooseXLU, lessThan, greaterThan)

import Control.Applicative (liftA2)
import Control.Arrow ((&&&))
import Data.Fixed (mod')
import System.Random (Random)
import Test.Hspec
import Test.QuickCheck

import Prelude hiding (pi, sin, cos)


main :: IO ()
main = hspec spec

double :: a -> (a, a)
double = undefined


newtype TestType0 = TestType0 Double
  deriving (Show, Eq, Ord, Arbitrary, Random, Num, Real, Zero, IsClose)
instance NormalizedBounded TestType0 where
  lowerBound = 3
  upperBound = 10


spec :: Spec
spec = do
  describe "normalize" $ do
    let ic v = any (isCloseAR 1e-5 1e-5 v) [zero, upperBound - lowerBound]

    describe "below" $ do
      let gb = lessThan lowerBound :: Gen TestType0

      it "alwayse within bounds" $
        forAll gb $
          ( liftA2 (&&) (< upperBound) (>= lowerBound) .
            getNormalized .
            normalize )

      it "original value is n away from the original value" $
        forAll gb $
          ( ic .
            (`mod'` (upperBound - lowerBound)) .
            uncurry (-) .
            mkSnd (getNormalized . normalize) )

    describe "above" $ do
      let gb = greaterThan upperBound :: Gen TestType0

      it "alwayse within bounds" $
        forAll gb $
          ( liftA2 (&&) (< upperBound) (>= lowerBound) .
            getNormalized .
            normalize )

      it "original value is n away from the original value" $
        forAll gb $
          ( ic .
            (`mod'` (upperBound - lowerBound)) .
            uncurry (-) .
            mkSnd (getNormalized . normalize) )

    describe "bounds" $ do
      it "upper bound converts to lower bound" $ do
        getNormalized (normalize upperBound) `shouldBe` (lowerBound :: TestType0)

      it "lower bound does not change" $ do
        getNormalized (normalize lowerBound) `shouldBe` (lowerBound :: TestType0)

  describe "normalizeLU" $ do
    let ic v = any (isCloseAR 1e-5 1e-5 v) [zero, upperBound - lowerBound]

    describe "below" $ do
      let gb = lessThan lowerBound :: Gen TestType0

      it "alwayse within bounds" $
        forAll gb $
          ( liftA2 (&&) (<= upperBound) (>= lowerBound) .
            getNormalizedLU .
            normalizeLU )

      it "original value is n away from the original value" $
        forAll gb $
          ( ic .
            (`mod'` (upperBound - lowerBound)) .
            uncurry (-) .
            mkSnd (getNormalizedLU . normalizeLU) )

    describe "above" $ do
      let gb = greaterThan upperBound :: Gen TestType0

      it "alwayse within bounds" $
        forAll gb $
          ( liftA2 (&&) (<= upperBound) (>= lowerBound) .
            getNormalizedLU .
            normalizeLU )

      it "original value is n away from the original value" $
        forAll gb $
          ( ic .
            (`mod'` (upperBound - lowerBound)) .
            uncurry (-) .
            mkSnd (getNormalizedLU . normalizeLU) )

    describe "bounds" $ do
      it "upper bound does not change" $ do
        getNormalizedLU (normalizeLU upperBound) `shouldBe` (upperBound :: TestType0)

      it "lower bound does not change" $ do
        getNormalizedLU (normalizeLU lowerBound) `shouldBe` (lowerBound :: TestType0)

  describe "Range Contains Normalized Value" $ do
    describe "contains" $ do
      describe "lower < upper" $ do
        it "does contain" $ property $
          liftA2 forAll
            ( Normalized <$<
              choose .
              bimap' getNormalized .
              (lower &&& upper) )
            (contains :: Range (Normalized TestType0) -> Normalized TestType0 -> Bool) .
          getNormalizedRangeLowerToUpper

        -- describe "does not contain" $ do
          -- it "lower" $ property $
          --   liftA2 forAll
          --     ( Normalized <$<
          --   getNormalizedRangeLowerToUpper

          -- it "upper" $ property $

        it "does not contain" $ property $
          liftA2 forAll
            ( Normalized <$<
              oneof .
              (\r ->
                map (\(c, f, _) -> c $ f r) $
                filter (\(_, f, b) -> f r /= b)
                  [ (chooseXU lowerBound,      getNormalized . lower, lowerBound)
                  , (flip chooseXL upperBound, getNormalized . upper, upperBound) ] ) )
            (curry $ not . contains') .
          getNormalizedRangeLowerToUpper @TestType0

      describe "upper < lower" $ do
        it "does contain" $ property $
          liftA2 forAll
            ( Normalized <$<
              oneof .
              (\r ->
                map (\(c, f, _) -> c $ f r) $
                filter (\(_, f, b) -> f r /= b)
                  [ ((curry choose) upperBound,      getNormalized . lower, upperBound)
                  , (flip (curry choose) lowerBound, getNormalized . upper, lowerBound) ] ) )
            contains .
          getNormalizedRangeUpperToLower @TestType0

        it "does not contain" $ property $
          liftA2 forAll
            ( Normalized <$<
              liftA2 chooseXLU
                (getNormalized . lower)
                (getNormalized . upper) )
            (curry $ not . contains') .
          getNormalizedRangeUpperToLower @TestType0

    describe "interiorContains" $ do
      describe "lower < upper" $ do
        it "does contain" $ property $
          liftA2 forAll
            ( Normalized <$<
              liftA2 chooseXLU
                (getNormalized . lower)
                (getNormalized . upper) )
            interiorContains .
          getNormalizedRangeLowerToUpper @TestType0

        it "does not contain" $ property $
          liftA2 forAll
            ( Normalized <$<
              oneof .
              (\r ->
                map (\(c, f, _) -> c $ f r) $
                filter (\(_, f, b) -> f r /= b)
                  [ (curry choose lowerBound,        getNormalized . lower, lowerBound)
                  , (flip (curry choose) upperBound, getNormalized . upper, upperBound) ] ) )
            (curry $ not . interiorContains') .
          getNormalizedRangeLowerToUpper @TestType0

      describe "upper < lower" $ do
        it "does contain" $ property $
          liftA2 forAll
            ( Normalized <$<
              oneof .
              (\r ->
                map (\(c, f, _) -> c $ f r) $
                filter (\(_, f, b) -> f r /= b)
                  [ (chooseXL upperBound,      getNormalized . lower, upperBound)
                  , (flip chooseXU lowerBound, getNormalized . upper, lowerBound) ] ) )
            interiorContains .
          getNormalizedRangeUpperToLower @TestType0

        it "does not contain" $ property $
          liftA2 forAll
            ( Normalized <$<
              liftA2 (curry choose) (getNormalized . lower) (getNormalized . upper) )
            (curry $ not . interiorContains') .
          getNormalizedRangeUpperToLower @TestType0

  describe "Arbitrary Instances" $ do
    it "NormalizedRangeLowerToUpper" $ property $
      liftA2 (<) lower upper .
      getNormalizedRangeLowerToUpper @TestType0

    it "NormalizedRangeUpperToLower" $ property $
      liftA2 (>) lower upper .
      getNormalizedRangeUpperToLower @TestType0

