{-# LANGUAGE LambdaCase
           , ScopedTypeVariables #-}

module PointSpec where

import PLS
  ( Point(Point)
  , pt
  , (.->.)
  , (.<-.)
  , (.+->)
  , (|--|)
  , avgPoint
  , rotate
  )

import IsClose (IsClose(isClose))
import Dimension
  ( Absolute(Absolute)
  , Rx
  , Ry
  , RelativeAddition(negate)
  , fromScalar
  , (.+=)
  , (=/~)
  , _toRelative
  , abs
  )
import Angle (getHalfCircleAngle, PiPi(PiPi, getPiPiAngle), normalize)
import Vector(Vector, _vec, vecAngle, toNZV)

import Utilities (zero, bimap', mkFst)
import Normalized (getNormalizedLU)

import Unified.Collections.List (List)
import Unified.AtLeast (NonEmpty)

import Data.Bitraversable (bisequenceA)
import Data.Foldable (traverse_)
import Test.Hspec
import Test.QuickCheck

import Prelude hiding (negate, abs)


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  it "pt (creates point)" $ do
    pt 1 2 `shouldBe` Point (Absolute 1, Absolute 2)

  describe "diffPoints (.->.) (.<-.)" $ do
    it "specific cases" $ do
      (pt 1 2) .->. (pt 3 5) `shouldBe` _vec 2 3
      (pt 1 2) .<-. (pt 3 5) `shouldBe` _vec (-2) (-3)

    it "property" $ do
      property (\p0 p1 -> (p0 .+-> (p0 .->. p1)) `isClose` p1)

    it "(.->.) reverse of (.<-.)" $ do
      property (\p0 p1 -> (p0 .->. p1) == negate (p0 .<-. p1))

  it "pointPlusVector" $ do
    (pt 2 3) .+-> (_vec 4 6 :: Vector) `shouldBe` (pt 6 9)

  describe "DistanceBetween Point Point" $ do
    let test (p0, p1, result, testName) = it testName $ do
          let r = fromScalar result
          p0 |--| p1 `shouldBe` r
          p1 |--| p0 `shouldBe` r

    traverse_ test [ (pt 0 0, pt 3 2, 13, "angled")
            , (pt 1 1, pt 1 3, 4,  "vertical")
            , (pt 1 1, pt 4 1, 9,  "horizontal")
            , (pt 1 1, pt 1 1, 0,  "same point") ]

  it "avgPoint" $ do
    property $
      (\(ps :: NonEmpty List Point) ->
        let l = fromIntegral $ length ps in
        avgPoint ps ==
          ( Point $
            (\(x, y) -> (x =/~ l, y =/~ l)) $
            foldr (\(Point (x, y)) (sx, sy) -> ( x .+= (_toRelative sx :: Rx)
                                               , y .+= (_toRelative sy :: Ry) ) )
                  (zero, zero)
                  ps ) )

  describe "rotate" $ do
    it "rotated by andle" $ do
      property $ \(c, a, p) ->
          case getHalfCircleAngle .
               getNormalizedLU .
               uncurry vecAngle <$>
                ( bisequenceA $
                  bimap' (toNZV . (.<-. c)) $
                  mkFst (rotate c a) p ) of
            Just va -> do
              isClose va (abs $ getPiPiAngle $ getNormalizedLU $ normalize $ PiPi a)
            Nothing -> True

    it "same distance from center of rotation" $ do
      property $ \c a p -> (rotate c a p) |--| c `isClose` p |--| c
