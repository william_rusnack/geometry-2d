{-# LANGUAGE ViewPatterns
           , LambdaCase
           , PartialTypeSignatures
           , FlexibleContexts #-}

module PolygonSpec where

import Polygon
  ( Polygon(Polygon, exteriorRing, interiorRings)
  , getPolygonPoints
  , numPolygonPoints
  , combinePolygonRings
  , triangulate
  )

import Dimension
  ( fromScalar
  , toScalar
  )
import Area (FindArea(area), Area)
import PLS (pt, Intersect(interiorIntersect), Point)
import Triangle (Triangle)
import Ring (toRingUnClosed)

import Unified
  ( (<|)
  , fromEFoldable
  )
import Unified.Utilities (concat)
import Unified.Collections.List (List(List))
import Unified.Utilities.List (allCombo)
import Unified.Collections.Set (Set)

import Control.Applicative (liftA2)
import Control.Arrow ((&&&))
import Data.Foldable (traverse_)
import Data.Maybe (fromJust, isJust)
import Data.Monoid (Sum(Sum, getSum))
import Test.Hspec
import Test.QuickCheck

import Prelude hiding (map, concat)


main :: IO ()
main = hspec spec


tAreas :: List Triangle -> Area
tAreas = fromScalar . sum . fmap (toScalar . area)

justOrError :: String -> Maybe a -> a
justOrError msg = \case Just v -> v
                        Nothing -> error msg

spec :: Spec
spec = do
  describe "basic functions on generic polygon" $ do
    let eps = List [pt 0 0, pt 1 1, pt 2 2]
    let ips = List $ fmap List
              [ [pt 3 3, pt 4 4]
              , [pt 5 5, pt 6 6, pt 7 7] ]
    let aps = concat $ eps <| ips
    let pg = Polygon (justOrError "cannot convert exterior" $ toRingUnClosed eps)
                     ( justOrError "cannot convert interiors" $
                       sequenceA $
                       fmap toRingUnClosed ips )

    it "getPolygonPoints" $ getPolygonPoints pg `shouldBe` (fromEFoldable aps :: Set Point)

    describe "numPolygonPoints" $ do
      it "specific case" $ numPolygonPoints pg `shouldBe` length aps

      it "property" $ property $
        uncurry (==) .
        ( numPolygonPoints &&&
          ( getSum .
            foldMap (Sum . length) .
            liftA2 (<|) exteriorRing interiorRings :: Polygon -> Int) ) .
        uncurry Polygon

    describe "combinePolygonRings" $ do
      it "specific case" $
        Just (combinePolygonRings pg) `shouldBe`
        sequenceA (fmap toRingUnClosed (eps <| ips))

      it "property" $ property $
        uncurry (==) .
        ( combinePolygonRings &&&
          liftA2 (<|) exteriorRing interiorRings ) .
        uncurry Polygon

  describe "triangulate" $ do
    let test (List -> er, List . fmap List -> irs, fromScalar -> a, testName) = it testName $ do
          let pg = Polygon
                (justOrError "cannot convert exterior" $ toRingUnClosed er)
                (justOrError "cannot convert interiors" $ sequenceA $ fmap toRingUnClosed irs)
          let mts = triangulate pg
          isJust mts `shouldBe` True
          let ts = fromJust mts

          length ts `shouldBe` length (getPolygonPoints pg) - 2
          any (uncurry interiorIntersect) (allCombo ts) `shouldBe` False
          (concat $ fmap fromEFoldable ts) `shouldBe` getPolygonPoints pg
          tAreas ts `shouldBe` a

    traverse_ test [
        ( [pt 0 0, pt 1 0, pt 0 2]
        , []
        , 1
        , "triangle" )
      , ( [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
        , []
        , 1
        , "square" )
      , ( [pt 0 0, pt 1 2, pt 0 1, pt (-1) 2]
        , []
        , 1
        , "star trek" )
      , ( [pt 0 0, pt 2 0, pt 2 1, pt 1 1, pt 1 2, pt 2 2, pt 2 3, pt 0 3]
        , []
        , 5
        , "C" )
      , ( [pt 0 0, pt 3 0, pt 3 3, pt 0 3]
        , [[pt 1 1, pt 2 1, pt 2 2, pt 1 2]]
        , 8
        , "square square" )
      -- , ( [ [pt 0 0, pt 4 0, pt 4 4, pt 0 4]
      --     , [pt 1 1, pt 2 1, pt 1 2]
      --     , [pt 3 2, pt 3 3, pt 2 3] ]
      --   , 15
      --   , "square triangle triangle" )
      -- , ( -- Reference: ../imgs/house-with-6-triangle-windows.png
      --     [ [pt 0 0, pt 7 0, pt 7 4, pt 4 9, pt 0 4]
      --     , [pt 1 1, pt 2 1, pt 2 2]
      --     , [pt 3 1, pt 4 1, pt 4 2]
      --     , [pt 5 1, pt 6 1, pt 6 2]
      --     , [pt 1 3, pt 2 3, pt 2 4]
      --     , [pt 3 3, pt 4 3, pt 4 4]
      --     , [pt 5 3, pt 6 3, pt 6 4] ]
      --   , 7*4 + 7*(9-4)/2 - 6/2
      --   , "house with 6 triangle windows" )
      ]
