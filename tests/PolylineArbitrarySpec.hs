{-# LANGUAGE FlexibleContexts
           , ScopedTypeVariables
           , DataKinds
           , TupleSections
           , NoImplicitPrelude
           , PartialTypeSignatures
           , ViewPatterns
           , LambdaCase
           , TypeApplications #-}

-- module Main where
module PolylineArbitrarySpec where

import PolylineArbitrary
import Polyline

import GeometryClasses (contains, interiorContains, contains')
import Vector (collinearV')
import qualified PLS
import PLS hiding (reverse)
import SegmArbitrary (getSegmPointCoincide, interiorPointOnSegment)

import IsClose (isClose)

import Utilities

import Unified hiding (fst)
import Unified.Arbitrary (uniqueValues)
import Unified.AtLeast (AtLeast(AtLeast), tailInitAL)
import Unified.Collections.List (List, getList)
import Unified.Collections.Set (Set)
import Unified.QuickCheck
import Unified.Utilities (isInfixOf, unzip)
import Unified.Utilities.List (toPairsL, allCombo'', secondCombo, filterL)

import Data.Bitraversable (bisequenceA)
import Control.Applicative (liftA2)
import Control.Arrow ((&&&))
import Control.Monad
import Data.Bifunctor (bimap, first, second)
import Data.Bits (xor)
import Data.Bool
import Data.Composition ((.:))
import Data.Either
import Data.Eq
import Data.Foldable hiding (length)
import Data.Function
import Data.Functor
import Data.Int
import Data.List.HT (isAscending)
import Data.Maybe
import Data.Ord
import Data.Semigroup
import Data.Tuple
import GHC.Base (String, undefined)
import GHC.IO
import GHC.Num
import GHC.Show (show)
import Test.HUnit (assertEqual)
import Test.Hspec
import Test.Hspec.Core.Spec (SpecM)
import Test.QuickCheck hiding (elements)


main :: IO ()
main = hspec spec

ps2pl :: Foldable t => t Point -> Polyline
ps2pl = Polyline . fromJust . fromFoldableMay

tps2pl :: ([Point], [Point]) -> (Polyline, Polyline)
tps2pl = bimap' ps2pl

spec :: Spec
spec = do
  it "PolylineAndContainedPoint" $ do
    property $ (\(pl, p) -> any (`contains` p) $ edges pl) .
               getPolylineAndContainedPoint

  it "PointOnPolylineSegm" $ do
    property $ (\(pl, p) -> any (`contains` p) $ edges pl) .
               getPointOnPolylineSegm

  describe "PolylineAndEndpoint" $ do
    let isEndpoint =
          (\(pl, p) -> biLiftC' (||) (== p) $ thps pl) .
          getPolylineAndEndpoint
    let getEndPoint = snd . getPolylineAndEndpoint

    it "point is an enput of polyline" $ property $
      isEndpoint

    describe "shrink" $ do
      it "endpoint remains the same" $
        noShrinking $ property $
        uncurry all .
        bimap ((==) . getEndPoint) (fmap getEndPoint) .
        mkSnd (shrink @PolylineAndEndpoint)

      it "still have point as an endpoint of polyline" $
        noShrinking $ property $
        traverse_ (flip shouldSatisfy (uncurry hasEndpoint) . getPolylineAndEndpoint) .
        shrink

      it "does not contain itself" $
        noShrinking $ property $
        not .
        uncurry elem .
        mkSnd (shrink @PolylineAndEndpoint)

  describe "PolylineAndUniqueEndpoint" $ do
    it "is an endpoint" $ property $
      uncurry hasEndpoint .
      getPolylineAndUniqueEndpoint

    it "is not an interior point" $ property $
      not .
      uncurry elem .
      swap .
      first (fmap getInterior . interiorPoints) .
      getPolylineAndUniqueEndpoint

  it "PolylinesTwoSharedEndpoints both have the same endpoints" $ property $
    biLiftC' shouldBe (fromEFoldable @Set . thps') .
    getPolylinesTwoSharedEndpoints

  it "PolylinesWithNoSharedEndpoints do not share an endpoint" $ property $
    (`shouldSatisfy` disjoint') .
    bimap' thps' .
    getPolylinesWithNoSharedEndpoints

  it "PolylineAndConnectorPoint" $ do
    property $ uncurry elem .
               fmap (tailInitAL . getPolyline) .
               swap .
               getPolylineAndConnectorPoint

  describe "PolylineAndInteriorPoint" $ do
    it "point is contained by and edge" $ property $
      (\(pl, p) -> any (`contains` p) $ edges pl) .
      getPolylineAndInteriorPoint

    it "point is not an endpoint" $
      withMaxSuccess 1000 $ property $
      (\(pl, p) -> all (/= p) $ thps' pl) .
      getPolylineAndInteriorPoint

  it "PolylineAndInteriorEndSegmPoint" $ do
    let test :: PolylineAndInteriorEndSegmPoint -> Bool
        test (PolylineAndInteriorEndSegmPoint (pl, p)) =
          any (`interiorContains` p) ([headNE es, lastNE es] :: [Segm])
          where es = edges pl
    property test

  describe "PolylineISsCP" $ do
    it "point is on an interior edge" $ property $
      (\(pl, p) -> any (`contains` p) $ interiorEdges pl) .
      getPolylineISsCP

    it "not on an end edge" $ property $
      not .
      (\(pl, p) -> any (`contains` p) $ endEdges pl) .
      getPolylineISsCP

  it "DisjointPolylineAndPoint" $ do
    property $ (\(pl, p) -> not $ any (`contains` p) $ edges pl) .
               getDisjointPolylineAndPoint

  it "SharedPolylineEndpoint" $ property $
    uncurry (&&) .
    uncurry bimap' .
    first (flip hasEndpoint) .
    swap .
    getSharedPolylineEndpoint
    -- let test (SharedPolylineEndpoint (pl0, pl1, p)) =
    --       all (elem p . thps') ([pl0, pl1] :: [Polyline])

  describe "PolylineIntersect" $ do
    it "point intersect" $
      forAll (suchThat
        (getPolylineIntersect <$> arbitrary)
        (isLeft . snd) ) $
        uncurry (biLiftC' (&&)) .
        bimap
          (any . flip contains . (\(Left x) -> x))
          (bimap' edges) .
        swap

    it "segm intersect" $
      forAll (suchThat
        (getPolylineIntersect <$> arbitrary)
        (isRight . snd) ) $
        uncurry (biLiftC' (&&)) .
        bimap
          (any . flip contains . (\(Right x) -> x))
          (bimap' edges) .
        swap

  describe "BoundryInteriorPlIntersect" $ do
    it "is an endpoint of one but not the other" $ property $
      (\((pl0, pl1), p) -> pl0 `hasEndpoint` p `xor` pl1 `hasEndpoint` p) .
      getBoundryInteriorPlIntersect

    it "both polylines contain the point" $ property $
      uncurry (&&) .
      uncurry bimap' .
      bimap (any . flip contains) (bimap' edges) .
      swap .
      getBoundryInteriorPlIntersect

    -- old test code for BoundryInteriorPlIntersect
    -- let test (BoundryInteriorPlIntersect (pl0', pl1', p)) =
    --       any (\(pl0, pl1) -> pl0 `hasEndpoint` p &&
    --                           not (pl1 `hasEndpoint` p) &&
    --                           any (`contains` p) (edges pl1) )
    --           ([pls, swap pls] :: [(Polyline, Polyline)])
    --       where pls = (pl0', pl1')
    
    describe "shrink" $ do
      let getIntPoint = snd . getBoundryInteriorPlIntersect
      it "intersect point remains the same" $ property $
        uncurry all .
        bimap ((==) . getIntPoint) (fmap getIntPoint) .
        mkSnd shrink

      it "both polylines still have intersect point" $ property $
        all ( uncurry (&&) .
              (\(pls, p) -> bimap' (any (`contains` p) . edges) pls) .
              getBoundryInteriorPlIntersect ) .
        shrink

  describe "InteriorPolylineIntersect" $ do
    it "there is an intersect" $ property $
      any intersects' .
      uncurry allCombo'' .
      bimap' edges .
      fst .
      getInteriorPolylineIntersect

    -- not sure if this is correct
    it "should not be an end point" $
      withMaxSuccess 1000 $ forAll 
        ( suchThat
          (getInteriorPolylineIntersect <$> arbitrary)
          (isLeft . snd) ) $
      uncurry nand .
      (\(pls, p) -> bimap' (`hasEndpoint` p) pls) .
      second (\(Left x) -> x)

    xit "elem fromjust tailinit" False
    -- let test (InteriorPolylineIntersect (pl0, pl1, pos)) =
    --       any intersects' (allCombo'' (edges pl0) (edges pl1)) &&
    --       case pos of
    --         Left p -> (not (pl0 `hasEndpoint` p) || elem p (fromJust $ tailInit pl0)) &&
    --                   (not (pl1 `hasEndpoint` p) || elem p (fromJust $ tailInit pl1))
    --         _ -> True

  describe "InteriorCrossPlIntersect" $ do
    it "point is not an endpoint of either polyline" $ property $
      not .
      (\(pls, p) -> elem p $ uncurry (<>) $ bimap' thps' pls) .
      getInteriorCrossPlIntersect

    it "point is containd by a segment of both polylines" $ property $
      uncurry (&&) .
      (\(pls, p) -> bimap' (any (`contains` p) . edges) pls) .
      getInteriorCrossPlIntersect

  describe "InteriorTransPtPlIntersect" $ do
    it "is an interior point" $ property $
      (\(pls, p) -> biLiftC' (&&) (elem p . tailInitAL . getPolyline) pls) .
      getInteriorTransPtPlIntersect

    it "not an endpoint" $ property $
      (\(pls, p) -> biLiftC' (&&) (not . flip hasEndpoint p) pls) .
      getInteriorTransPtPlIntersect

  describe "InteriorTransPtSegmOfPlIntersect" $ do
    it "point is not end point" $ property $
      not .
      uncurry (||) .
      (\(pls, p) -> bimap' (`hasEndpoint` p) pls) .
      getInteriorTransPtSegmOfPlIntersect

    it "point is an interior point of one polyline and lies on a segment interior of the other" $ property $
      (uncurry xor) .
      (\(pls, p) -> bimap'
        ( uncurry (&&) .
          bimap (elem p . fmap getInterior . interiorPoints)
                (any (`interiorContains` p) . edges) )
        (mkSnd swap pls) ) .
      getInteriorTransPtSegmOfPlIntersect

  it "InteriorCollinearPlIntersect" $ property $
    (\(pls, s) -> 
      any ( liftA2 (||)
              (isClose $ Just s)
              (isClose $ Just $ PLS.reverse s) .
            intersectSegm' ) $
      uncurry allCombo'' $
      bimap' edges
      pls ) .
    getInteriorCollinearPlIntersect

  it "DisjointPolylines" $ property $
    not .
    any intersects' .
    uncurry allCombo'' .
    bimap' edges .
    getDisjointPolylines

  it "NonSelfIntersectingPolyline" $ do
    property $ liftA2 (&&) (not . any intersects' . secondCombo)
                           (not . any (uncurry interiorIntersect) . toPairsL) .
               edges .
               getNonSelfIntersectingPolyline

  describe "BoundryOnlyPolylineIntersect" $ do
    it "each polyline has at least 2 points" $ property $
      uncurry (&&) .
      bimap' (`lengthGTEq` 2) .
      fst .
      getBoundryOnlyPolylineIntersect

    it "shares an endpoint" $ property $
      uncurry (&&) .
      (\(pls, p) -> bimap' (flip hasEndpoint p) pls) .
      getBoundryOnlyPolylineIntersect

    it "no internal intersects" $ property $
      not .
      any interiorIntersect' .
      uncurry allCombo'' .
      bimap' interiorEdges .
      fst .
      getBoundryOnlyPolylineIntersect

    it "end segments do not intersect interior segments" $ property $
      void .
      bitraverse'
        ( traverse_ (`shouldSatisfy` (not . intersects')) .
          fmap (bimap getInterior getEnd) .
          biLift' allCombo'' interiorEdges endEdges ) .
      mkSnd swap .
      fst .
      getBoundryOnlyPolylineIntersect

    it "no zero length segments" $ property $
      void .
      bitraverse' (`shouldSatisfy` all (uncurry (/=)) . toPairsL) .
      fst .
      getBoundryOnlyPolylineIntersect

    describe "end segments do not have a" $ do
      let alignEndSegms = bimap' $
            uncurry (<|) .
            (firstEdge &&& singleton @List . PLS.reverse . lastEdge)

      it "interior intersect" $ property $
        not .
        any interiorIntersect' .
        uncurry allCombo'' .
        alignEndSegms .
        fst .
        getBoundryOnlyPolylineIntersect

      it "intersect on their interior point" $
        forAll (suchThat (fst . getBoundryOnlyPolylineIntersect <$> arbitrary)
                         (uncurry (&&) . bimap' (`lengthGT` 2)) ) $
          not .
          any (uncurry (==) . bimap' hp) .
          uncurry allCombo'' .
          alignEndSegms

    it "shrink stays connected" $ property $
      all ( uncurry (&&) .
            (\(pls, p) -> bimap' (`hasEndpoint` p) pls) .
            getBoundryOnlyPolylineIntersect ) .
      shrink @BoundryOnlyPolylineIntersect

  it "OpenPolyline" $ property $
    uncurry shouldNotBe .
    thps .
    getOpenPolyline

  it "ClosedPolyline" $ do
    property $ liftA2 (&&) ((>= 3) . length)
                           (uncurry (==) . thps) .
               getClosedPolyline

  it "SelfIntersectingPolyline" $ do
    let ec (Left  p)  s = s `contains` p
        ec (Right s') s = s `contains` s'

    property $ (>= 2) .
               length .
               (\(pl, pos) -> filterL (ec pos) $ edges pl) .
               getSelfIntersectingPolyline

  it "CrossPlSelfIntersect" $ do
    property $ (>= 2) .
               length .
               (\(pl, p) -> filterL (`contains` p) $ edges pl) .
               getCrossPlSelfIntersect

  it "TransPtPlSelfIntersect" $ do
    property $ (>= 2) .
               length .
               (\(pl, p) -> filterL (== Interior p) $ interiorPoints pl) .
               getTransPtPlSelfIntersect

  it "TransPtSegmOfPlSelfIntersect" $ do
    property $ (\(pl, p) ->
                 any (== Interior p) (interiorPoints pl) &&
                 any (`interiorContains` p) (edges pl)
               ) .
               getTransPtSegmOfPlSelfIntersect

  it "CollinearPlSelfIntersect" $ do
    property $ (>= 2) .
               length .
               (\(pl, s) -> filterL (`contains` s) (edges pl)) .
               getCollinearPlSelfIntersect

  it "TransPtEndPtPLSelfIntersect" $ do
    property $ (\(pl, p) -> pl `hasEndpoint` p &&
                            elem (Interior p) (interiorPoints pl) ) .
               getTransPtEndPtPLSelfIntersect

  it "EndPtSegmPlSelfIntersect" $ do
    property $ (\(pl, p) -> pl `hasEndpoint` p &&
                            any (`interiorContains` p) (edges pl) ) .
               getEndPtSegmPlSelfIntersect

  describe "PolylinePointDistance" $ do
    it "close to point" $ property $
      (\((pl, p), d) -> any ((`isClose` d) . (|--| p)) (edges pl)) .
      getPolylinePointDistance

    it "close to segment" $ property $
      (\((pl, p), d) -> minimum (fmap (|--| p) (edges pl)) `isClose` d) .
      getPolylinePointDistance

  describe "PolylinePolylineDistance" $ do
    it "if distance is zero there is a polyline intersect" $
      forAll
        ( suchThat
            (getPolylinePolylineDistance <$> arbitrary)
            ((== zero) . snd) ) $
        any intersects' .
        uncurry allCombo'' .
        bimap' edges .
        fst

    it "distance is correct if > 0" $
      forAll
        ( suchThat
          (getPolylinePolylineDistance <$> arbitrary)
          ((/= zero) . snd) ) $
        uncurry shouldBe . swap .
        first ( minimum .
                fmap (uncurry (|--|)) .
                uncurry allCombo'' .
                bimap' edges )

  it "MissingPointPolyline" $ property $
    biLift' (biLiftC' xor)
      (\p -> biLift' (&&) (elem p) (not . elem p))
      (mkSnd swap) .
    swap .
    getMissingPointPolyline

  describe "shrinkBothEndsToContainedPoint" $ do
    let shrinkTest = shrinkBothEndsToContainedPoint
          BoundryInteriorPlIntersect
          getBoundryInteriorPlIntersect
    let extractPoint = snd . getBoundryInteriorPlIntersect
    let extractPls = fst . getBoundryInteriorPlIntersect

    it "not empty" $
      forAll (suchThat arbitrary $
        not .
        (\((pl, _), p) -> firstEdge pl `contains` p && lastEdge pl `contains` p) .
        getBoundryInteriorPlIntersect ) $
      (`lengthGT` 0) .
      shrinkTest

    it "ascending number of points" $ property $
      isAscending .
      fmap (biLiftC' (+) length . extractPls) .
      shrinkTest

    it "p remains the same" $ property $
      uncurry traverse_ .
      bimap (flip shouldBe . extractPoint) (fmap extractPoint) .
      mkSnd shrink

    let pair :: ((a, b), (c, d)) -> ((a, c), (b, d))
        pair ((a, b), (c, d)) = ((a, c), (b, d))
    it "are sub-polylines of the correct polyline" $ property $
      traverse_ (`shouldSatisfy` uncurry isInfixOf) .
      biLiftC' (<>) (\(pl, pls) -> fmap (, pl) pls) .
      ( pair .
        bimap extractPls (unzip . fmap extractPls) .
        mkSnd shrinkTest
        :: BoundryInteriorPlIntersect -> ((Polyline, [Polyline]),(Polyline,[Polyline])) )

  describe "shrinkPolylineLeavePoint" $ do
    it "not empty" $
      forAll (suchThat 
        (getPolylineAndContainedPoint <$> arbitrary)
        (not . (\(pl, p) -> firstEdge pl `contains` p && lastEdge pl `contains` p)) ) $
      any (`lengthGT` 0) .
      uncurry shrinkPolylineLeavePoint .
      swap

    describe "proper subsequence" $ do
      it "order and sequence is maintained" $ property $
        (\(pl, p) -> 
          traverse_ (`shouldSatisfy` (`isInfixOf` pl)) $
          shrinkPolylineLeavePoint p pl ) .
        getPolylineAndContainedPoint

      it "polyline not included in shrinks" $ property $
        liftA2 (not .: elem)
          fst
          (uncurry $ flip shrinkPolylineLeavePoint) .
        getPolylineAndContainedPoint
      
    it "order shortest to longest" $ property $
      flip shouldSatisfy isAscending .
      getList .
      fmap length .
      uncurry shrinkPolylineLeavePoint .
      swap .
      getPolylineAndContainedPoint

    describe "end point" $ do
      let test :: (Polyline -> Point) -> String -> SpecM () ()
          test gp msg = it msg $ property $
            liftA2 ($)
              ( (\p -> traverse_ (\pl ->
                  assertEqual
                    (show pl <> " does not have " <> show p <> " as its " <> msg <> " point")
                    (gp pl)
                    p )) .
                gp )
              (liftA2 shrinkPolylineLeavePoint gp id)

      test tp "left"
      test hp "right"

    let testShrinkList :: (Polyline -> Gen Point) -> Int -> (List Polyline -> Bool) -> Property
        testShrinkList genPt minPts (f :: List Polyline -> Bool) = 
          forAll (suchThat
            (Polyline . AtLeast <$> (flip uniqueValues empty =<< arbitrary))
            (`lengthGT` minPts) ) $
          liftA2 forAll
            genPt
            (f .: flip shrinkPolylineLeavePoint)

    describe "interior point" $ do
      let minPts = 3
      let genPt = elements . fmap getInterior . interiorPoints
      let test = testShrinkList genPt minPts

      it "always has at least 3 points" $
        test $ all (`lengthGTEq` minPts)

      it "shrinks to 3 points" $
        test $ any (`lengthEq` minPts)

      it "empty shrink if shrinking polyline with only 3 points" $
        forAll (Polyline . AtLeast <$> uniqueValues minPts empty) $
          null .
          liftA2 shrinkPolylineLeavePoint
            secondHead
            id

    describe "interior contained point" $ do
      let minPts = 2
      let genPt = interiorPointOnSegment <=< elements . edges

      it "shrinks to 2 points" $
        testShrinkList genPt minPts $ any (`lengthEq` minPts)

      it "empty shrink if shrinking polyline with only 2 points" $ property $
        null .
        uncurry shrinkPolylineLeavePoint .
        swap .
        first (fromJust . toPolyline . thps') .
        getSegmPointCoincide

  describe "SharedEpVectorColinearPls" $ do
    it "shares head point and tail point" $ property $
      uncurry shouldBe .
      bimap hp tp .
      fst .
      getSharedEpVectorColinearPls

    it "last line and first line are collinear" $ property $
      collinearV' .
      bimap lastEdge firstEdge .
      fst .
      getSharedEpVectorColinearPls

  describe "SharedEpRevColinearPls" $ do
    it "shares head point and tail point" $ property $
      uncurry shouldBe .
      bimap hp tp .
      fst .
      getSharedEpRevColinearPls

    it "last line and reversed first line are collinear" $ property $
      collinearV' .
      bimap lastEdge (PLS.reverse . firstEdge) .
      fst .
      getSharedEpRevColinearPls

  describe "SharedEpNonColinearPls" $ do
    it "shares head point and tail point" $ property $
      uncurry shouldBe .
      bimap hp tp .
      fst .
      getSharedEpNonColinearPls

    it "last and first lines are not collinear" $ property $
      not .
      collinearV' .
      bimap lastEdge firstEdge .
      fst .
      getSharedEpNonColinearPls

