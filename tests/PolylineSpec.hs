{-# LANGUAGE FlexibleContexts
           , ScopedTypeVariables
           , PartialTypeSignatures
           , ViewPatterns
           , LambdaCase
           , TypeApplications
           , NoImplicitPrelude
           #-}

-- module Main where
module PolylineSpec where

import Polyline
import PolylineArbitrary

import GeometryClasses
  ( SelfIntersecting(selfIntersecting)
  , Contain(contains, contains', interiorContains, interiorContains')
  )
import Dimension (fromScalar, abs)
import Angle
  ( Ra
  , getHalfCircleAngle
  , PiPi(PiPi, getPiPiAngle)
  , normalize
  )
import Vector (_vec, vecAngle, toNZV, parallel)
import PLS hiding (reverse, rotate)
import SegmArbitrary (NoSharedEndpoint(getNoSharedEndpoint))
import Ratio (Ratio(Ratio))

import IsClose (IsClose(isClose, isClose', isCloseAR))
import Normalized (getNormalizedLU)

import Utilities
import Utilities.Arbitrary

import Unified
import Unified.Arbitrary (localUnique, localUniqueRanges)
import Unified.AtLeast
import Unified.Collections.List (List(List))
import Unified.Collections.Set (Set)
import Unified.QuickCheck
import Unified.Utilities (count, isInfixOf)
import Unified.Utilities.List (zipL, toPairsL, tupleCombo)

import Data.Composition ((.:))
import Control.Monad
import Control.Applicative (liftA2)
import Data.Traversable (sequenceA)
import Data.Bifunctor (bimap, first, second)
import Data.Bitraversable (bisequenceA)
import Data.Foldable (elem, all, traverse_, for_, toList)
import Data.Maybe (fromJust, isJust, isNothing)
import Data.Semigroup ((<>))
import Data.Bool
import GHC.Float
import Data.Maybe
import Data.Foldable (Foldable)
import GHC.IO
import Data.Function
import Data.Tuple hiding (fst)
import Data.String (String)
import Data.Eq
import Data.Ord
import Data.Functor
import GHC.Num hiding (abs)
import Test.Hspec hiding (parallel)
import Test.QuickCheck

import qualified Data.List as L

import Prelude (error)

main :: IO ()
main = hspec spec

ps2pl :: Foldable t => t Point -> Polyline
ps2pl = Polyline . fromJust . fromFoldableMay

tps2pl :: ([Point], [Point]) -> (Polyline, Polyline)
tps2pl = bimap' ps2pl

spec :: Spec
spec = do
  describe "ToPolyline" $ do
    describe "[Point]" $ do
      for_
        [ ( [pt 0 0, pt 1 1]
          , Just [pt 0 0, pt 1 1]
          , "two different points" )
        , ( [pt 0 0, pt 0 0]
          , Nothing
          , "two same points" )
        , ( [pt 0 0, pt 1 1, pt 2 2]
          , Just [pt 0 0, pt 1 1, pt 2 2]
          , "three different points" )
        , ( [pt 0 0, pt 0 0, pt 1 1]
          , Just [pt 0 0, pt 1 1]
          , "three points with two same points" )
        , ( [pt 0 0, pt 0 0, pt 0 0]
          , Nothing
          , "three same points" )
        , ( [pt 0 0, pt 2 0, pt 1 0]
          , Just [pt 0 0, pt 2 0, pt 1 0]
          , "double back half")
        , ( [pt 0 0, pt 1 0, pt 0 0]
          , Just [pt 0 0, pt 1 0, pt 0 0]
          , "double back half") ] $
        \( List -> ps, fmap List -> result, testName) -> it testName $ do
           toPolyline ps `shouldBe` fmap ps2pl result
           toPolyline (reverse ps) `shouldBe` fmap (ps2pl . reverse) result

      it "too few points" $ do
        forAll (flip localUnique empty =<< choose (0, 1)) $
          isNothing . (toPolyline :: List Point -> Maybe Polyline)

      it "removes duplicates" $
        forAll (localUniqueRanges =<< minChoose 2) $
          uncurry shouldBe . swap .
          bimap' (toPolyline :: List Point -> Maybe Polyline)

    describe "[Segm]" $ do
      for_
        [ ([pt 0 0, pt 1 1], "2 points")
        , ([pt 0 0, pt 1 1, pt 0 2], "3 points")
        , ([pt 0 0, pt 1 1, pt 0 2, pt 3 4], "4 points")]
        $  \( ps2pl -> pl
            , testName
            ) -> it testName $
              toPolyline (edges pl) `shouldBe` Just pl

      it "unmatched interior points" $ do
        (toPolyline $ fmap nS''' [(pt 0 0, pt 1 1), (pt 2 2, pt 3 2)])
        `shouldBe` Nothing

      it "Nothing if disjoint segments" $ property $
        isNothing .
        toPolyline .
        (\(x, y) -> [x, y] :: [Segm]) .
        getNoSharedEndpoint

      it "Just if joined segments" $ property $
        isJust .
        toPolyline .
        (edges :: Polyline -> NonEmpty List Segm)

      it "has correct points" $ do
        property $ \(pl :: Polyline) -> let es = edges pl in
          length pl == length es + 1 &&
          fromFoldableMay pl == Just ((tp $ fromJust $ head es) <| fmap hp es)

  describe "Contain Point" $ do
    let singlePolyline = [pt 0 0, pt 2 2]
    let doublePolyline = [pt 0 0, pt 2 2, pt 4 2]
    let multiPolyline  = [pt 0 0, pt 2 2, pt 4 2, pt 6 3]

    let test func
             (ps2pl -> pl)
             (p, result, testName) =
          it testName $ do
            pl `func` p `shouldBe` result
            reverse pl `func` p `shouldBe` result

    describe "contains" $ do
      let singleTests = [
              (pt 1 1, True, "interior")
            , (pt 0 0, True, "endpoint")
            , (pt 1 0, False, "disjoint")
            ]
      let doubleTests = [
              (pt 2 2, True, "interior point")
            ]
      let multiTests = [
              (pt 3 2, True, "mid section")
            ]

      describe "single" $ do
        traverse_ (test contains singlePolyline)
           singleTests

      describe "double" $ do
        traverse_ (test contains doublePolyline)
           (singleTests <> doubleTests)

      describe "multi" $ do
        traverse_ (test contains multiPolyline)
           (singleTests <> doubleTests <> multiTests)

      it "does contain" $ do
        property $ contains' . getPointOnPolylineSegm

      it "does not contain" $ do
        property $ not . contains' . getDisjointPolylineAndPoint

    describe "interiorContains" $ do
      let singleTests = [
              (pt 1 1, True, "interior")
            , (pt 0 0, False, "endpoint")
            , (pt 1 0, False, "disjoint")
            ]
      let doubleTests = [
              (pt 2 2, True, "interior point")
            ]
      let multiTests = [
              (pt 3 2, True, "mid section")
            ]

      describe "single" $ do
        traverse_ (test interiorContains singlePolyline)
           singleTests

      describe "double" $ do
        traverse_ (test interiorContains doublePolyline)
           (singleTests <> doubleTests)

      describe "multi" $ do
        traverse_ (test interiorContains multiPolyline)
           (singleTests <> doubleTests <> multiTests)

      it "does contain" $ do
        property $ interiorContains' . getPolylineAndInteriorPoint

      it "does not contain endpoints" $ property $
        not . interiorContains' . getPolylineAndUniqueEndpoint

      it "does not contain disjoint point" $ do
        property $ not . interiorContains' . getDisjointPolylineAndPoint

      it "does contain connector points" $ do
        property $ interiorContains' . getPolylineAndConnectorPoint

  describe "Intersect" $ do
    describe "intersects" $ do
      it "does intersect" $ property $
        intersects' . fst . getPolylineIntersect

      it "no intersect" $ do
        property $ not . intersects' . getDisjointPolylines

    describe "interiorIntersect" $ do
      it "no intersect" $ do
        property $ not . interiorIntersect' . getDisjointPolylines

      it "boundry only intersect no interior intersect" $ property $
        not .
        interiorIntersect' .
        fst .
        getBoundryOnlyPolylineIntersect

      it "does have interior intersect" $ property $
        interiorIntersect' . fst . getInteriorPolylineIntersect

    describe "ininIntersect" $ do
      it "no intersect" $ do
        property $ not . ininIntersect' . getDisjointPolylines

      it "boundry only intersect no interior intersect" $ property $
        not .
        ininIntersect' .
        fst .
        getBoundryOnlyPolylineIntersect

      it "does have interior intersect" $ property $
        ininIntersect' . fst . getInteriorPolylineIntersect

      describe "interior boundry" $ for_
        ( [ ( [pt 0 0, pt 2 2]
            , [pt 1 1, pt 0 2]
            , "end segment" )
          , ( [pt 1 0, pt 0 0, pt 0 1]
            , [pt 0 0, pt 1 1]
            , "end segment interior endpoint" )
          , ( [pt 0 3, pt 0 2, pt 2 0, pt 3 0]
            , [pt 0 0, pt 1 1]
            , "interior segment" )
          , ( [pt 0 2, pt 0 1, pt 1 1, pt 1 0, pt 2 0]
            , [pt 0 0, pt 1 1]
            , "non end segment point" )
          , ( [pt 0 0, pt 1 1]
            , [pt 3 1, pt 3 0, pt 2 0, pt 0 2, pt 0 3, pt 1 3]
            , "interior segment that does not connect with end segment" ) ]
          :: [([Point], [Point], String)] )
        (\case (toPolyline -> Just pl0, toPolyline -> Just pl1, testName) -> it testName $
                  traverse_ ((`shouldBe` False) . ininIntersect')
                            (tupleCombo reverse (pl0, pl1))
               (_, _, testName) -> error ("Cannot create the Polylines for: " <> testName) )

  it "isClosed" $ do
    property $ isClosed . getClosedPolyline

  it "edges" $
    edges (ps2pl [pt 2 2, pt 4 4, pt 5 5])
    `shouldBe`
    fromJust (fromFoldableMay [nS'' (pt 2 2) (pt 4 4), nS'' (pt 4 4) (pt 5 5)])

  describe "interiorEdges" $ do
    it "empty for polylines with only one or two segments" $
      forAll (Polyline . AtLeast <$> vectorRange (NonNegative 2) (NonNegative 3)) $
        (`lengthEq` 0) .
        interiorEdges

    it "has two less edges than the polyline has" $
      forAll (Polyline . AtLeast <$> vectorGTEq (NonNegative 3)) $
        liftA2 shouldBe
          (length . interiorEdges)
          (subtract 3 . length)
      
    it "specific cases" $ do
      let ti = fmap Interior . fromFoldable
      interiorEdges (ps2pl [pt 0 0, pt 1 1]) `shouldBe` empty
      interiorEdges (ps2pl [pt 0 0, pt 1 1, pt 2 1]) `shouldBe` empty
      interiorEdges (ps2pl [pt 0 0, pt 1 1, pt 2 1, pt 3 3]) `shouldBe`
        ti [nS'' (pt 1 1) (pt 2 1)]
      interiorEdges (ps2pl [pt 0 0, pt 1 1, pt 2 1, pt 3 3, pt 3 2]) `shouldBe`
        ti [nS'' (pt 1 1) (pt 2 1), nS'' (pt 2 1) (pt 3 3)]

  it "firstEdge" $ property $
    \pl -> firstEdge pl == fromJust (f2p =<< bisequenceA (0 !! pl, 1 !! pl))

  it "lastEdge" $ property $
    \pl -> lastEdge pl == (End $ L.last $ toList $ edges pl)

  describe "endEdges" $ do
    it "always has length of 2" $ property $
      (`lengthEq` 2) . endEdges

    it "endEdges" $ property $
      maybeBool .
      \pl -> let l = length pl in
        (== endEdges pl) <$>
        ( sequenceA $
          fmap (f2p <=< bisequenceA) $
          fromFoldable [(0 !! pl, 1 !! pl), ((l - 2) !! pl, (l - 1) !! pl)] )

  it "interiorPoints" $ do
    let ti = fmap Interior . fromFoldable
    interiorPoints (ps2pl [pt 0 0, pt 1 1]) `shouldBe` empty
    interiorPoints (ps2pl [pt 0 0, pt 1 1, pt 2 1]) `shouldBe` ti [pt 1 1]
    interiorPoints (ps2pl [pt 0 0, pt 1 1, pt 2 1, pt 3 3]) `shouldBe` ti [pt 1 1, pt 2 1]

  describe "rotate" $ do
    it "points are concerved" $ do
      property $ \c a (pl :: Polyline) -> length (rotate c a pl) == length pl

    it "each point is rotated" $ do
      property $ \c (a :: Ra) (pl :: Polyline) ->
        all ( (\case Just b -> b
                     Nothing -> True ) .
              ( isCloseAR 1e-6 1e-6 (abs $ getPiPiAngle $ getNormalizedLU $ normalize $ PiPi a) .
                getHalfCircleAngle .
                getNormalizedLU .
                uncurry vecAngle <$<
                bisequenceA .
                bimap' (toNZV . (.<-. c)) ) )
            (zipL pl (rotate c a pl))

    it "same distance from center of rotation" $ do
      property $ \c a (pl :: Polyline) ->
        all (isClose' . bimap' (|--| c))
            (zipL pl (rotate c a pl))

  describe "selfIntersecting" $ do
    for_
      [ ([pt 0 0, pt 1 1, pt 0 0], True, "complete overlap")
      , ([pt 0 0, pt 1 0, pt 1 1], False, "triangle")
      , ([pt 1 0, pt 1 3, pt 0 2, pt 2 2], True, "4")
      , ([pt 0 0, pt 2 0, pt 2 1, pt 1 0, pt 0 1], True, "point intersect") ]
      $  \(ps2pl -> pl, result, testName) -> it testName $
          selfIntersecting pl `shouldBe` result

    it "not self intersecting" $ do
      property $ not . selfIntersecting . getNonSelfIntersectingPolyline

    it "is self intersecting" $ do
      property $ selfIntersecting . fst . getSelfIntersectingPolyline

  describe "DistanceBetween" $ do
    describe "Polyline Point" $ do
      for_
        [ ( [pt 0 0, pt 1 1]
          , pt 3 4
          , 13
          , "from endpoint" )
        , ( [pt 0 0, pt 1 1, pt 3 3, pt 4 4]
          , pt 3 1
          , 2
          , "mid-edge" )
        , ( [pt 0 0, pt 2 2, pt 4 4]
          , pt 3 1
          , 2
          , "inner-point" ) ]
        $  \( ps2pl -> pl
            , p
            , fromScalar -> result
            , testName
            ) -> it testName $ do
              pl |--| p `shouldBe` result
              (reverse pl) |--| p `shouldBe` result

      it "property" $ property $
        isClose' .
        first (uncurry (|--|)) .
        -- (\(pl, p, d) -> (pl |--| p) `isClose` d) .
        getPolylinePointDistance

    describe "Polyline Polyline" $ do
      for_
        [ ( ( [pt 0 0, pt 1 1]
            , [pt 2 2, pt 3 3] )
          , 2
          , "endpoints")
        , ( ( [pt 2 0, pt 0 2]
            , [pt 3 3, pt 4 4] )
          , 8
          , "endline to endpoint")
        , ( ( [pt 2 0, pt 1 1, pt 0 2]
            , [pt 4 4, pt 5 5] )
          , 18
          , "inner-point to endpoint")
        , ( ( [pt 0 1, pt 1 1, pt 1 0]
            , [pt 4 0, pt 0 4] )
          , 2
          , "inner-point to endline")
        , ( ( [pt 0 1, pt 1 1, pt 1 0]
            , [pt 5 0, pt 4 0, pt 0 4, pt 0 5] )
          , 2
          , "inner-point to inner-line") ]
        $  \( tps2pl -> pls
           , fromScalar -> result
           , testName
           ) -> it testName $
                for_ (tupleCombo reverse pls) $
                (`shouldBe` result) . uncurry (|--|)

      it "property" $ property $
        isClose' .
        first (uncurry (|--|)) .
        -- (\(pl0, pl1, d) -> pl0 |--| pl1 `isClose` d) .
        getPolylinePolylineDistance

    describe "AddNoise" $ do
      describe "arNoise" $ do
        it "same length" $ do
          property $ \a r pl -> forAll (arNoise a r pl :: Gen Polyline)
                                         ((== length pl) . length)

        it "not all equal" $ do
          property $ \(NonEqual (a, r)) pl -> do
            npl <- sample' $ (arNoise a r pl :: Gen Polyline)
            all (/= pl) npl `shouldBe` True

      describe "arNoiseUnbounded" $ do
        it "same length" $ do
          property $ \a r pl -> forAll (arNoiseUnbounded a r pl :: Gen Polyline)
                                         ((== length pl) . length)

        it "not all equal" $ do
          property $ \(NonEqual (a, r)) pl -> do
            npl <- sample' $ (arNoiseUnbounded a r pl :: Gen Polyline)
            all (/= pl) npl `shouldBe` True

  describe "IsClose" $ do
    it "different lengths are false" $ property $
      not . isClose' . fst . getMissingPointPolyline

    it "within range" $ do
      let test :: Positive Double -> Ratio -> Polyline -> Property
          test (Positive a) (Ratio r) pl0 = forAll (arNoise a r pl0) $
                           \pl1 -> isCloseAR a r pl0 pl1
      property test

    it "out of range" $ do
      let test :: Positive Double -> Ratio -> Polyline -> Property
          test (Positive a) (Ratio r) pl0 = forAll (arNoiseUnbounded a r pl0) $
                           \pl1 -> not $ isCloseAR a r pl0 pl1
      property test

  describe "maxDistance" $ do
    for_
      [ ( ( [pt 0 0, pt 1 0]
          , [pt 0 1, pt 1 1] )
        , 1
        , "horizontal parallel" )
      , ( ( [pt 0 0, pt 1 1, pt 2 0]
          , [pt 0 2, pt 2 2] )
        , 2
        , "notch close" )
      , ( ( [pt 0 1, pt 1 0, pt 2 1]
          , [pt 0 2, pt 2 2] )
        , 2
        , "notch far" ) ]
      $  \( tps2pl -> pls
          , result
          , testName
          ) -> it testName $
            traverse_ ((`shouldBe` fromScalar result) . maxDistance')
                      (tupleCombo reverse pls)

    it "property" $ property $
      uncurry shouldBe .
      first (uncurry (|--|)) .
      -- (\(pl0, pl1, d) -> pl0 |--| pl1 == d) .
      getPolylinePolylineDistance

  describe "combine" $ do
    for_
      [ ( ( [pt 0 1, pt 0 0, pt 1 1]
          , [pt 1 1, pt 2 2, pt 2 0] )
        , Just [pt 0 1, pt 0 0, pt 2 2, pt 2 0]
        , "straight segments" )
      , ( ( [pt 0 0, pt 0 1]
          , [pt 0 1, pt 1 1] )
        , Just [pt 0 0, pt 0 1, pt 1 1]
        , "same endpoint" )
      , ( ( [pt 0 0, pt 1 1]
          , [pt 1 0, pt 2 1] )
        , Nothing
        , "disjoint" )
      , ( ( [pt 0 0, pt 2 0]
          , [pt 2 0, pt 1 0] )
        , Just [pt 0 0, pt 2 0, pt 1 0]
        , "double back half" )
      ]
      $  \( tps2pl -> (pl0, pl1)
          , rps
          , testName
          ) -> it testName $ do
            let result = Polyline <$> (fromFoldableMay =<< rps)
            pl0 `combine` pl1 `shouldBe` result
            pl1 `combine` pl0 `shouldBe` result

    let extractPoints = getAtLeast . getPolyline
    it "if two shared endpoints polyline order is not changed" $
      forAll 
        ( suchThat
            (getPolylinesTwoSharedEndpoints <$> arbitrary)
            (biLift' (not .: parallel) lastEdge firstEdge) ) $
        liftA2 shouldBe
          combine'
          ( Polyline . AtLeast <$<
            biLift' (<>)
              (Just . extractPoints)
              (tail . extractPoints) )

    it "if joining segments are vector collinear the segments are merged into one" $ property $
      liftA2 shouldBe
        combine'
        ( Polyline . AtLeast .
          uncurry (<>) <$<
          bisequenceA . bimap init tail .
          bimap' (getAtLeast . getPolyline) ) .
      fst .
      getSharedEpVectorColinearPls

    describe "if joining segments are collinear but not vector collinear the segments are not merged" $ do
      it "still has connecting point" $ property $
        (`shouldBe` Just 1) .
        (\(pls, p) -> count p <$> combine' pls) .
        getSharedEpRevColinearPls

      it "still has all polyline points" $ property $
        liftA2 shouldBe
          (fromEFoldable @Set <$< combine')
          (Just . uncurry (<>) . bimap' fromEFoldable) .
        fst .
        getSharedEpRevColinearPls

    it "joins if polylines have a shared endpoint" $ property $
      isJust .
      combine' .
      fst .
      getSharedPolylineEndpoint

    it "Nothing if polylines do not share an endpoint" $ property $
      isNothing .
      combine' . 
      getPolylinesWithNoSharedEndpoints
      

  describe "closePl" $ do
    it "already closed" $ property $
      liftA2 shouldBe closePl id .
      getClosedPolyline

    describe "not closed yet" $ do
      it "length increases by one" $ property $
        liftA2 shouldBe
          (length . closePl)
          ((+ 1) . length) .
        getOpenPolyline

      it "endpoints are identical" $ property $
        liftA2 shouldBe tp hp .
        closePl .
        getOpenPolyline

      it "original polyline remains in closed polyline" $ property $
        liftA2 isInfixOf id closePl .
        getOpenPolyline

  describe "translate" $ do
    it "specific case" $ do
      translate (_vec 1 2) <$> toPolyline @[Point] [pt 0 0, pt 1 1, pt 0 1] `shouldBe`
        toPolyline @[Point] [pt 1 2, pt 2 3, pt 1 3]

    it "same number of points" $ do
      property $ \(pl :: Polyline) v -> length (translate v pl) == length pl

    it "every point translated by vector" $ do
      property $ \(pl :: Polyline) v -> all (isClose v . uncurry (.->.)) $
                                            zipL pl (translate v pl)

  describe "Polyline" $ do
    it "2 or more points" $ do
      property ((>= 2) . length :: Polyline -> Bool)

    it "no repeat points" $ do
      property $ (all (uncurry (/=)) . toPairsL :: Polyline -> Bool)
