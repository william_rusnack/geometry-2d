module RangeSpec where

import Range
  ( Range(Range)
  , nRange
  , overlaps
  , interiorOverlap
  )

import GeometryClasses (Contain(contains))
import Dimension
  ( Absolute(Absolute)
  , Relative(Relative)
  , (.-.)
  , (=+.)
  , (=*~)
  )

import Ratio (Ratio(Ratio), NonRatio(NonRatio))

import Data.Foldable (traverse_)
import Test.Hspec
import Test.QuickCheck


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "Range" $ do
    it "nRange" $ do
      let l = Absolute 1
      let u = Absolute 2
      nRange u l `shouldBe` Range l u
      nRange l u `shouldBe` Range l u

    describe "Contains" $ do
      describe "contains" $ do
        it "does contain" $ property $
          \rng@(Range l u) (Ratio r) ->
            rng `contains` ((u .-. l :: Relative ()) =*~ r =+. (l :: Absolute ()))

        it "does not contain" $ property $
          \rng@(Range l u) (NonRatio r) -> let
            s = if s' == Relative 0
                then Relative 1
                else s'
            s' = u .-. l
            in not $ rng `contains` ((s :: Relative ()) =*~ r =+. l :: Absolute ())

    describe "Intersect" $ do
      let test func (r0', r1', result, testName) = it testName $ do
            let abs' (x, y) = (Absolute x, Absolute y)
            let r0 = uncurry nRange $ abs' r0'
            let r1 = uncurry nRange $ abs' r1'
            func r0 r1 `shouldBe` result
            func r1 r0 `shouldBe` result

      describe "overlaps" $ do
        traverse_ (test overlaps)
          [ ((0, 2), (1, 3), True,  "partial partial")
          , ((0, 3), (1, 2), True,  "partial total")
          , ((0, 1), (1, 2), True,  "end point 0")
          , ((1, 2), (2, 3), True,  "end point 1")
          , ((0, 1), (2, 3), False, "none") ]

      describe "interiorOverlap" $ do
        traverse_ (test interiorOverlap)
          [ ((0, 1), (0, 1), True,  "itself")
          , ((0, 2), (1, 3), True,  "partial partial")
          , ((0, 3), (1, 2), True,  "partial total")
          , ((0, 1), (1, 2), False, "end point 0")
          , ((1, 2), (2, 3), False, "end point 1")
          , ((0, 1), (2, 3), False, "none") ]
