module RatioSpec where

import Ratio

import Control.Applicative (liftA2)
import Test.Hspec
import Test.QuickCheck


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  it "Ratio" $ do
    property (liftA2 (&&) (>= 0) (<= 1) :: Ratio -> Bool)

  it "NonRatio" $ do
    property (liftA2 (||) (< (-1)) (> 1) :: NonRatio -> Bool)

  it "NZRatio" $ do
    property (liftA2 (&&) (> 0) (< 1) :: NZRatio -> Bool)

  it "ZRatio" $ do
    property (liftA2 (&&) (>= 0) (< 1) :: ZRatio -> Bool)

