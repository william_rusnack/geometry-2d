module RectangleSpec where

import Rectangle
  ( Rect2P(Rect2P), RectVec(RectVec)
  , mbb
  )

import Dimension(fromScalar)
import Area (FindArea(area))
import Vector(_vec)
import PLS(pt, intersects)

import Data.Foldable (traverse_)
import Test.Hspec


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "RectVec" $ do
    it "area" $ do
      let r = RectVec (_vec (-2) 6)
      area r `shouldBe` fromScalar 12

  describe "Rect2P" $ do
    describe "intersects" $ do
      let test (rectangle, result, testName) = it testName $ do
              let rectangle2 = Rect2P (pt 1 1, pt 4 4)
              rectangle `intersects` rectangle2 `shouldBe` result
              rectangle2 `intersects` rectangle `shouldBe` result
      traverse_ test [
          (Rect2P (pt 0 0, pt 1 1), True, "corner intesect")
        , (Rect2P (pt 5 5, pt 3 4), True, "edge intersect")
        , (Rect2P (pt 2 0, pt 3 2), True, "area intersect")
        , (Rect2P (pt 2 2, pt 3 3), True, "contained intersect")
        , (Rect2P (pt 5 5, pt 6 6), False, "no intersect")
        ]

    it "area" $ do
      let (p0, p1) = (pt 1 2, pt 4 8)
      let refArea = fromScalar 18
      area (Rect2P (p0, p1)) `shouldBe` refArea
      area (Rect2P (p1, p0)) `shouldBe` refArea

  it "mbb (minimum bounding box)" $ do
    mbb [pt 1 1, pt 2 2, pt 3 1, pt 0 1] `shouldBe` Just (Rect2P (pt 0 1, pt 3 2))

