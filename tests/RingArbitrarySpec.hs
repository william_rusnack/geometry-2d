module RingArbitrarySpec where

import Test.Hspec

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  it "all tests commented out" $ True `shouldBe` False
--   it "genRing" $ do
--     forAll (minChoose 2) $ \l -> forAll (genRing l) $ \r -> length r == l

--   describe "CCWClosed" $ do
--     it "first and last points are the same" $ do
--       property $ ( liftA2 (==) head last .
--                    fst .
--                    getCCWClosed
--                    :: CCWClosed [] -> Bool )

--     it "next point always >= angle from the previous point" $ do
--       let correctRot :: ([Point], Point) -> Bool
--           correctRot (ps, cp) =
--             all ( flip elem ([Leftward, Forward, Stopped] :: [Ward]) .
--                   uncurry toward ) $
--                 toPairs $ map (.<-. cp) ps

--       let centerPoints :: [Point] -> Point -> [Point]
--           centerPoints ps p = p <| (avgP <| ps)
--             where avgP :: Point
--                   avgP = (.+->) p $
--                          sconcat $
--                          fromJust $ fromList $
--                          mapL (.<-. p) ps

--       property $ any correctRot .
--                  (\(ps, p) -> zipL (repeat ps) (centerPoints ps p)) .
--                  (getCCWClosed :: CCWClosed [] -> ([Point], Point))

--   it "SelfIntersectingRing" $ do
--     property $ (>= 2) .
--                length .
--                filter id .
--                uncurry (flip map) .
--                bimap edges
--                      (\case Left  p -> (`contains` p)
--                             Right s -> (`contains` s) ) .
--                getSelfIntersectingRing

--   describe "NonSelfIntersectingRing" $ do
--     it "adjecent segments do not have an interior intersect" $ do
--       property $ not .
--                  any interiorIntersect' .
--                  toPairs .
--                  fromJust . (\es -> (<| es) <$> last es) .
--                  edges .
--                  getNonSelfIntersectingRing

--     it "tail edges have no intersects" $ do
--       property $ not .
--                  any intersects' .
--                  secondCombo .
--                  fromJust . tail .
--                  edges .
--                  getNonSelfIntersectingRing

--     it "first edge has no intersects" $ do
--       property $ not .
--                  (\es -> any (intersects $ fromJust $ head es) $
--                          drop 2 $
--                          fromJust $ init es ) .
--                  edges .
--                  getNonSelfIntersectingRing

--   describe "DisjointRings" $ do
--     it "has at least two points" $ do
--       property $ uncurry (&&) .
--                  bimap' ((>= 2) . length) .
--                  getDisjointRings


--     it "no intersects between rings" $ do
--       property $ not .
--                  any intersects' .
--                  uncurry allCombo'' .
--                  bimap' edges .
--                  getDisjointRings
