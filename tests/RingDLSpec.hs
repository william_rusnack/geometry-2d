{-# LANGUAGE FlexibleInstances
           , PartialTypeSignatures
           , ViewPatterns
           , ScopedTypeVariables #-}

-- module Main where
module RingDLSpec where

import RingDL
  ( RingDL, RingDL_(RingDL)
  , linkRings, link2Rings, combineRings
  , r2rdl, rdl2ps
  , edges
  )

import GeometryClasses
  ( SelfIntersecting(selfIntersecting)
  )
import PLS
  ( Point
  , pt
  , crossIntersect
  , intersects
  )
import DSegm (NumeralPrefix(Uni, Bi, Pre, Pst, PPB), nDS'')
import Ring
  ( Ring
  , IsCCW(isCCW)
  , toRing
  )

import Utilities (bimap', (<$<))

import Unified
  ( last
  , (<|)
  , fromFoldable
  , fromEFoldable
  , fromFoldableMay
  , empty
  )
import Unified.Utilities
  ( rotations
  , (<$|)
  )
import Unified.Collections.List (List(List))
import Unified.Utilities.List
  ( toPairsL
  , zipL
  )
import Unified.Collections.Set (Set)

import Data.Foldable (traverse_, for_, toList)
import Data.Maybe (fromJust)
import Control.Monad ((<=<))

import Test.Hspec (hspec, Spec, describe, it, shouldBe, shouldNotBe)

import Prelude hiding (last, map, zip)


main :: IO ()
main = hspec spec


class QuickRingDL a where
  quickRingDL :: a -> RingDL

instance Foldable t => QuickRingDL (t (NumeralPrefix, Point)) where
  quickRingDL = fromJust .
                ( RingDL <$<
                  fromFoldableMay <=<
                  fmap (\((np, p0), (_, p1)) -> nDS'' np p0 p1) .
                  toPairsL <$<
                  (\ps -> last ps <$| ps) ) .
                (fromFoldable :: t a -> List a)

instance QuickRingDL [Point] where
  quickRingDL = quickRingDL . List

instance QuickRingDL (List Point) where
  quickRingDL = r2rdl . fromJust . toRing . (\ps -> (fromJust $ last ps) <| ps)

toRing' :: forall t. Foldable t => t Point -> Ring
toRing' = fromJust .
          ( toRing <=<
            (\ps -> last ps <$| ps) ) .
          (fromFoldable :: t a -> List a)

spec :: Spec
spec = do
  it "r2rdl" $ do
    r2rdl (toRing' [pt 1 1, pt 2 2, pt 1 2]) `shouldBe`
      ( RingDL $ fromJust $ fromFoldableMay
        [ nDS'' Uni (pt 1 2) (pt 1 1)
        , nDS'' Uni (pt 1 1) (pt 2 2)
        , nDS'' Uni (pt 2 2) (pt 1 2) ] )

  describe "intersects" $ do
    let test (ps0, ps1, result, testName) = it testName $ do
          let [r0, r1] = fmap (\ps -> r2rdl $ toRing' ps) [ps0, ps1]
          r0 `intersects` r1 `shouldBe` result

    traverse_ test [
        ( [pt 0 0, pt 2 0, pt 2 3]
        , [pt 1 1, pt 3 1, pt 3 2]
        , True
        , "Triangle ring intersect")
      ]


  describe "selfIntersecting" $ do
    for_
      [ ( [(Uni, pt 0 0), (Uni, pt 1 0), (Uni, pt 1 1)]
        , False
        , "△")
      , ( [(Uni, pt 0 0), (Uni, pt 1 0), (Uni, pt 0 1), (Uni, pt 1 1)]
        , True
        , "last line intersect")
      , ( [(Uni, pt 1 0), (Uni, pt 0 1), (Uni, pt 1 1), (Uni, pt 0 0)]
        , True
        , "first line intersect")
      , ( [(Uni, pt 0 0), (Uni, pt 1 1), (Uni, pt 0 1), (Uni, pt 1 0)]
        , True
        , "hourglass")
      , ( --   ▷-◁   what the shape looks like. Bi section is the line connecting the triangles
          [ (Uni, pt 0 2), (Pre, pt 0 0), (Bi, pt 1 1), (Pst, pt 2 1)
          , (Uni, pt 3 0), (Pre, pt 3 2), (Bi, pt 2 1), (Pst, pt 1 1)
          ]
        , False
        , "▷-◁")
      , ( [ (Pst, pt 1 0), (Uni, pt 0 0), (Uni, pt 0 1), (Pre, pt 1 1)
          , (Bi, pt 1 0)
          , (Pst, pt 2 0), (Uni, pt 3 0), (Uni, pt 3 1), (Pre, pt 2 1)
          , (Bi, pt 2 0)
          ]
        , False
        , "□_□")
      , ( [ (Pre,  pt 2 2)
          , (Bi,     pt 1 2)
          , (Pst, pt 0 3)
          , (Uni,    pt 0 0)
          , (Uni,    pt 3 0)
          , (Pre,  pt 3 3)
          , (Bi,     pt 0 3)
          , (Pst, pt 1 2)
          , (Uni,    pt 1 1)
          , (Uni,    pt 2 1)
          ]
        , False
        , "square in square"
        )
      , ( [ (Pre, pt 0 0)
          , (Bi, pt 2 0)
          , (PPB, pt 3 0)
          , (Bi, pt 5 0)
          , (Pst, pt 6 0)
          , (Uni, pt 8 0)
          , (Pre, pt 7 1)
          , (Bi, pt 6 0)
          , (Pst, pt 5 0)
          , (Pre, pt 4 1)
          , (Bi, pt 3 0)
          , (Pst, pt 2 0)
          , (Uni, pt 1 1)
          ]
        , False
        , "△_△_△ (double connection)") ]
      $  \( List -> ps
          , result
          , testName) -> it testName $ do
            let ps' = fromJust $ last ps <$| ps
            let ring = RingDL $
                       fromJust $ fromFoldableMay $
                       fmap (\((np, p0), (_, p1)) -> nDS'' np p0 p1) $
                       zipL ps' ps

            traverse_ (\r -> selfIntersecting r `shouldBe` result) (rotations ring :: List RingDL)

  describe "linkRings" $ do
    for_
      [ ( [ [pt 0 0, pt 1 0, pt 0 1]
          ]
        , "No interiors")
      , ( [ [pt 0 0, pt 3 0, pt 3 3, pt 0 3]
          , [pt 1 1, pt 2 1, pt 2 2, pt 1 2]
          ]
        , "Square in Square")
      , ( [ [pt 0 0, pt 3 0, pt 3 3, pt 0 3]
          , [pt 1 1, pt 2 1, pt 2 2]
          ]
        , "Triangle in Square")
      , ( -- img ref: ../imgs/hard-to-get-square.png
          [ [pt 0 0, pt 7 0, pt 7 7, pt 0 7]
          , [pt 3 3, pt 4 3, pt 4 4, pt 3 4]
          , [pt 1 1, pt 6 1, pt 2 2, pt 2 5, pt 6 6, pt 1 6]
          ]
        , "hard to get square")
      , ( [ [pt 0 0, pt 7 0, pt 0 7]
          , [pt 1 1, pt 2 1, pt 2 2]
          , [pt 3 1, pt 4 1, pt 4 2]
          , [pt 1 3, pt 2 3, pt 2 4] ]
        , "quad triangle")
      , ( -- Reference: ../imgs/house-with-6-triangle-windows.png
          [ [pt 0 0, pt 7 0, pt 7 4, pt 4 9, pt 0 4]
          , [pt 1 1, pt 2 1, pt 2 2]
          , [pt 3 1, pt 4 1, pt 4 2]
          , [pt 5 1, pt 6 1, pt 6 2]
          , [pt 1 3, pt 2 3, pt 2 4]
          , [pt 3 3, pt 4 3, pt 4 4]
          , [pt 5 3, pt 6 3, pt 6 4] ]
        , "house with 6 triangle windows" ) ]
      $  \(pss, testName) -> it testName $ do
            let rs = fmap (quickRingDL) pss :: [RingDL]

            let mr = linkRings rs
            mr `shouldNotBe` Nothing
            let Just r = mr
            let es = toList $ edges r

            -- no self intersects
            any (uncurry crossIntersect)
                [(e0, e1) | e0 <- es, e1 <- es] -- needs have pairs of the same edge
              `shouldBe` False

            -- correct number of line segments
            length es `shouldBe`
              sum (fmap (length . edges) rs) +
              2 * (length pss - 1)

            -- contains same points
            fromEFoldable (concat pss) `shouldBe` (fromEFoldable (rdl2ps r) :: Set Point)

            isCCW r `shouldBe` True

    it "Nothing because of intersection" $ do
      let rs = fmap quickRingDL
            [ [pt 0 0, pt 5 0, pt 5 5, pt 0 5]
            , [pt 2 2, pt 4 2, pt 4 4, pt 2 4]
            , [pt 1 1, pt 3 1, pt 3 3, pt 1 3]
            ]
      linkRings rs `shouldBe` Nothing

    it "CW Ring" $ do
      let ps0 = reverse [pt 0 0, pt 4 0, pt 0 4]
      let ps1 = reverse [pt 1 1, pt 2 1, pt 1 2]
      let r0 = r2rdl $ toRing' ps0
      let r1 = r2rdl $ toRing' ps1
      linkRings [r0, r1] `shouldNotBe` Nothing


  describe "link2Rings" $ do
    for_
      [ ( ( [pt 0 0, pt 4 0, pt 0 4]
          , [pt 1 1, pt 2 1, pt 1 2] )
        , []
        , 9
        , "triangle in triangle")
      , ( ( [pt 0 0, pt 3 0, pt 3 3, pt 0 3]
          , [pt 1 1, pt 2 1, pt 1 2] )
        , []
        , 9
        , "triangle in square")
      , ( ( [pt 2 2, pt 3 2, pt 2 3]
          , [pt 0 0, pt 6 0, pt 0 6] )
        , [ [pt 1 1, pt 2 1, pt 1 2] ]
        , 6
        , "blocking triangle"
        )
      , ( ( [pt 0 0, pt 5 0, pt 5 5, pt 5 0]
          , [pt 2 2, pt 3 2, pt 3 3, pt 2 3] )
        , [ [pt 1 1, pt 4 1, pt 4 4, pt 1 4] ]
        , 0
        , "all blocked")
      , ( ( [pt 0 0, pt 4 0, pt 0 4]
          , [pt 1 1, pt 2 1, pt 1 2] )
        , [ [pt 5 0, pt 6 0, pt 6 1]
          , [pt 5 0, pt 6 0, pt 6 1] ]
        , 9
        , "triangle in triangle with multi non-blocking")
      , ( ( [pt 0 0, pt 7 0, pt 0 7]
          , [pt 1 1, pt 2 1, pt 2 2] )
        , [ [pt 3 1, pt 4 1, pt 4 2]
          , [pt 1 3, pt 2 3, pt 2 4] ]
        , 5
        , "quad triangle") ]
      $  \( bimap' quickRingDL -> rs
          , fromFoldable -> ops :: List [Point]
          , count
          , testName
          ) -> it testName $ do
            let lrs = uncurry link2Rings rs (fmap quickRingDL ops)

            length lrs `shouldBe` count
            traverse_ (\r -> selfIntersecting r `shouldBe` False) lrs

    it "2 combined already" $ do
      let combined = quickRingDL [ (Pre, pt 2 2)
                                 , (Bi,  pt 1 1)
                                 , (Pst, pt 0 7)
                                 , (Uni, pt 0 0)
                                 , (Pre, pt 7 0)
                                 , (Bi,  pt 0 7)
                                 , (Pst, pt 1 1)
                                 , (Uni, pt 2 1) ]
      let triangle = r2rdl $ toRing' [pt 4.0 2.0,pt 3.0 1.0,pt 4.0 1.0]

      let rs = link2Rings combined triangle (empty :: List RingDL)
      length rs > 0 `shouldBe` True


  describe "combineRings" $ do
    let test (r0, r1, result, revResult, testName) = it testName $ do
          let dl0 = quickRingDL r0
          let dl1 = quickRingDL r1
          dl0 `combineRings` dl1 `shouldBe` quickRingDL result
          dl1 `combineRings` dl0 `shouldBe` quickRingDL revResult

    traverse_ test
      [ ( [ (Uni, pt 2 0)
          , (Uni, pt 1 1)
          , (Uni, pt 0 0)
          ]
        , [ (Uni, pt 3 0)
          , (Uni, pt 5 0)
          , (Uni, pt 4 1)
          ]
        , [ (Bi,  pt 2 0)
          , (Pst, pt 3 0)
          , (Uni, pt 5 0)
          , (Pre, pt 4 1)
          , (Bi,  pt 3 0)
          , (Pst, pt 2 0)
          , (Uni, pt 1 1)
          , (Pre, pt 0 0)
          ]
        , [ (Bi,  pt 3 0)
          , (Pst, pt 2 0)
          , (Uni, pt 1 1)
          , (Pre, pt 0 0)
          , (Bi,  pt 2 0)
          , (Pst, pt 3 0)
          , (Uni, pt 5 0)
          , (Pre, pt 4 1)
          ]
        , "△_△" )
      , ( [ (Uni, pt 8 0)
          , (Pre, pt 7 1)
          , (Bi,  pt 6 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 6 0)
          ]
        , [ (Uni, pt 9 0)
          , (Uni, pt 11 0)
          , (Uni, pt 10 1)
          ]
        , [ (Bi,  pt 8 0)
          , (Pst, pt 9 0)
          , (Uni, pt 11 0)
          , (Pre, pt 10 1)
          , (Bi,  pt 9 0)
          , (Pst, pt 8 0)
          , (Pre, pt 7 1)
          , (Bi,  pt 6 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (PPB, pt 6 0)
          ]
        , [ (Bi,  pt 9 0)
          , (Pst, pt 8 0)
          , (Pre, pt 7 1)
          , (Bi,  pt 6 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (PPB, pt 6 0)
          , (Bi,  pt 8 0)
          , (Pst, pt 9 0)
          , (Uni, pt 11 0)
          , (Pre, pt 10 1)
          ]
        , "△_△_△ Pst 2 PPB")
      , ( [ (Pre, pt 7 1)
          , (Bi,  pt 6 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 6 0)
          , (Uni, pt 8 0)
          ]
        , [ (Uni, pt 9 0)
          , (Uni, pt 11 0)
          , (Uni, pt 10 1)
          ]
        , [ (Bi,  pt 7 1)
          , (Pst, pt 9 0)
          , (Uni, pt 11 0)
          , (Pre, pt 10 1)
          , (Bi,  pt 9 0)
          , (PPB, pt 7 1)
          , (Bi,  pt 6 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 6 0)
          , (Pre, pt 8 0)
          ]
        , [ (Bi,  pt 9 0)
          , (PPB, pt 7 1)
          , (Bi,  pt 6 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 6 0)
          , (Pre, pt 8 0)
          , (Bi,  pt 7 1)
          , (Pst, pt 9 0)
          , (Uni, pt 11 0)
          , (Pre, pt 10 1)
          ]
        , "△_△_△ Pre 2 PPB")
        , ( [ (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 6 0)
          , (Uni, pt 8 0)
          , (Pre, pt 7 1)
          , (Bi,  pt 6 0)
          ]
        , [ (Uni, pt 9 0)
          , (Uni, pt 11 0)
          , (Uni, pt 10 1)
          ]
        , [ (Bi,  pt 5 0)
          , (Pst, pt 9 0)
          , (Uni, pt 11 0)
          , (Pre, pt 10 1)
          , (Bi,  pt 9 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 6 0)
          , (Uni, pt 8 0)
          , (Pre, pt 7 1)
          , (Bi,  pt 6 0)
          ]
        , [ (Bi,  pt 9 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 6 0)
          , (Uni, pt 8 0)
          , (Pre, pt 7 1)
          , (Bi,  pt 6 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 9 0)
          , (Uni, pt 11 0)
          , (Pre, pt 10 1)
          ]
        , "△_△_△ @ Bi Point 0")
      , ( [ (Bi,  pt 6 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 6 0)
          , (Uni, pt 8 0)
          , (Pre, pt 7 1)
          ]
        , [ (Uni, pt 9 0)
          , (Uni, pt 11 0)
          , (Uni, pt 10 1)
          ]
        , [ (Bi,  pt 6 0)
          , (Pst, pt 9 0)
          , (Uni, pt 11 0)
          , (Pre, pt 10 1)
          , (Bi,  pt 9 0)
          , (Bi,  pt 6 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 6 0)
          , (Uni, pt 8 0)
          , (Pre, pt 7 1)
          ]
        , [ (Bi,  pt 9 0)
          , (Bi,  pt 6 0)
          , (Pst, pt 5 0)
          , (Uni, pt 4 1)
          , (Pre, pt 3 0)
          , (Bi,  pt 5 0)
          , (Pst, pt 6 0)
          , (Uni, pt 8 0)
          , (Pre, pt 7 1)
          , (Bi,  pt 6 0)
          , (Pst, pt 9 0)
          , (Uni, pt 11 0)
          , (Pre, pt 10 1)
          ]
        , "△_△_△ @ Bi Point 1")
      ]
