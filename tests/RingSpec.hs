{-# Language FlexibleContexts
           , PartialTypeSignatures
           , ScopedTypeVariables
           , ViewPatterns
           , TypeApplications
           , DataKinds
           , NoImplicitPrelude
           , LambdaCase #-}

-- module Main where
module RingSpec where

import Ring
import RingArbitrary

import GeometryClasses
  ( SelfIntersecting(selfIntersecting)
  )
import Dimension (fromScalar)
import PLS
  ( Point
  , pt
  , nS''
  , midpoint
  , Edges(edges)
  , DistanceBetween((|-|), (|--|))
  , Intersect(intersects)
  , Segm
  )

import Polyline
  ( Polyline_(Polyline)
  , toPolyline
  )
import PolylineArbitrary
  ( getOpenPolyline
  , ClosedPolyline(getClosedPolyline)
  )

import Utilities (bimap', mkFst, mkSnd, iff, (<$<))
import Utilities.Arbitrary (minChoose)

import Unified
import Unified.Utilities (rotations, concat, lookup)
import Unified.Collections.List (List(List))
import Unified.Utilities.List (allCombo'', toPairsL, rotationsL)
import Unified.Collections.Set (Set)
import Unified.AtLeast (AtLeast(AtLeast, getAtLeast), NonEmpty, initAL)
import Unified.Arbitrary (localUniqueRanges, localUnique)

import Control.Applicative (liftA2)
import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad ((=<<))
import Data.Bifunctor (bimap)
import Data.Bitraversable (bisequenceA)
import Data.Bool
import Data.Eq
import Data.Foldable (Foldable, elem, foldr, all)
import Data.Foldable (traverse_, for_)
import Data.Function
import Data.Functor
import Data.Maybe
import Data.Maybe (isNothing, isJust, fromJust)
import Data.Ord
import Data.Semigroup
import Data.Tuple (uncurry, snd)
import Data.Tuple.HT (mapFst3)
import GHC.IO
import GHC.Int
import GHC.Num
import Test.Hspec
import Test.QuickCheck

import qualified Data.Foldable as F

type TP = (Point, Point)

main :: IO ()
main = hspec spec

tor :: Foldable t => t a -> Ring_ a
tor = Ring . Polyline . fromJust . fromFoldableMay

spec :: Spec
spec = do
  describe "ToRing" $ do
    describe "from points" $ do
      describe "specific cases" $ do
        for_
          [ ( [pt 0 0, pt 1 1, pt 0 0]
            , Just [pt 0 0, pt 1 1]
            , "2 point ring" )
          , ( [pt 0 0, pt 1 0, pt 1 1, pt 0 0]
            , Just [pt 1 0, pt 1 1, pt 0 0]
            , "ccw triangle" )
          , ( [pt 0 0, pt 1 1, pt 1 0, pt 0 0]
            , Just [pt 0 0, pt 1 0, pt 1 1]
            , "cw triangle" )
          , ( [pt 0 0, pt 1 0 , pt 1 1, pt 0 1]
            , Nothing
            , "not closed" )
          , ( [pt 0 0, pt 1 0]
            , Nothing
            , "less than 3 points" ) ]
          $ \(ps, fmap tor -> result, testName) -> it testName $ do
              toRing ps `shouldBe` result -- [Point]
              (toRing $ fromJust $ toPolyline ps) `shouldBe` result -- Polyline
              -- (toRing $ edges $ Polyline ps) `shouldBe` r -- Segm

      it "not enough points (less than 3 local unique points" $
        forAll (localUniqueRanges =<< choose (0, 2)) $
          isNothing . (toRing :: List Point -> Maybe Ring) . snd

      it "closed polyline always becomes a ring" $ property $
        isJust .
        toRing .
        getClosedPolyline

      it "ccw stays ccw" $ property $
        -- (\ps -> (getRing <$> toRingUnClosed ps) == tail ps ) .
        liftA2 (==) (getRing <$< toRingUnClosed)
                    return .
        getRing .
        fst .
        getCCWClosed

      it "cw reverses to ccw" $ property $
        -- (\ps -> (getRing <$> toRing ps) == (reverse <$> tail ps) ) .
        -- reverse .
        -- fst .
        -- getCCWClosed

        liftA2 (==) (getRing <$< toRingUnClosed . reverse . getRing)
                    (Just . getRing) .
        fst .
        getCCWClosed

      it "removes local duplicate points" $
        forAll (bisequenceA (minChoose 3, minChoose 1)) $ \(n, e) ->
          forAll ( (\ps -> (replicate e $ fromJust $ last ps) <> ps) .
                   fst <$>
                   localUniqueRanges n :: Gen (List Point) ) $
            (`shouldBe` True) .
            all (uncurry (/=)) . toPairsL .
            fromJust . toRing

  it "ToPolyline" $ property $
    \r -> let pl = fromJust $ toPolyline r in
      head pl == last pl &&
      Just (getRing r) == tail pl

  describe "isCCW" $ do
    let ccwRings = [ ([pt 0 0, pt 1 0, pt 0 1], "triangle")
                   , ([pt 0 1, pt 1 0, pt 1 1], "vertical right")
                   , ([pt 0 1, pt 0 0, pt 1 0], "vertical left") ]

    describe "isCCW True" $ do
      for_ ccwRings $
        \(tor -> r, testName) -> it testName $
          traverse_ ((`shouldBe` True) . isCCW)
                    (rotationsL r)

      it "property" $ property $
        (isCCW :: Ring -> Bool) .
        fromJust . tail .
        fst .
        getCCWClosed

    describe "isCCW False" $ do
      for_ ccwRings $
        \(tor -> r, testName) -> it testName $ do
          traverse_ ((`shouldBe` False) . isCCW . reverse)
                    (rotationsL r)

      it "property" $ property $
        not .
        (isCCW :: Ring -> Bool) .
        reverse .
        fromJust . tail .
        fst .
        getCCWClosed

    it "zero volume ring is never true" $ property $
      not .
      (isCCW :: Ring -> Bool) .
      tor .
      (\(p0, p1) -> [p0, p1, p0])

    it "shit testing so should not error out" $ property $
      const True .
      (isCCW :: Ring -> Bool) .
      Ring .
      getOpenPolyline

  describe "edges" $ do
    it "specific case" $
      edges (tor [pt 1 1, pt 2 2, pt 1 2]) `shouldBe`
        ( fromJust $ fromFoldableMay
          [ nS'' (pt 1 2) (pt 1 1)
          , nS'' (pt 1 1) (pt 2 2)
          , nS'' (pt 2 2) (pt 1 2) ] )

    it "correct number of segments" $ property $
      uncurry (==) .
      bimap length length .
      mkSnd (edges :: Ring -> NonEmpty List Segm)

  it "rotations" $
    rotations (tor [1,2,3] :: Ring_ Int) `shouldBe`
      List (fmap tor [[1,2,3],[2,3,1],[3,1,2]])

  describe "DistanceBetween Ring Point" $ do
    for_
      [ ([pt 0 0, pt 1 1, pt 0 1], pt 3 1, 2, "point")
      , ([pt 1 0, pt 1 2, pt 0 1], pt 5 1, 4, "edge") ]
      $  \((tor -> ir), p, (fromScalar -> result), testName) -> it testName $
            traverse_ ((`shouldBe` result) . (|-| p))
                      (rotationsL ir)

    it "all rotations have same distance" $ property $
      \(r :: Ring) (p :: Point) -> all (uncurry (==)) $ toPairsL $ fmap (|--| p) $ rotationsL r

  describe "DistanceBetween Ring Ring" $
    for_
      [ (([pt 0 0, pt 1 1, pt 0 1], [pt 3 1, pt 4 0, pt 4 1]), 2, "point point")
      , (([pt 0 0, pt 1 1, pt 0 1], [pt 4 2, pt 4 0, pt 5 1]), 3, "point edge")
      , (([pt 1 0, pt 1 1, pt 0 0], [pt 2 1, pt 2 0, pt 3 0]), 1, "edge edge") ]
      -- $  \(bimap' tor -> (r0, r1), fromScalar -> result, testName) -> it testName $
      --       traverse_
      --         (\(r0, r1) -> do
      --           r0 |-| r1 `shouldBe` result
      --           r1 |-| r0 `shouldBe` result )
      --         (allCombo'' (rotationsL r0) (rotationsL r1))

      $ (\(rcs, fromScalar -> result, testName) -> it testName $
          for_ rcs $ \(r0, r1) -> do
            r0 |-| r1 `shouldBe` result
            r1 |-| r0 `shouldBe` result ) .
        mapFst3 (uncurry allCombo'' . bimap' (rotationsL . tor))

  describe "SelfIntersecting Ring" $ do
    for_
      [ ( [pt 0 0, pt 1 0, pt 1 1]
        , False
        , "triangle" )
      , ( [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
        , False
        , "square" )
      , ( [pt 0 1, pt 1 0, pt 1 1, pt 0 0]
        , True
        , "hour glass" )
      , ( [pt 0 2, pt 1 0, pt 2 1, pt 1 2, pt 0 0]
        , True
        , "fish" ) ]
      $  \(tor -> r, result, testName) -> it testName $
            traverse_ ((`shouldBe` result) . selfIntersecting)
                      (rotationsL r)

    it "is self intersecting" $ do
      property $ selfIntersecting . fst . getSelfIntersectingRing

    it "not self intersecting" $ do
      property $ not . selfIntersecting . getNonSelfIntersectingRing

  describe "intersects" $ do
    let baseRing = tor [pt 0 0, pt 2 0, pt 2 2]

    for_
      [ ( [pt 2 2, pt 3 2, pt 3 3]
        , True
        , "point on point" )
      , ( [pt 2 1, pt 3 1, pt 3 2]
        , True
        , "point on edge" )
      , ( [pt 0 1, pt 3 1, pt 3 4]
        , True
        , "edges" )
      , ( [pt 5 5, pt 6 5, pt 6 6]
        , False
        , "disjoint" ) ]
      $ \(tor -> r, result, testName) -> it testName $
          for_ (allCombo'' (rotationsL baseRing) (rotationsL r))
            $ \(r0, r1) -> do
                r0 `intersects` r1 `shouldBe` result
                r1 `intersects` r0 `shouldBe` result

  describe "partition" $ do
    for_
      [ ( [pt 0 0, pt 1 0, pt 1 1]
        , [[pt 0 0, pt 1 0, pt 1 1]]
        , "single triangle" )
      , ( [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
        , [[pt 0 0, pt 1 0, pt 1 1, pt 0 1]]
        , "single square" )
      , ( [pt 0 0, pt 2 0, pt 0 2, pt 2 2]
        , [ [pt 0 0, pt 2 0, pt 1 1]
          , [pt 1 1, pt 2 2, pt 0 2] ]
        , "hour glass" )
      , ( [pt 2 0, pt 4 2, pt 4 1, pt 0 1, pt 0 2]
        , [ [pt 0 1, pt 1 1, pt 0 2]
          , [pt 1 1, pt 2 0, pt 3 1]
          , [pt 3 1, pt 4 1, pt 4 2] ]
        , "double interect") ]
      $ \(tor -> r, fmap tor -> rs, testName) -> it testName $
          for_ (rotationsL r) $
            ( (\rs' -> do
                length rs' `shouldBe` F.length rs
                all (flip elem $ concat $ fmap rotationsL rs) rs' `shouldBe` True ) .
              (partition :: Ring -> List Ring) )

    -- it "correct number of partitions" $ do
    --   property $ liftA2 (==)
    --                (length . partition)

    describe "support functions" $ do
      describe "_incSplits" $ do
        it "has the correct number of increments" $ do
          property $ \(xs :: List ()) -> let l = length xs in
            _incSplits xs `lengthEq` (l * if l >= 4 then l - 3 else 0)

        it "goes through all rotations" $ property $
          ( uncurry (==) .
            bimap' (fmap (fromJust . head) . group) .
            ( iff (`lengthLT` 4) (const empty) id &&&
              fmap (fst . fst) . _incSplits )
            :: List Int -> Bool )

        it "elements of the container are concerved" $ property $
          \(l :: List Int) ->
            all (== fromEFoldable l) $
            fmap ( (fromEFoldable :: (Foldable f, Ord a) => f a -> Set a) .
                   (\((x0, x1), (xs0, xs1)) -> x0 <| x1 <| xs0 <> xs1) ) $
            _incSplits l

        it "ordered correctly" $ do
          forAll (minChoose 0) $ \n ->
            let l = List [0..n]
                h2r = flip lookup $ fmap (mkFst (fromJust . head)) $ rotationsL l
            in all (\((x0, x1), (xs0, xs1)) ->
                     h2r x0 ==
                     Just ((x0 <| reverse xs0) <> (x1 <| xs1)) ) $
                   _incSplits l

      describe "_addP" $ do
        it "no parallel segments" $ do
          forAll ( AtLeast <$>
                   ( flip localUnique empty =<<
                     minChoose 2 )
                   :: Gen (AtLeast 2 List Point) ) $
            uncurry ((==) :: List Point -> List Point -> Bool) .
            ( ( foldr _addP empty .
                (\lu -> foldr
                  (\np (uncons -> Just (op, ps)) -> np <| (midpoint $ nS'' np op) <| op <| ps)
                  (singleton $ lastNE lu :: List Point)
                  (initAL lu) ) ) &&&
              getAtLeast )

        it "no local duplicates" $ do
          forAll (localUniqueRanges @List =<< minChoose 1) $
            all (uncurry (/=)) .
            toPairsL .
            foldr _addP (empty :: List Point) .
            fst

      describe "splitRing" $ do
        it "shit test to ensure no failures" $ do
          property $ (\case Just (r0, r1) -> r0 /= r1
                            Nothing -> True) .
                     splitRing

        let inPossible p (splitRing . tor -> rs) = do
              rs `shouldNotBe` Nothing
              let Just (r0, r1) = rs

              r0 `shouldNotBe` r1
              elem r0 p `shouldBe` True
              elem r1 p `shouldBe` True

        describe "single split" $ do
          let testSplit (tor -> r, List . fmap tor -> rs, testName) = it testName $ do
                traverse_ (inPossible $ concat $ fmap rotationsL rs)
                          (rotationsL r)

          describe "point intersects" $ do
            traverse_ testSplit
              [ ( [pt 0 0, pt 2 0, pt 0 2, pt 2 2]
                , [ [pt 0 0, pt 2 0, pt 1 1]
                  , [pt 1 1, pt 2 2, pt 0 2] ]
                , "one line each side" )
              , ( [pt 1 2, pt 0 1, pt 1 0, pt 3 2, pt 3 0]
                , [ [pt 1 2, pt 0 1, pt 1 0, pt 2 1]
                  , [pt 2 1, pt 3 0, pt 3 2] ]
                , "two lines and one line for sides" )
              , ( [pt 0 0, pt 1 0, pt 1 1, pt 2 2, pt 2 1, pt 1 1]
                , [ [pt 0 0, pt 1 0, pt 1 1]
                  , [pt 1 1, pt 2 1, pt 2 2] ]
                , "point intersect" )
              , ( [pt 0 0, pt 1 0, pt 1 3, pt 2 3, pt 1 1]
                , [ [pt 0 0, pt 1 0, pt 1 1]
                  , [pt 1 1, pt 2 3, pt 1 3] ]
                , "segment point intersect" )
              , ( [pt 0 0, pt 1 0, pt 1 2, pt 2 2, pt 2 1, pt 0 1]
                , [ [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
                  , [pt 1 1, pt 2 1, pt 2 2, pt 1 2] ]
                , "box point intersect" )
              , ( [pt 0 0, pt 3 0, pt 3 1, pt 1 3, pt 1 1, pt 3 3, pt 3 4, pt 0 4]
                , [ [pt 1 1, pt 2 2, pt 1 3]
                  , [pt 0 0, pt 3 0, pt 3 1, pt 2 2, pt 3 3, pt 3 4, pt 0 4] ]
                , "collinear lines after break" )
              ]

          describe "whole shared line" $ do
            traverse_ testSplit
              [ ( [pt 0 0, pt 1 0, pt 1 1, pt 2 0, pt 1 0, pt 1 1]
                , [ [pt 0 0, pt 1 0, pt 1 1]
                  , [pt 1 0, pt 2 0, pt 1 1] ]
                , "triangles" )
              , ( [pt 0 0, pt 1 0, pt 1 1, pt 2 1, pt 2 0, pt 1 0, pt 1 1, pt 0 1]
                , [ [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
                  , [pt 1 0, pt 2 0, pt 2 1, pt 1 1] ]
                , "boxes" )
              ]

          describe "partial shared line" $ do
            traverse_ testSplit
              [ ( [pt 0 0, pt 1 0, pt 1 2, pt 2 0, pt 1 0, pt 1 1]
                , [ [pt 0 0, pt 1 0, pt 1 1]
                  , [pt 1 0, pt 2 0, pt 1 2] ]
                , "with one endpoint 0" )
              , ( [pt 0 0, pt 1 0, pt 1 2, pt 2 0, pt 1 1, pt 1 2]
                , [ [pt 0 0, pt 1 0, pt 1 2]
                  , [pt 1 1, pt 2 0, pt 1 2] ]
                , "with one endpoint 1 reversed" )
              , ( [pt 0 0, pt 1 0, pt 1 3, pt 2 0, pt 1 1, pt 1 2]
                , [ [pt 0 0, pt 1 0, pt 1 2]
                  , [pt 1 1, pt 2 0, pt 1 3] ]
                , "without endpoint" )
              , ( [pt 0 0, pt 1 0, pt 1 3, pt 2 0, pt 1 2, pt 1 1]
                , [ [pt 0 0, pt 1 0, pt 1 1]
                  , [pt 1 2, pt 2 0, pt 1 3] ]
                , "without endpoint reversed" )
              ]

          describe "two point ring" $ do
            traverse_ testSplit
              [ ( [pt 0 0, pt 1 0, pt 1 2, pt 1 1]
                , [ [pt 0 0, pt 1 0, pt 1 1]
                  , [pt 1 1, pt 1 2] ]
                , "partial overlap" )
              , ( [pt 0 0, pt 1 1, pt 2 0, pt 2 4, pt 2 3]
                , [ [pt 0 0, pt 1 1, pt 2 0, pt 2 3]
                  , [pt 2 3, pt 2 4] ]
                , "partial overlap more" )
              , ( [pt 0 0, pt 2 0, pt 1 1, pt 1 2, pt 1 1]
                , [ [pt 0 0, pt 2 0, pt 1 1]
                  , [pt 1 1, pt 1 2] ]
                , "full overlap" )
              , ( [pt 0 1, pt 0 0, pt 2 0, pt 1 1, pt 1 2, pt 1 1]
                , [ [pt 0 1, pt 0 0, pt 2 0, pt 1 1]
                  , [pt 1 1, pt 1 2] ]
                , "full overlap more" )
              ]

        it "more than one cross" $ do
          let rs = fmap tor
                [ [pt 0 1, pt 1 1, pt 0 2]
                , [pt 1 1, pt 2 0, pt 3 1]
                , [pt 3 1, pt 4 1, pt 4 2]
                , [pt 0 2, pt 2 0, pt 3 1, pt 0 1]
                , [pt 1 1, pt 2 0, pt 4 2, pt 4 1] ]
          traverse_ (inPossible $ concat $ fmap rotationsL rs)
                    (rotationsL $ tor [pt 0 1, pt 4 1, pt 4 2, pt 2 0, pt 0 2])

        describe "no split" $ do
          for_
            [ ( [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
                , "square" )
            , ( [pt 0 0, pt 1 0, pt 0 1]
              , "only 3 points" )
            , ( [pt 2 0, pt 2 1, pt 1 2, pt 2 3, pt 2 4, pt 0 2]
              , "collinear lines" )
            , ( [pt 0 0, pt 1 1]
              , "only two points" ) ]
            $ \(tor -> ps, testName) -> it testName $
                traverse_ ((`shouldBe` Nothing) . splitRing)
                          (rotationsL ps)



  -- -- describe "removeHairs" $ do
  -- --   let test (ps, result, testName) = it testName $ do
  -- --         let testRotation r v = do
  -- --               let rs = fullRotation $ translate v $ quickRing result

  -- --               let testSingle r = do
  -- --                     selfIntersecting r `shouldBe` True
  -- --                     let rhr = removeHairs r
  -- --                     selfIntersecting rhr `shouldBe` False
  -- --                     elem rhr rs `shouldBe` True

  -- --               mapM_ testSingle $ fullRotation $ translate v r

  -- --         property $ testRotation $ Ring ps

  -- --   mapM_ test [
  -- --       ( [pt 0 0, pt 1 1, pt 2 2, pt 1 1, pt 0 1]
  -- --       , [pt 0 0, pt 1 1, pt 0 1]
  -- --       , "point to point" )
  -- --     , ( [pt 0 0, pt 2 0, pt 1 0, pt 0 1]
  -- --       , [pt 0 0, pt 1 0, pt 0 1]
  -- --       , "mid to point" )
  -- --     , ( [pt 0 0, pt 2 0, pt 1 0, pt 1 2, pt 2 2, pt 1 2]
  -- --       , [pt 0 0, pt 1 0, pt 1 2]
  -- --       , "2 hairs" )
  -- --     , ( [pt 0 0, pt 2 0, pt 3 1, pt 2 0, pt 1 0, pt 1 1]
  -- --       , [pt 0 0, pt 1 0, pt 1 1]
  -- --       ,  "long hair" )
  -- --     ]



  -- -- describe "offset" $ do
  -- --   let testSingle t o nrs r = do
  -- --         print $ "r: " ++ show r
  -- --         let rs = offset t o r
  -- --         print $ "rs: " ++ show rs
  -- --         print $ "nrs: " ++ show nrs ++ ", length rs: " ++ show (length rs)
  -- --         length rs `shouldBe` nrs

  -- --         let minO = o - t - floatingErrorTolerance
  -- --         let maxO = o + t + floatingErrorTolerance
  -- --         let perRing r' = do
  -- --               let minDistance = r' |-| r
  -- --               print $ "minO: " ++ show minO ++ ", minDistance: " ++ show minDistance ++ ", minO <= minDistance: " ++ show (minO <= minDistance)
  -- --               minO <= minDistance && minDistance <= maxO
  -- --                 `shouldBe` True

  -- --               let md = maximum $ map ((|-|) r) $ toPs r'
  -- --               print $ "maxO: " ++ show maxO ++ ", md: " ++ show md ++ ", minO <= md && md <= maxO: " ++ show (minO <= md && md <= maxO)
  -- --               minO <= md && md <= maxO `shouldBe` True

  -- --               print $ "selfIntersecting r': " ++ show (selfIntersecting r')
  -- --               selfIntersecting r' `shouldBe` False

  -- --         mapM_ perRing rs

  -- --   let test (t, o, nrs, ps, testName) = it testName $ do
  -- --         let testRotation v = do
  -- --               mapM_ (testSingle t o nrs)
  -- --                     (fullRotation $ translate v $ quickRing ps)

  -- --         property testRotation

  -- --   let starTrek = [pt 3 1, pt 1 1, pt 1 3, pt 0 0]

  -- --   mapM_ test [
  -- --                ( 0.1, 1, 1
  -- --                , [pt 0 0, pt 1 0, pt 1 1]
  -- --                , "triangle" )
  -- --              , ( 0.1, 2, 1
  -- --                , [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
  -- --                , "square" )
  -- --              , ( 0.1, 1, 1
  -- --                , starTrek
  -- --                , "star trek less than" )
  -- --              , ( 0.1, 2, 1
  -- --                , starTrek
  -- --                , "star trek equal" )
  -- --              , ( 0.1, 3, 1
  -- --                , starTrek
  -- --                , "star trek greater" )
  -- --              , ( 0.5, 1, 1
  -- --                , [pt 3 0, pt 2 1, pt 2 3, pt 3 4, pt 0 2]
  -- --                , "concave 3" )
  -- --              , ( 0.5, 1, 1
  -- --               , [pt 2 4, pt 0 4, pt 0 0, pt 3 1, pt 2 1, pt 1 2, pt 1 3]
  -- --               , "concave 4" )
  -- --              -- , ( 0.5, 1, 2
  -- --              --   , [pt 0 0, pt 4 0, pt 4 2, pt 1 1, pt 1 4, pt 4 3, pt 4 5, pt 0 5]
  -- --              --   , "dovetail key" )
  -- --              ]

  -- --   it "problem case st3 other" $ do
  -- --     testSingle 0.5 1 1 $ quickRing [pt 0 2, pt 3 0, pt 2 1, pt 2 3, pt 3 4]

  -- --   it "problem case st3" $ do
  -- --     testSingle 0.5 1 1 $ quickRing [pt 3 4, pt 0 2, pt 3 0, pt 2 1, pt 2 3]

  -- --   -- it "problem case concave 4 other" $ do
  -- --   --   testSingle 0.5 1 1 $ quickRing [pt 2 4, pt 0 4, pt 0 0, pt 3 1, pt 2 1, pt 1 2, pt 1 3]

  -- --   -- it "problem case concave 4" $ do
  -- --   --   testSingle 0.5 1 1 $
  -- --   --     translate (vec 1.8864640024512251 (-0.6636522824205721))
  -- --   --               (quickRing [pt 2 4, pt 0 4, pt 0 0, pt 3 1, pt 2 1, pt 1 2, pt 1 3])


  -- --   -- it "problem case dove" $ do
  -- --   --   testSingle 0.5 1 2 $
  -- --   --     translate (vec 1.153338420210376 0.11072198350341049)
  -- --   --               (quickRing [pt 0 0, pt 4 0, pt 4 2, pt 1 1, pt 1 4, pt 4 3, pt 4 5, pt 0 5])


