{-# LANGUAGE LambdaCase
           , ScopedTypeVariables
           , TupleSections
           , PartialTypeSignatures #-}

module SegmArbitrarySpec where

import SegmArbitrary

import GeometryClasses (contains, interiorContains)
import Dimension
  ( (=*=)
  , Squared(Squared)
  )
import Vector
  ( collinearV
  , magSqrd
  , Ward(Backward)
  , toward
  , parallel
  , parallel'
  , toVec
  , perp
  , xp
  )
import qualified PLS
import PLS
  ( Segm
  , Point
  , thps
  , thps'
  , tp
  , hp
  , (.->.)
  , (.<-.)
  , sharedEndpoint
  , collinearL
  , intersectPoint
  , intersectSegm
  , intersects
  , hasEndpoint
  , hasEndpoint'
  , (..-)
  )

import Utilities
  ( bimap'
  , zero
  , t3to2
  )
import IsClose (isClose)

import Unified
  ( filter
  , singleton
  )
import Unified.Utilities
  ( concat
  , take2
  )
import Unified.Collections.List (List)
import Unified.Utilities.List
  ( allCombo''
  , tupleCombo
  )

import Control.Applicative (liftA2)
import Control.Arrow (app)
import Data.Bits (xor)
import Data.Maybe (isNothing, fromJust)
import Data.Tuple (swap)
import Test.Hspec hiding (parallel)
import Test.QuickCheck
import Data.Bifunctor (bimap)

import Prelude hiding (reverse, map, concat, (++), filter)


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  it "IntersectingSegms" $ do
    property $
      all ( (\case (s, Just s') -> s == s' ||
                                   ( s `collinearV` s' &&
                                     magSqrd s >= magSqrd s' )
                   (_, Nothing) -> True ) .
            (\(s, p) -> (s, tp s ..- p)) ) .
      uncurry allCombo'' .
      (\(s, s', ps) -> ([s, s'], ps)) .
      (\case (s, s', Left  p) -> (s, s', singleton p)
             (s, s', Right s'') -> (s, s', thps' s'') ) .
      getIntersectingSegms

  it "InteriorIntersectingSegms" $ do
    property $
      (\vs@(s0, s1, e) -> let ss = [s0, s1]
                              ss' = t3to2 vs in
          case e of
            Left p -> flip all ss
                        ( (\case (s, Just (s' :: Segm)) ->
                                   s `collinearV` s' &&
                                   magSqrd s > magSqrd s'
                                 (_, Nothing) -> False ) .
                          (\s -> (s, tp s ..- p)) ) ||
                      flip any [ss', swap ss']
                        ( (\case (s, s', Just (s'' :: Segm)) ->
                                   s `hasEndpoint` p &&
                                   s' `collinearV` s'' &&
                                   magSqrd s' > magSqrd s''
                                 _ -> False ) .
                          (\(s, s') -> (s, s', tp s' ..- p)) )
            Right s -> all ( (\case (s', Just s'') ->
                                      ( s' `collinearV` s'' &&
                                        magSqrd s' >= magSqrd s' ) ||
                                      s' == s''
                                    (_, Nothing) -> True ) .
                             (\(s', p) -> (s', tp s' ..- p)) ) $
                         allCombo'' ss (thps' s) ) .
      getInteriorIntersectingSegms

  it "SegmsIntersectingAtPoint" $ do
    let test :: SegmsIntersectingAtPoint -> Bool
        test (SegmsIntersectingAtPoint (s0, s1, p)) =
          containsP s0 && containsP s1
          where containsP s = hasEndpoint s p ||
                              ( s `collinearV` vp &&
                                magSqrd s >= magSqrd vp )
                  where vp = tp s .->. p
    property test

  it "InteriorPointIntersectingSegms" $ do
    let test :: InteriorPointIntersectingSegms -> Bool
        test (InteriorPointIntersectingSegms (s0, s1, p)) =
          containsP s0 && containsP s1
          where containsP s = s `collinearV` vp &&
                              magSqrd s > magSqrd vp
                  where vp = tp s .->. p
    property test

  it "EndInteriorIntersectingSegms" $ do
    let test :: EndInteriorIntersectingSegms -> Bool
        test (EndInteriorIntersectingSegms (s0, s1, p)) =
          not (s0 `parallel` s1) &&
          (c s0 s1 || c s1 s0)
          where c s s' = s /= s' &&
                         hasEndpoint s p &&
                         s' `collinearV` v &&
                         magSqrd s' > magSqrd v
                  where v = tp s' .->. p
    property test

  it "DisjointSegms" $ do
    let test :: DisjointSegms -> Bool
        test (DisjointSegms (s0, s1)) =
          not (intersects s0 s1) &&
          isNothing (intersectPoint s0 s1) &&
          isNothing (intersectSegm s0 s1)
    property test

  it "AngledDisjointSegms" $ do
    let test :: AngledDisjointSegms -> Bool
        test (AngledDisjointSegms (s0, s1)) =
          not (intersects s0 s1) &&
          isNothing (intersectPoint s0 s1) &&
          isNothing (intersectSegm s0 s1)
    property test

  it "SharedEndpointSegms" $ do
    let test :: SharedEndpointSegms -> Bool
        test (SharedEndpointSegms (s0, s1, p)) = elem p (thps' s0) &&
                                                 elem p (thps' s1)
    property test

  it "OneSharedEndpointSegms" $ do
    let test :: OneSharedEndpointSegms -> Bool
        test (OneSharedEndpointSegms (s0, s1, p)) = elem p (thps' s0) &&
                                                    elem p (thps' s1) &&
                                                    op s0 /= op s1
          where op s = if p == tp s then hp s else tp s
    property test

  it "TwoSharedEndpointSegms" $ property $
    (\(s0, s1) -> s0 == PLS.reverse s1 || s0 == s1) .
    getTwoSharedEndpointSegms

  it "HeadToTailSegms" $ do
    property $ \(HeadToTailSegms (s0, s1)) -> hp s0 == tp s1

  it "HeadToHeadSegms" $ do
    property $ \(HeadToHeadSegms (s0, s1)) -> hp s0 == hp s1

  it "TailToTailSegms" $ do
    property $ \(TailToTailSegms (s0, s1)) -> tp s0 == tp s1

  it "IdenticalSegms" $ do
    property $ \(IdenticalSegms (s0, s1)) -> s0 == s1

  it "ReversedSegms" $ do
    property $ \(ReversedSegms (s0, s1)) -> thps s0 == swap (thps s1)

  it "CollinearIntersectingSegms" $ do
    let test :: CollinearIntersectingSegms -> Bool
        test (CollinearIntersectingSegms (s0, s1, s2)) =
          all (`elem` ps01) ps2 &&
          all (contains s0) ps2 &&
          all (contains s1) ps2
          where ps01 = concat $ fmap thps' [s0, s1]
                ps2 = thps' s2
    property test

  it "SegmAndInteriorSegment" $ do
    let test :: SegmAndInteriorSegment -> Bool
        test (SegmAndInteriorSegment (s0, s1)) =
          all ( liftA2 (&&)
                  (collinearV v)
                  ((< l) . magSqrd) .
                (.<-. tp s0) )
              (thps' s1)
          where v = toVec s0
                l = magSqrd v

    property test

  it "BoundryBoundryInteriorIntersectingSegms" $ do
    let test :: BoundryBoundryInteriorIntersectingSegms -> Bool
        test (BoundryBoundryInteriorIntersectingSegms (s0, s1, s2)) =
          any (interiorContains s0) (thps' s2) &&
          s0 `collinearL` s1 &&
          s1 `collinearL` s2 &&
          isNothing (s0 `sharedEndpoint` s1)
    property test

  it "CollinearEndInteriorIntersectingSegms" $ property $
      (\(s0, s1, s2, p) -> let
        (p0, p1) = fromJust $ take2 $ filter (/= p) $ concat $ fmap thps' [s0, s1]
        in elem p (thps' s0) &&
           elem p (thps' s1) &&
           elem p (thps' s2) &&
           (p .->. p0) `collinearV` (p .->. p1) &&
           all (contains s0) (thps' s2) &&
           all (contains s1) (thps' s2) ) .
      getCollinearEndInteriorIntersectingSegms

  it "CollinearEndsIntersectingSegms" $ do
    let test :: CollinearEndsIntersectingSegms -> Bool
        test (CollinearEndsIntersectingSegms (s0, s1, p)) =
          any (\(s, s') -> hp s == tp s' && hp s == p)
              (tupleCombo PLS.reverse (s0, s1))
    property test

  it "CollinearDisjointSegms" $ do
    let test :: CollinearDisjointSegms -> Bool
        test (CollinearDisjointSegms (s0, s1)) =
          v0 `parallel` v1 && v0 `parallel` vtt && -- are collinear lines
          vtt `collinearV` vth && -- s1 does not contain s0 tail point
          ( not (collinearV v0 vtt || collinearV v0 vth) || -- collinear segm is behind
            (magSqrd v0 < magSqrd vtt && magSqrd v0 < magSqrd vth) )
          where v0 = toVec s0
                v1 = toVec s1
                vtt = t0 .->. t1
                vth = t0 .->. h1
                t0 = tp s0
                t1 = tp s1
                h1 = hp s1
    property test

  it "PointAlongSegm" $ do
    let test :: PointAlongSegm -> Bool
        test (PointAlongSegm (s, p, _)) =
          s `contains` p ||
          ((tp s .->. p) `xp` vp) =*= ((hp s .->. p) `xp` vp) < (Squared $ zero)
          where vp = perp $ toVec s
    property test

  it "PointNearSegmEndPoint" $ do
    let test :: PointNearSegmEndPoint -> Bool
        test (PointNearSegmEndPoint (s, p, d)) =
          any ((isClose d) . magSqrd . (.->. p)) (thps' s)
    property test

  it "DisjointSegmPoint" $ do
    let test :: DisjointSegmPoint -> Bool
        test (DisjointSegmPoint (s, p)) =
          magSqrd vs < magSqrd v ||
          v `toward` vs == Backward ||
          (not (v `collinearV` vs) && tp s /= p)
          where v = tp s .->. p
                vs = toVec s
    property test

  it "DisjointCollinearSegmPoint" $ do
    let test :: DisjointCollinearSegmPoint -> Bool
        test (DisjointCollinearSegmPoint (s, p)) =
          s `parallel` v &&
          ( s `toward` v == Backward ||
            magSqrd s < magSqrd v )
          where v = tp s .->. p
    property test

  describe "DisjointSegmAndSegmEndpoint" $ do
    it "point is collinear and disjoint" $ property $
      uncurry xor .
      bimap' (\(s :: Segm, p :: Point) -> let v = tp s .->. p
        in s `parallel` v &&
           ( not (s `collinearV` v) ||
               magSqrd s < magSqrd v ) ) .
      (\(ss, p) -> bimap' (, p) ss) .
      getDisjointSegmAndSegmEndpoint

    it "point is an endpoint" $ property $
      uncurry xor .
      bimap' hasEndpoint' .
      (\(ss, p) -> bimap' (, p) ss) .
      getDisjointSegmAndSegmEndpoint

    it "segments are not parallel" $ property $
      not .
      parallel' .
      fst .
      getDisjointSegmAndSegmEndpoint

  it "SegmPointCoincide & pointOnSegment" $ do
    let test :: SegmPointCoincide -> Bool
        test (SegmPointCoincide (s, p)) =
          (s `collinearV` v && magSqrd s >= magSqrd v) ||
          tp s == p
          where v = tp s .->. p
    property test

  it "SegmAndEndPoint" $ property $
    app .
    bimap elem thps' .
    swap .
    getSegmAndEndPoint

  it "endpointOfSegment" $ property $
    liftA2 forAll
      endpointOfSegment
      hasEndpoint

  describe "SegmAndInteriorPoint" $ do
    it "point on line's interior" $ property $
      (\(s, v) -> s `collinearV` v &&
                  magSqrd s > magSqrd v ) .
      (\(s, p) -> (s, tp s .->. p)) .
      getSegmAndInteriorPoint

    it "not an endpoint" $ property $
      not .
      hasEndpoint' .
      getSegmAndInteriorPoint

  describe "interiorPointOnSegment" $ do
    it "point on line's interior" $ property $
      liftA2 forAll
        interiorPointOnSegment
        (\s p -> let v = tp s .->. p in
          s `collinearV` v &&
          magSqrd s > magSqrd v )

    it "not an endpoint" $ property $
      liftA2 forAll
        interiorPointOnSegment
        (\s -> not . hasEndpoint s)

  it "NoSharedEndpoint" $ do
    property $ all (uncurry (/=)) .
               uncurry allCombo'' .
               bimap' (thps' :: Segm -> List Point) .
               getNoSharedEndpoint

