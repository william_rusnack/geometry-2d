{-# LANGUAGE FlexibleContexts
           , LambdaCase
           , ScopedTypeVariables #-}

-- module Main where
module SegmSpec where

import qualified PLS
import PLS
  ( Segm(Segm)
  , crossIntersect'
  , intersectSegm
  , intersectSegm'
  , (->->), (-><-), (<-->), (<->)
  , nS'', nS'''
  , sharedEndpoint
  , hasEndpoint
  , midpoint
  , intersects'
  , interiorIntersect'
  , ininIntersect'
  , DistanceBetween((|--|))
  , otherEndpoint

  -- Point
  , pt
  , (.->.)

  -- Line
  , Line(Line)
  , TailHeadPoints(tp, hp, thps)
  , IntersectPoint(intersectPoint, intersectPoint')
  )

import SegmArbitrary

import GeometryClasses
  ( Contain(contains', interiorContains, interiorContains')
  , biInteriorIntersect
  )
import LineArbitrary (pointOnEachSide, twoPointsOnOneSide)

import Utilities (bimap', t3to2, zero, mkSnd)
import IsClose (IsClose(isClose))

import Control.Applicative (liftA2)
import Data.Bifunctor (first)
import Data.Maybe (isNothing)
import Data.Tuple.HT (swap)
import Test.Hspec
import Test.QuickCheck

import Prelude hiding (reverse, map)


main :: IO ()
main = hspec spec


spec :: Spec
spec = do
  describe "intersectPoint" $ do
    it "endpoint" $ do
      let l0 = nS'' (pt 0 0) (pt 3 3)
      let refPt = pt 2 2
      let l1 = nS'' refPt (pt 3 2)
      l0 `intersectPoint` l1 `shouldBe` Just refPt
      l1 `intersectPoint` l0 `shouldBe` Just refPt

    it "has intersect point" $ property $
      (\(s0, s1, p) -> intersectPoint s0 s1 `isClose` Just p) .
      getInteriorPointIntersectingSegms

    it "no intersect point" $ property $
      isNothing .
      intersectPoint' .
      getDisjointSegms

    it "endpoints intersect" $ property $
      (\(s0, s1, p) -> intersectPoint s0 s1 == Just p) .
      getOneSharedEndpointSegms

    it "endpoint to midsection intersect" $ property $
      (\(s0, s1, p) -> intersectPoint s0 s1 == Just p) .
      getEndInteriorIntersectingSegms

    it "collinear lines with segment overlap do not have an intersect point" $ property $
      isNothing .
      intersectPoint' .
      t3to2 .
      getCollinearIntersectingSegms

    it "collinear lines with only an endpoint intersect do have an intersect point" $ property $
      uncurry (==) .
      (\(s0, s1, p) -> (intersectPoint s0 s1, Just p)) .
      getCollinearEndsIntersectingSegms

    it "specific collinear intersect" $ do
      nS'' (pt (-5.005235899550797) 2.2760524417775456) (pt (-13.244771127731275) 0.3061315277516876)
      `intersectPoint`
      nS'' (pt (-5.005235899550797) 2.2760524417775456) (pt (-76.6515837899691) (-14.853267988845166))
      `shouldBe` Nothing

  describe "intersectSegm" $ do
    it "is intersecting" $ do
      let test :: CollinearIntersectingSegms -> Bool
          test (CollinearIntersectingSegms (s0, s1, s2)) =
            any (isClose si) [s2, PLS.reverse s2]
            where Just si = intersectSegm s0 s1
      property test

    it "no intersect" $ do
      property $ isNothing . intersectSegm' . getDisjointSegms

  describe "Intersect" $ do
    describe "intersect" $ do
      it "does intersect" $ do
        property $ intersects' . t3to2 . getIntersectingSegms

      it "does not intersect" $ do
        property $ not . intersects' . getDisjointSegms

    describe "interiorIntersect" $ do
      it "is interior intersect" $ do
        property $ interiorIntersect' . t3to2 . getInteriorIntersectingSegms

      it "interior point intersects" $ do
        property $ interiorIntersect' .
                   t3to2 .
                   getInteriorPointIntersectingSegms

      it "no intersects" $ do
        property $ not . interiorIntersect' . getDisjointSegms

      it "no intersect with only a shared endpoint" $ do
        property $ not .
                   interiorIntersect' .
                   t3to2 .
                   getOneSharedEndpointSegms

    describe "ininIntersect" $ do
      describe "has intersect" $ do
        it "interior point intersects" $ property $
          ininIntersect' .
          t3to2 .
          getInteriorPointIntersectingSegms

        it "collinear" $ property $
          ininIntersect' .
          t3to2 .
          getCollinearIntersectingSegms

      describe "no intersect" $ do
        it "disjoint" $ property $
          not . ininIntersect' . getDisjointSegms

        it "only a shared endpoint" $ property $
          not .
          ininIntersect' .
          t3to2 .
          getOneSharedEndpointSegms

        it "endpoint interior intersect" $ property $
          not .
          ininIntersect' .
          t3to2 .
          getEndInteriorIntersectingSegms

    describe "crossIntersect" $ do
      it "does cross intersect" $ do
        property $ crossIntersect' .
                   t3to2 . 
                   getInteriorPointIntersectingSegms

      it "collinear overlap is not a cross intersect" $ do
        property $ not .
                   crossIntersect' .
                   t3to2 .
                   getCollinearIntersectingSegms

      it "endpoint on interior is not a cross intersect" $ do
        property $ not .
                   crossIntersect' .
                   t3to2 .
                   getEndInteriorIntersectingSegms

      it "disjoint segms have no cross intersect" $ do
        property $ not .
                   crossIntersect' .
                   getDisjointSegms

  describe "BiIntersect" $ do
    describe "biInteriorIntersect" $ do
      it "does have interior intersect" $ do
        property $ \(l :: Line) -> forAll (pointOnEachSide l) $
          biInteriorIntersect l . nS'''

      it "does not have interior intersect" $ do
        property $ \(l :: Line) -> forAll (twoPointsOnOneSide l) $
          not . biInteriorIntersect l . nS'''

  describe "Contain Segm Point" $ do
    describe "contain" $ do
      it "does contain" $ do
        property $ contains' . getSegmPointCoincide

      it "does not contain disjoint point" $ do
        property $ not . contains' . getDisjointSegmPoint

    describe "interiorContains" $ do
      it "does contain" $ do
        property $ interiorContains' . getSegmAndInteriorPoint

      it "does not contain endpoint)" $ do
        property $ not . interiorContains' . getSegmAndEndPoint

      it "does not contain" $ do
        property $ not . interiorContains' . getDisjointSegmPoint

      it "specific case" $ do
        let s = Segm (Line (pt 1.1187760973982568 (-0.3359682779696716),pt 1.858909846689242 6.597804966856382))
        let p = pt 1.2432196704353666 0.8298527888873033
        s `interiorContains` p `shouldBe` True

  describe "Contain Segm Segm" $ do
    describe "contain" $ do
      it "interior segment" $ do
        property $ liftA2 (&&)
                      contains'
                      (not . contains' . swap) .
                   getSegmAndInteriorSegment

      describe "does contain" $ do
        it "two shared endpoints" $ do
          property $ contains' . getTwoSharedEndpointSegms

      describe "does not contain" $ do
        it "partial intersect" $ do
          property $ not . contains' . t3to2 . getSegmsIntersectingAtPoint

        it "disjoint" $ do
          property $ not . contains' . getDisjointSegms

    describe "interiorContains" $ do
      it "interior segment" $ do
        property $ liftA2 (&&)
                      interiorContains'
                      (not . interiorContains' . swap) .
                   getSegmAndInteriorSegment

      describe "does not contain" $ do
        it "partial intersect" $ do
          property $ not .
                     interiorContains' .
                     t3to2 .
                     getSegmsIntersectingAtPoint

        it "disjoint" $ do
          property $ not . interiorContains' . getDisjointSegms


  describe "endpoint intersects" $ do
    describe "(->->)" $ do
      it "HeadToTailSegms" $ do
        property $ (== True) . uncurry (->->) . getHeadToTailSegms
      it "HeadToHeadSegms" $ do
        property $ (== False) . uncurry (->->) . getHeadToHeadSegms
      it "TailToTailSegms" $ do
        property $ (== False) . uncurry (->->) . getTailToTailSegms
      it "ReversedSegms" $ do
        property $ (== False) . uncurry (->->) . getReversedSegms
      it "InteriorIntersectingSegms" $ do
        property $ (== False) . uncurry (->->) . t3to2 . getInteriorIntersectingSegms
      it "EndInteriorIntersectingSegms" $ do
        property $ (== False) . uncurry (->->) . t3to2 . getEndInteriorIntersectingSegms

    describe "(-><-)" $ do
      it "HeadToTailSegms" $ do
        property $ (== False) . uncurry (-><-) . getHeadToTailSegms
      it "HeadToHeadSegms" $ do
        property $ (== True) . uncurry (-><-) . getHeadToHeadSegms
      it "TailToTailSegms" $ do
        property $ (== False) . uncurry (-><-) . getTailToTailSegms
      it "ReversedSegms" $ do
        property $ (== False) . uncurry (-><-) . getReversedSegms
      it "InteriorIntersectingSegms" $ do
        property $ (== False) . uncurry (-><-) . t3to2 . getInteriorIntersectingSegms
      it "EndInteriorIntersectingSegms" $ do
        property $ (== False) . uncurry (-><-) . t3to2 . getEndInteriorIntersectingSegms

    describe "(<-->)" $ do
      it "HeadToTailSegms" $ do
        property $ (== False) . uncurry (<-->) . getHeadToTailSegms
      it "HeadToHeadSegms" $ do
        property $ (== False) . uncurry (<-->) . getHeadToHeadSegms
      it "TailToTailSegms" $ do
        property $ (== True) . uncurry (<-->) . getTailToTailSegms
      it "ReversedSegms" $ do
        property $ (== False) . uncurry (<-->) . getReversedSegms
      it "InteriorIntersectingSegms" $ do
        property $ (== False) . uncurry (<-->) . t3to2 . getInteriorIntersectingSegms
      it "EndInteriorIntersectingSegms" $ do
        property $ (== False) . uncurry (<-->) . t3to2 . getEndInteriorIntersectingSegms

    describe "(<->)" $ do
      it "HeadToTailSegms" $ do
        property $ (== False) . uncurry (<->) . getHeadToTailSegms
      it "HeadToHeadSegms" $ do
        property $ (== False) . uncurry (<->) . getHeadToHeadSegms
      it "TailToTailSegms" $ do
        property $ (== False) . uncurry (<->) . getTailToTailSegms
      it "ReversedSegms" $ do
        property $ (== True) . uncurry (<->) . getReversedSegms
      it "EndInteriorIntersectingSegms" $ do
        property $ (== False) . uncurry (<->) . t3to2 . getEndInteriorIntersectingSegms

  it "midpoint" $ do
    property $ \s -> let p = midpoint s in
      (isClose zero) $ uncurry (<>) $ bimap' (.->. p) $ thps s

  describe "DistanceBetween" $ do
    describe "Segm Point" $ do
      it "point lies along side the segment" $ do
        let test :: PointAlongSegm -> Bool
            test (PointAlongSegm (s, p, d)) = (s |--| p) `isClose` d
        property test

      it "point lies closer to the segment's endpoint than its interior" $ do
        let test :: PointNearSegmEndPoint -> Bool
            test (PointNearSegmEndPoint (s, p, d)) = (s |--| p) `isClose` d
        property test

      it "near zero when point coincides with segment" $ do
        property $ isClose zero . uncurry (|--|) . getSegmPointCoincide

    describe "Segm Segm" $ do
      it "intersecting segments distance is 0" $ do
        property $ isClose zero . uncurry (|--|) . t3to2 . getIntersectingSegms

      it "non-intersecting segments have segment to point distance" $ do
        let test :: DisjointSegms -> Bool
            test (DisjointSegms (s0, s1)) =
              s0 |--| s1 == minimum [ s0 |--| tp s1
                                    , s0 |--| hp s1
                                    , s1 |--| tp s0
                                    , s1 |--| hp s0]
        property test

  describe "sharedEndpoint" $ do
    it "does share" $ do
      let test :: OneSharedEndpointSegms -> Bool
          test (OneSharedEndpointSegms (s0, s1, p)) = sharedEndpoint s0 s1 == Just p
      property test

    it "none shared" $ do
      property $ isNothing . uncurry sharedEndpoint . getDisjointSegms

  describe "hasEndpoint" $ do
    it "true" $ do
      property $ uncurry hasEndpoint . getSegmAndEndPoint

    it "false" $ do
      property $ not . uncurry hasEndpoint . getSegmAndInteriorPoint

  describe "otherEndpoint" $ do
    it "never returns input point" $ property $
      uncurry (/=) .
      first snd .
      mkSnd (uncurry otherEndpoint) .
      getSegmAndEndPoint
