{-# LANGUAGE FlexibleContexts
           , LambdaCase
           , NoImplicitPrelude
           , ViewPatterns
           , PartialTypeSignatures #-}

module TriangleSpec where

import Triangle
  ( Triangle
  , nTriangle
  , nTriangle'
  , triVectors

  , getSharedBoundryNoInterior
  , getSharesOneEdgeNoInterior
  , getSharesPartialEdgeNoInterior
  , getSharesPointNoInterior
  )

import GeometryClasses
  ( biInteriorIntersect'
  , Contain(contains)
  )
import Dimension (fromScalar)
import Area (area)
import Vector (between, _toNZV)
import qualified PLS
import PLS
  ( (.->.)
  , (..-)
  , (.<-.)
  , midpoint
  , Point
  , pt
  , Segm(Segm, getLine)
  , thps'
  , edges
  , intersects
  , intersects'
  , interiorIntersect
  , interiorIntersect'
  )

import Utilities (t3to2, bimap', maybeBool, mkSnd, (<$<))

import Unified
import Unified.Utilities (take2, take3, concat)
import Unified.Collections.List (List(List))
import Unified.Utilities.List (allCombo'', filterL, permutations, zipL)
import Unified.Collections.Set (Set)
import Unified.Arbitrary (uniqueValues)

import Control.Applicative (liftA2)
import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad ((<=<))
import Data.Bifunctor (second)
import Data.Bool
import Data.Either
import Data.Eq
import Data.Foldable (Foldable, elem, all, for_, toList, any)
import Data.Function
import Data.Functor
import Data.Maybe
import Data.Maybe (fromJust, isNothing)
import Data.Semigroup ((<>))
import Data.Traversable (sequenceA)
import Data.Tuple (uncurry)
import Data.Tuple.HT (thd3, mapThd3)
import GHC.IO
import Test.Hspec
import Test.QuickCheck

import qualified Data.List as L


_nTriangle :: (Point, Point, Point) -> Triangle
_nTriangle = fromJust . nTriangle'

pts2tri :: Foldable t => t Point -> Triangle
pts2tri = _nTriangle . (\[p0, p1, p2] -> (p0, p1, p2)) . toList

ups :: ((Point, Point, Point) -> Bool) -> Property
ups = forAll $ fromJust . take3 <$> uniqueValues 3 (empty :: List Point)

triSetPoints :: Triangle -> Set Point
triSetPoints = fromEFoldable


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "nTriangle" $ do
    it "Nothing if 2 equal points" $ property $
      all ( isNothing .
            nTriangle' .
            fromJust . take3) .
      permutations .
      (\(p, p') -> List [p, p, p'])

    it "Nothing if all points are collinear" $ property $
      all ( isNothing .
            nTriangle' .
            fromJust . take3 ) .
      permutations .
      (\s -> midpoint s <| thps' s)

    it "vectors match point order" $ ups $
      \(p0, p1, p2) -> let t = fromJust $ nTriangle p0 p1 p2 in
        triVectors t ==
          ( fromJust $
            fromFoldableMay $
            fmap _toNZV
            [p0 .->. p1, p1 .->. p2, p2 .->. p0] )

    it "set contains all points" $ ups $
      \(p0, p1, p2) -> (fromEFoldable $ fromJust $ nTriangle p0 p1 p2) ==
                       (fromEFoldable [p0, p1, p2] :: Set Point)

  describe "triangle contains point" $ do
    for_
      [ ([pt 0 0, pt 2 0, pt 1 2], pt 1 1, True,  "inside")
      , ([pt 0 0, pt 2 0, pt 1 2], pt 2 2, False, "outside")
      , ([pt 0 0, pt 2 0, pt 1 2], pt 1 2, True,  "on edge")
      , ([pt 0 0, pt 2 0, pt 1 2], pt 0 0, True,  "on corner") ]
      $ \(pts2tri -> t, ip, result, testName) -> it testName $
          t `contains` ip `shouldBe` result

  it "edges" $ do
    ups $ \(p0, p1, p2) -> let t = nTriangle p0 p1 p2 in
      (edges <$> t) == (sequenceA =<< fromFoldableMay [p0 ..- p1, p1 ..- p2, p2 ..- p0])

  describe "area" $ do
    for_
      [ ([pt 0 0, pt 2 0, pt 2 2], 2, "half square")
      , ([pt 0 0, pt 2 0, pt 0 1], 1, "horizontal base")
      , ([pt 0 0, pt 5 4, pt 8 2], 11, "low left")
      , ([pt 0 0, pt (-5) 4, pt (-8) 2], 11, "low right")
      , ([pt 1 2,pt 4 0,pt 0 4], 2, "upside down") ]
    $ \(pts, result, testName) -> it testName $
        area (pts2tri pts) `shouldBe` fromScalar result

  describe "Intersect" $ do
    let initPoints = [pt 1 1, pt 4 1, pt 4 5]

    let testPermutations func
                         (List -> basePts)
                         (List -> pts, result, testName) =
          it testName $
            for_ (allCombo'' (permutations pts) (permutations basePts))
              $ \(pts2tri -> t0, pts2tri -> t1) -> do
                  func t0 t1 `shouldBe` result
                  func t1 t0 `shouldBe` result

    describe "intersects" $ do
      for_
        [ ([pt 3 2, pt 3 0, pt 2 0], True, "ln Xs 0th seg, pt in")
        , ([pt 0 2, pt 5 2, pt 5 3], True, "2 Xs 2, no interior pts")
        , ([pt 3 2, pt 3 3, pt 2 2], True, "all interior")
        , ([pt 6 6, pt 7 7, pt 6 7], False, "no intersect")
        , ([pt 2 1, pt 3 1, pt 0 0], True, "boundry only") ]
        $ testPermutations intersects initPoints

      it "does intersect when there is only a boundry intersect" $ property $
        intersects' . t3to2 . getSharedBoundryNoInterior

      -- it "does intersect with interior intersect" $ do


    describe "interiorIntersect" $ do
      describe "specific cases" $ do
        for_
          [ ([pt 3 2, pt 3 0, pt 2 0], True, "ln Xs 0th seg, pt in")
          , ([pt 0 2, pt 5 2, pt 5 3], True, "2 Xs 2, no interior pts")
          , ([pt 3 2, pt 3 3, pt 2 2], True, "all interior")
          , ([pt 6 6, pt 7 7, pt 6 7], False, "no intersect")
          , ([pt 2 1, pt 3 1, pt 0 0], False, "boundry only")
          , ([pt 1 1, pt 4 1, pt 0 0], False, "boundry only") ]
          $ testPermutations interiorIntersect initPoints

      -- it "has intersect when triangles have overlapping areas" $ do

      it "no intersect with only shared boundry" $ property $
        not . interiorIntersect' . t3to2 . getSharedBoundryNoInterior


  describe "Arbitrary Helpers" $ do
    describe "shared boundry" $ do
      let containsSharedPoint :: (Triangle, Triangle, Point) -> Bool
          containsSharedPoint =
            all (uncurry elem . second triSetPoints) .
            (\(t0, t1, p) -> zipL (L.repeat p) [t0, t1])

      let noInterirorIntersectPoint :: (Triangle, Triangle, Point) -> Bool
          noInterirorIntersectPoint =
            maybeBool .
            ( not .
              any id .
              concat .
              fmap (\(vs, (v0, v1)) -> fmap (uncurry between vs) [v0, v1]) .
              liftA2 zip id reverse <$<
              sequenceA .
              (\(t0, t1, p) -> fmap ( bimap' (.<-. p) <$<
                                     take2 .
                                     filterL (/= p) .
                                     triSetPoints )
                                    (List [t0, t1]) ) )

      let containsSharedSegm :: (Triangle, Triangle, Segm) -> Bool
          containsSharedSegm (t0, t1, s) = tc t0 && tc t1
            where tc = (== 1) . length . filterL (`contains` s) . edges

      let noInterirorIntersectSegm :: (Triangle, Triangle, Segm) -> Bool
          noInterirorIntersectSegm =
            maybeBool .
            ( biInteriorIntersect' <$<
              sequenceA .
              (&&&) (getLine . thd3)
                    ( (uncurry (..-) :: (Point, Point) -> Maybe Segm) <=<
                      take2 .
                      (\(ps, (Segm l)) -> filterL (not . contains l) ps) .
                      (\(t0, t1, s) -> (concat $ fmap triSetPoints [t0, t1], s)) ) )

      describe "SharedBoundryNoInterior" $ do
        it "triangles contain shared element" $ property $
          (\case tup@(_, _, Left  p) ->
                   containsSharedPoint (mapThd3 (const p) tup)
                 tup@(_, _, Right s) ->
                   containsSharedSegm  (mapThd3 (const s) tup) ) .
          getSharedBoundryNoInterior

        it "no interior intersect" $ property $
          (\case tup@(_, _, Left  p) ->
                   noInterirorIntersectPoint (mapThd3 (const p) tup)
                 tup@(_, _, Right s) ->
                   noInterirorIntersectSegm  (mapThd3 (const s) tup) ) .
          getSharedBoundryNoInterior

      describe "SharesOneEdgeNoInterior" $ do
        it "each triangle has edge" $ property $
          (\(t0, t1, s) -> all ( elem s .
                                 uncurry (<>) . mkSnd (fmap PLS.reverse) .
                                 edges :: Triangle -> Bool)
                               [t0, t1] ) .
          getSharesOneEdgeNoInterior

        it "no interior intersect" $ property $
          noInterirorIntersectSegm . getSharesOneEdgeNoInterior

      describe "SharesPartialEdgeNoInterior" $ do
        it "one edge of each triangle contains the segment" $ property $
          containsSharedSegm . getSharesPartialEdgeNoInterior

        it "no interior intersect" $ property $
          noInterirorIntersectSegm . getSharesPartialEdgeNoInterior


      describe "SharesPointNoInterior" $ do
        it "each triangle has the point" $ property $
          containsSharedPoint . getSharesPointNoInterior

        it "no interior intersect" $ property $
          noInterirorIntersectPoint . getSharesPointNoInterior

    -- let pointInTriangle :: Triangle -> Point -> Bool
    --     pointInTriangle t p =
    --       maybeBool $
    --       ( sequenceA $ fmap ( uncurry between <=<
    --                           bisequenceA . (&&&)
    --                             ( take2 .
    --                               uncurry fmap .
    --                               first (.->.) )
    --                             (return . (.->. p) . fst) <=<
    --                           uncons ) $
    --         rotations $
    --         toList $
    --         triSetPoints t )

    -- it "genPointInTriangle" $ do
    --   property $ \t -> forAll (genPointInTriangle t) $ pointInTriangle t


    -- describe "shared interior" $ do
    --   describe "SharedInterior" $ do

    --   describe "InsideTriangle" $ do
    --     it "all points in other triangle" $ do
    --     it "triangle points are not collinear" $ do
    --     it "triangle points are unique" $ do


    --   OneCrossTwo
    --   TwoCrossTwo
    --   OneInside
    --   TwoInside
