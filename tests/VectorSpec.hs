{-# LANGUAGE ScopedTypeVariables
           , ViewPatterns
           , PartialTypeSignatures
           , NoImplicitPrelude
           , TypeApplications #-}
-- module Main where
module VectorSpec where

import Vector
  ( Vector(Vector), NZVector(NZVector), Unit
  , Magnitude(mag, magSqrd)
  , parallel, parallel'
  , collinearV
  , perp, perpendicular
  , toward
  , Ward(Leftward, Rightward, Forward, Backward, Stopped)
  , GetDimension(getX, getY, getXY)
  , diff
  , between
  , between'
  , xp
  , dp
  , ang, vecAngle, setLength, within90, to1Dsq
  , vec, _vec
  , toVec, fromVec
  , toUv, _toUv
  , rotate
  , reverse
  , ParallelVectors(ParallelVectors, getParallelVectors)
  , NonParallelVectors(NonParallelVectors, getNonParallelVectors)
  , PerpendicularVectors(PerpendicularVectors, getPerpendicularVectors)
  , NonPerpendicularVectors(NonPerpendicularVectors, getNonPerpendicularVectors)
  , CollinearVectors(CollinearVectors, getCollinearVectors)
  , NonCollinearVectors(NonCollinearVectors, getNonCollinearVectors)
  , Within90DegVectors(getWithin90DegVectors)
  , Outside90DegVectors(getOutside90DegVectors)
  )

import Dimension
  ( Rx
  , Ry
  , Distance
  , Squared(Squared)
  , Relative(Relative)
  , (=*~)
  , (~*=)
  , (=/~)
  , (.-.)
  , (.+=)
  , (=+.)
  , RelativeAddition((=+=), (=-=), negate)
  , RelativeMultiplication(sq)
  , SquaredOps((^+^))
  , Absolute(Absolute)
  , ToScalar(toScalar)
  , ABS(abs)
  , fromScalar
  )
import Angle
  ( Aa, Ra
  , pi, piN, twoPi
  , Clock(CCW, CW)
  , HalfCircle(getHalfCircleAngle)
  , PiPi(PiPi, getPiPiAngle)
  , normalize
  )

import Trig (asin)
import Ratio (NZRatio(NZRatio, getNZRatio), ZRatio(ZRatio), Ratio(Ratio))
import Normalized (NormalizedLU(getNormalizedLU))
import IsClose(IsClose(isCloseA, isClose, isCloseNZ))

import Utilities (zero, bimap')

import Unified (sortOn, empty)
import Unified.Utilities (take3)
import Unified.Collections.List (List)
import Unified.Utilities.List (tupleCombo, rotationsL)
import Unified.QuickCheck (elements)
import Unified.Arbitrary (uniqueValues)

import Control.Applicative ((<*>))
import Control.Monad ((=<<))
import Data.Bifunctor (bimap)
import Data.Bool
import Data.Eq
import Data.Foldable (traverse_, for_)
import Data.Function
import Data.Functor
import Data.Maybe
import Data.Maybe (fromJust)
import Data.Ord
import Data.Tuple (uncurry)
import GHC.Float (sqrt)
import GHC.IO
import GHC.Num ((*))
import GHC.Real ((/))
import Test.Hspec hiding (parallel)
import Test.Hspec.Core.Spec (SpecM)
import Test.QuickCheck hiding (elements)

import Prelude (Double, String)


main :: IO ()
main = hspec spec


approxEqual:: IsClose a => a -> a -> IO ()
approxEqual x y = do
  isClose x y `shouldBe` True


spec :: Spec
spec = do

  describe "NZVector" $ do
    describe "FromCoords" $ do
      it "zero case" $ do
        (vec 0 0 :: Maybe NZVector) `shouldBe` Nothing

      it "non zero" $ do
        property $ \(NonZero (x :: Double)) (NonZero (y :: Double)) ->
          vec x y == (Just $ NZVector $ _vec x y)

    it "arbitrary" $ do
      property (/= (NZVector $ zero))


  describe "Unit" $ do
    describe "FromCoords" $ do
      it "length one" $ do
        property $ \(NonZero (x :: Double)) (NonZero (y :: Double)) ->
          (uncurry (^+^) $ bimap sq sq $ getXY @Relative (_vec x y :: Unit)) `isCloseNZ` fromScalar 1

      it "collinear with NZVector" $ do
        property $ \(NonZero (x :: Double)) (NonZero (y :: Double)) ->
          (_vec x y :: NZVector) `collinearV` (_vec x y :: Unit)

      it "zero vector" $ do
        (vec 0 0 :: Maybe Unit) `shouldBe` Nothing

    describe "ToUv from Aa" $ do
      it "specific case" $ do
        let a = (5 / 4) ~*= (pi :: Aa)
        let edge = ((-1) / sqrt(2))
        let refVec = _vec edge edge :: Unit
        approxEqual (_toUv a) refVec

      it "property" $ do
        let test :: Unit -> Bool
            test u = (_toUv $ ang u) `isCloseNZ` u
        property test

    it "setLength" $ do
      let test :: Distance -> Unit -> Bool
          test l u = mag (setLength l u) `isClose` abs l
      property test

    it "Arbitrary length one" $ do
      let test :: Unit -> Bool
          test u = magSqrd u `isCloseNZ` fromScalar 1
      property test


  describe "Vector" $ do
    it "xp (cross product)" $ do
      let v0 = _vec 1 2 :: Vector
      let v1 = _vec 3 4 :: Vector
      (v0 `xp` v1) `shouldBe` fromScalar (-2)
      (v1 `xp` v0) `shouldBe` fromScalar 2

    it "dp (dot product)" $ do
      let v0 = _vec 2 3 :: Vector
      let v1 = _vec 4 5 :: Vector
      (v0 `dp` v1) `shouldBe` (Squared $ Relative 23)

    describe "parallel" $ do
      let test :: (Vector, Vector, Bool, String) -> SpecM () ()
          test (v0, v1, result, testName) = it testName $ do
            let testParallel (v0', v1') = do
                  v0' `parallel` v1' `shouldBe` result

            traverse_ testParallel $ tupleCombo negate (v0, v1)

      traverse_ test [ (_vec 1 1, _vec 1 1, True, "parallel")
              , (_vec 1 1, _vec 2 1, False, "not parallel")
              , (zero, _vec 1 1, False, "zero length vector")
              ]

      it "is paralell (property)" $ property $
        (parallel' :: (Vector, Vector) -> Bool) .
        getParallelVectors

      it "is not parallel (property)" $ property $
        not .
        (parallel' :: (Vector, Vector) -> Bool) .
        getNonParallelVectors

    describe "collinear" $ do
      it "is collinear" $ property $
        uncurry (collinearV :: Vector -> Vector -> Bool) .
        getCollinearVectors

      it "not collinear" $ property $
        not .
        uncurry (collinearV :: Vector -> Vector -> Bool) .
        getNonCollinearVectors

    describe "Magnitude" $ do
      it "mag" $ do
        mag (_vec 3 4 :: Vector) `shouldBe` Relative 5

      it "mag and magSqrd are equivalent" $ do
        let test :: Vector -> Bool
            test v = sq (mag v) `isClose` magSqrd v
        property test

      it "correct length" $ do
        let test :: Unit -> Double -> Bool
            test u f = magSqrd (toVec u =*~ f) `isClose` sq (fromScalar f)
        property test

    describe "perp" $ do
      it "pi/2 ccw angle" $ do
        let test :: NZVector -> Bool
            test v = isClose (ap .-. a) (pi =/~ 2 :: Ra)
              where a = ang v
                    ap' = ang (perp v)
                    ap | a > pi =/~ 2 = ap' .+= (twoPi :: Ra)
                       | otherwise          = ap'

        property test

      it "magnitude is equivalent" $ do
        let test :: Vector -> Bool
            test v = magSqrd v `isClose` magSqrd (perp v)
        property test

    describe "perpendicular" $ do
      it "is perpendicular" $ do
        property $ uncurry (perpendicular :: Vector -> Vector -> Bool) .
                   getPerpendicularVectors

      it "not perpendicular" $ do
        property $ not .
                   uncurry (perpendicular :: Vector -> Vector -> Bool) .
                   getNonPerpendicularVectors

      it "zero is not perpendicular" $ do
        let test :: Vector -> Bool
            test v = not ( v `perpendicular` z ||
                           z `perpendicular` v )
              where z = zero :: Vector
        property test

    describe "ang (angle)" $ do
      it "quad 1" $ do
        let a = ang (_vec 3 4 :: NZVector)
        let refA = asin (4 / 5) =+. zero
        approxEqual a refA

      it "quad 3" $ do
        let a = ang (_vec (-3) (-4) :: NZVector)
        let refA = asin (4 / 5) =-= pi =+. zero
        approxEqual a refA

      it "range (-pi,pi]" $ do
        let test :: Unit -> Bool
            test u = (a > piN && a <= pi)
              where a = ang u

        property test

      it "unit from vector and unit from angle are equivalent" $ do
        let test :: NZVector -> Bool
            test v = _toUv v `isCloseNZ` _toUv (ang v)
        property test

    describe "RelativeAddition" $ do
      it "all properties" $ do
        let test :: Vector -> Vector -> Bool
            test v0 v1 = ((v0 =+= v1) =-= v1) `isClose` v0
        property test

      describe "negate" $ do
        it "non zero vector" $ do
          let test (NZVector v) = v `parallel` n && not (v `collinearV` n)
                where n = negate v
          property test

        it "zero" $ do
          negate (zero :: Vector) `shouldBe` zero

    describe "between" $ do
      describe "specific tests" $ do
        let test :: (Vector, Vector, Vector, Bool, String) -> SpecM () ()
            test (v0, v1, v2, result, testName) = it testName $ do
              between v0 v1 v2 `shouldBe` result
              between v1 v0 v2 `shouldBe` result

        traverse_ test
          [ (_vec 1 1, _vec (-1) 1, _vec 1 2,    True,  "acute between")
          , (_vec 1 1, _vec (-1) 1, _vec 2 1,    False, "acute not between")
          , (_vec 1 1, _vec (-1) 1, _vec 1 1,    True,  "on boundry 0")
          , (_vec 1 1, _vec (-1) 1, _vec (-1) 1, True,  "on boundry 1")
          ]

      it "is between" $ do
        let test (NZVector v) (ZRatio rb) (Ratio ri) (Positive l) (Positive l') =
              between v  v1 v2 &&
              between v1 v  v2
              where v1 = rot ab =*~ l
                    v2 = rot ai =*~ l'

                    ab = pi =*~ rb
                    ai = ab =*~ ri

                    rot = (`rotate` v)

        property test

      it "not between" $ do
        property $ \(NZVector v)
                    (ZRatio rb)
                    (NZRatio ri)
                    (Positive l)
                    (Positive l') -> let
                      v1 = rot ab =*~ l
                      v2 = rot ai =*~ l'

                      ab = pi =*~ rb
                      ai = negate (2 ~*= pi =-= ab) =*~ ri

                      rot = (`rotate` v)
                      in not ( between v  v1 v2 ||
                               between v1 v  v2 )

      it "zero vector" $ do
        property $ \(v0 :: Vector) (v1 :: Vector) -> let
          z = zero :: Vector
          in not $ ( between v0 v1 z ||
                     between v0 z v1 ||
                     between z v0 v1 )

    describe "between'" $ do
      -- describe "is between" $ do
      --   forAll ( fromJust . take4 <$>
      --            (fmap . setLength . fromScalar . getPositive <$> arbitrary) <*>
      --            (elements . rotations . sortOn ang =<< uniqueValues 4 empty) )

      --          (\(v0, v1, v2, v3) -> between v0 v1 v2 v3)

      -- describe "not between" $ do
      --   forAll ( fromJust . take4 <$>
      --            (fmap . setLength . fromScalar . getPositive <$> arbitrary) <*>
      --            (elements . rotations . sortOn ang =<< uniqueValues 4 empty) )

      --          ( not .
      --            (\(v0, v1, v2, v3) -> between v0 v1 v2 v3) )

      let gvs =
            fromJust . take3 <$>
            ( (fmap . setLength . fromScalar . getPositive <$> arbitrary) <*>
              (elements . rotationsL @List . sortOn ang =<< uniqueValues 3 empty) )
             :: Gen (Vector, Vector, Vector)

      let isBetween :: Clock
                    -> (Double -> Ra)
                    -> NZVector
                    -> (NZRatio, NZRatio)
                    -> (Positive Double, Positive Double)
                    -> Bool
          isBetween c
                    toAng
                    (NZVector v0)
                    (bimap' getNZRatio -> (r1, r2))
                    (bimap' getPositive -> (p1, p2)) = between' v0 v2 c v1
            where v2 = rotate a2 v0 =*~ p2
                  v1 = rotate (a2 =*~ r1) v0 =*~ p1
                  a2 = toAng r2


      describe "is between CCW" $ do
        describe "CCW" $ do
          it "acute and obtuse" $ do
            property $ isBetween CCW (~*= pi)

          it "reflex" $ do
            property $ isBetween CCW ((=+= pi) . (~*= pi))

          it "pi" $ do
            property $ \(NZVector v) (getNZRatio -> r) (getPositive -> p) ->
              between' v (reverse v) CCW (rotate (pi =*~ r) v =*~ p)

          it "all" $ do
            forAll gvs $ \(v0, v1, v2) -> between' v0 v2 CCW v1

        describe "CW" $ do
          it "acute and obtuse" $ do
            property $ isBetween CW (~*= piN)

          it "reflex" $ do
            property $ isBetween CW ((=+= piN) . (~*= piN))

          it "pi" $ do
            property $ \(NZVector v) (getNZRatio -> r) (getPositive -> p) ->
              between' v (reverse v) CW (rotate (piN =*~ r) v =*~ p)

          it "all" $ do
            forAll gvs $ \(v0, v1, v2) -> between' v0 v1 CW v2

      describe "is not between" $ do
        describe "CCW" $ do
          it "acute and obtuse" $ do
            property $ \(NZVector v0)
                        (bimap' getNZRatio -> (r1, r2))
                        (bimap' getPositive -> (p1, p2)) ->
                          let v1 = rotate ((twoPi =-= a2) =*~ r1 =+= a2) v0 =*~ p1
                              v2 = rotate a2 v0 =*~ p2
                              a2 = r2 ~*= pi
                          in not $ between' v0 v2 CCW v1

          it "reflex" $ do
            property $ \(NZVector v0)
                        (bimap' getNZRatio -> (r1, r2))
                        (bimap' getPositive -> (p1, p2)) ->
                          let v1 = rotate (r1 ~*= a2) v0 =*~ p1
                              v2 = rotate a2 v0 =*~ p2
                              a2 = r2 ~*= piN
                          in not $ between' v0 v2 CCW v1

          it "pi" $ do
            property $ \(NZVector v) (getNZRatio -> r) (getPositive -> p) ->
              not $ between' v (reverse v) CCW (rotate (piN =*~ r) v =*~ p)

          it "all" $ do
            forAll gvs $ \(v0, v1, v2) -> not $ between' v0 v1 CCW v2

        describe "CW" $ do
          it "all" $ do
            forAll gvs $ \(v0, v1, v2) -> not $ between' v0 v2 CW v1

      it "not between if collinear with boundry vector" $ do
        property $ \(v0 :: NZVector) (v1 :: NZVector) ->
          between' v0 v1 CCW v0 &&
          between' v0 v1 CCW v1 &&
          between' v0 v1 CW v0 &&
          between' v0 v1 CW v1


    describe "to1Dsq" $ do
      let test :: (NZVector, Vector, Double, String) -> SpecM () ()
          test (v0, v1, result, testName) = it testName $ do
            to1Dsq v0 v1 `shouldBe` Squared (Absolute result)

      traverse_ test [ (_vec 1 1, _vec 1 2, 5, "positive above")
              , (_vec 1 1, _vec 2 1, 5, "positive below")
              , (_vec 1 1, _vec 2 2, 8, "positive inline")
              , (_vec 1 1, _vec (-1) (-2), -5, "negative above")
              , (_vec 1 1, _vec (-2) 1, -5, "negative above 2")
              , (_vec 1 1, _vec (-2) (-1), -5, "negative below")
              , (_vec 1 1, _vec (-2) (-2), -8, "negative inline")]

      it "acute angle is positive" $ do
        property $ (> zero) .
                   (uncurry to1Dsq) .
                   (getWithin90DegVectors @NZVector)

      it "obtuse angle is negative" $ do
        property $ (< zero) .
                   (uncurry to1Dsq) .
                   (getOutside90DegVectors @NZVector)

      it "always returns absolute value of the vector's squared length" $ do
        property $ \(v :: NZVector) (v' :: Vector) ->
          (abs $ toScalar $ to1Dsq v v') == toScalar (magSqrd v')


    describe "toward" $ do
      let test :: (Vector, Vector, Ward, String) -> SpecM () ()
          test (v0, v1, result, testName) = it testName $ do
            toward v0 v1 `shouldBe` result

      traverse_ test [ (_vec 1 0, _vec 0 1, Leftward, "Leftward")
              , (_vec 0 1, _vec 1 0, Rightward, "Rightward")
              , (_vec 1 1, _vec 1 1, Forward, "Forward")
              , (_vec 1 1, _vec (-1) (-1), Backward, "Backward")
              , (zero,     _vec 1 1, Stopped, "Stopped")
              ]

      it "Forward (property)" $ do
        property $ (== Forward) .
                   uncurry (toward :: Vector -> Vector -> Ward) .
                   getCollinearVectors

      it "Backward (property)" $ do
        property $ (== Backward) .
                   uncurry (toward :: Vector -> Vector -> Ward) .
                   fmap reverse .
                   getCollinearVectors

      it "Leftward (property)" $ do
        property $ \(NZVector v) (NZRatio ra) (NZRatio rl) ->
          v `toward` rotate (pi =*~ ra) (v =*~ (2 * rl)) == Leftward

      it "Rightward (property)" $ do
        property $ \(NZVector v) (NZRatio ra) (NZRatio rl) ->
          v `toward` rotate (piN =*~ ra) (v =*~ (2 * rl)) == Rightward

      it "Stopped" $ do
        property $ \(v :: Vector) -> let
          z = zero :: Vector
          in v `toward` z == Stopped &&
             z `toward` v == Stopped

    it "GetDimension" $ do
      let x = fromScalar 1 :: Rx
      let y = fromScalar 2 :: Ry
      let v = Vector (x, y)
      getX v `shouldBe` x
      getY v `shouldBe` y
      getXY v `shouldBe` (x, y)

    describe "diff" $ do
      it "specific case" $ do
        (_vec 4 7 :: Vector) `diff` (_vec 1 2 :: Vector) `shouldBe` _vec 3 5

      it "equivalence check" $ do
        let test :: Vector -> Vector -> Bool
            test x y = ((x `diff` y) =+= y) `isClose` x
        property test

    describe "within90" $ do
      it "within 90 degrees" $ do
        property $ uncurry (within90 :: Vector -> Vector -> Bool) .
                   getWithin90DegVectors

      it "further than 90 degrees" $ do
        property $ not .
                   uncurry (within90 :: Vector -> Vector -> Bool) .
                   getOutside90DegVectors

      it "perpendicular not within 90" $ do
        property $ not .
                   uncurry (within90 :: Vector -> Vector -> Bool) .
                   getPerpendicularVectors

      it "zero vector always false" $ do
        let test :: Vector -> Bool
            test v = not ( v `within90` z ||
                           z `within90` v )
              where z = zero :: Vector
        property test

    describe "rotate" $ do
      it "specific case" $ do
        let rot = (`rotate` _vec 2 2)
        rot (pi =/~ 2)  `approxEqual` (_vec (-2) 2 :: Vector)
        rot (piN =/~ 2) `approxEqual` (_vec 2 (-2) :: Vector)

      it "is rotated" $ do
        property $ \(fromScalar . getNonZero -> a) (v :: NZVector) ->
          let v' = rotate a v
          in v `isClose` rotate (negate a) v' && v /= v'

      it "rotated by correct angle" $ do
        property $ \(fromScalar . getNonZero -> a) (v :: NZVector) ->
          (getHalfCircleAngle $ getNormalizedLU $ vecAngle v $ rotate a v) `isClose`
          (abs $ getPiPiAngle $ getNormalizedLU $ normalize $ PiPi $ a)

      it "zero vector" $ do
        let test :: Ra -> Bool
            test a = rotate a (zero :: Vector) == zero
        property test

    describe "reverse" $ do
      it "non zero vector" $ do
        let test :: NZVector -> Bool
            test v = v `parallel` r &&
                     not (v `collinearV` r) &&
                     v `isClose` reverse r
              where r = reverse v
        property test

      it "zero vector" $ do
        reverse (zero :: Vector) `shouldBe` zero

  describe "vecAngle" $ do
    describe "specific cases" $ do
      let test :: (NZVector, NZVector, HalfCircle Ra) -> NZRatio -> NZRatio -> Bool
          test ( NZVector v0
               , NZVector v1
               , result )
               (NZRatio r0) (NZRatio r1) =
            (getNormalizedLU $ v0' `vecAngle` v1') `isClose` result &&
            (getNormalizedLU $ v1' `vecAngle` v0') `isClose` result
            where v0' = NZVector (v0 =*~ r0)
                  v1' = NZVector (v1 =*~ r1)

      for_
        [ ((_vec 1 0, _vec (-1) 0, pi),       "pi")
        , ((_vec 1 1, _vec (-1) 1, pi =/~ 2), "pi / 2")
        , ((_vec 1 1, _vec 0    1, pi =/~ 4), "pi / 4") ]
        $ \(args, testName) -> it testName $ property $ test args

    it "within range [0, pi]" $ do
      property $ \(v0 :: NZVector) (v1 :: NZVector) -> let
        a0 = getNormalizedLU $ v0 `vecAngle` v1
        a1 = getNormalizedLU $ v1 `vecAngle` v0
        in a0 == a1 &&
           a0 <= pi &&
           a0 >= zero

    it "rotate and compare" $ do
      property $ \v@(NZVector v') (a :: Ra) (Positive l) ->
        let (vr :: NZVector) = fromJust $ fromVec $ rotate a v' =*~ l
        in isCloseA 1e-5 (getHalfCircleAngle $ getNormalizedLU $ vecAngle v vr)
                         (abs $ getPiPiAngle $ getNormalizedLU $ normalize $ PiPi a)

    -- it "Nothing if zero vector" $ do
    --   vecAngle (zero :: Vector) (zero :: Vector) `shouldBe` Nothing
    --   vecAngle (vec 1 1 :: Vector) (zero :: Vector) `shouldBe` Nothing
    --   vecAngle (zero :: Vector) (vec 1 1 :: Vector) `shouldBe` Nothing

    it "collinear vecs near zero" $ do
      property $ isCloseA 1e-5 zero .
                 getNormalizedLU .
                 uncurry (vecAngle :: NZVector -> NZVector -> NormalizedLU (Angle.HalfCircle Ra)) .
                 getCollinearVectors

  describe "Arbitrary Helpers" $ do
    it "ParallelVectors" $ do
      let test :: ParallelVectors Vector -> Bool
          test (ParallelVectors (v0, v1)) =
            u0 `isClose` u1 || u0 `isClose` negate u1
            where u0 = _toUv v0
                  u1 = _toUv v1
      property test

    it "NonParallelVectors" $ do
      let test :: NonParallelVectors Vector -> Bool
          test (NonParallelVectors (v0, v1)) =
            v0 == zero || v1 == zero ||
            not ((v0 `xp` v1) `isCloseNZ` zero)
      property test

    it "PerpendicularVectors" $ do
      let test :: PerpendicularVectors Vector -> Bool
          test (PerpendicularVectors (v0, v1)) =
            (_toUv v0 `dp` _toUv v1) `isCloseNZ` zero
      property test

    it "NonPerpendicularVectors" $ do
      let test :: NonPerpendicularVectors Vector -> Bool
          test (NonPerpendicularVectors (v0, v1)) =
            v0 == zero || v1 == zero ||
            (not $ isCloseNZ zero $ dp v0 v1)
      property test

    it "CollinearVectors" $ do
      let test :: CollinearVectors Vector -> Bool
          test (CollinearVectors (v0, v1)) =
            toUv v0 `isCloseNZ` toUv v1
      property test

    it "NonCollinearVectors" $ do
      let test :: NonCollinearVectors Vector -> Bool
          test (NonCollinearVectors (v0, v1)) =
            v0 == zero || v1 == zero ||
            not (toUv v0 `isClose` toUv v1)
      property test

    it "Within90DegVectors" $ do
      property $ (< pi) .
                 getNormalizedLU .
                 uncurry (vecAngle :: NZVector -> NZVector -> NormalizedLU (Angle.HalfCircle Ra)) .
                 getWithin90DegVectors

    it "Outside90DegVectors" $ do
      property $ (> pi =/~ 2) .
                 getNormalizedLU .
                 uncurry (vecAngle :: NZVector -> NZVector -> NormalizedLU (Angle.HalfCircle Ra)) .
                 getOutside90DegVectors
